from distutils.core import setup, Extension

module = Extension(name = 'y5',
                   sources = ['y5-python.cpp'],
                   libraries = ['y5'],
                   extra_compile_args = ['-std=c++17'])

setup(name = 'y5',
      version = '1.0.0',
      description = 'Python bindings for the Y5 file format',
      ext_modules = [module])

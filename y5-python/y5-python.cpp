#include <y5.h>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

PYBIND11_MODULE(y5, module) {
    pybind11::register_exception<y5::detail::failed_assertion>(module, "failed_assertion");

    auto tuneables = pybind11::class_<y5::performance::tuneables>(module, "tuneables")
        .def_readwrite("maximum_overallocation", &y5::performance::tuneables::maximum_overallocation, "Maximum size by which to grow a file at a single time")
        .def_readwrite("maximum_held_buffer", &y5::performance::tuneables::maximum_held_buffer, "Maximum size of the already-used part of the internal buffer")
        .def_readwrite("initial_buffer_size", &y5::performance::tuneables::initial_buffer_size, "Initial buffer size for writing.")
        .def_readwrite("compression_level", &y5::performance::tuneables::compression_level, "How well to compress data");
    tuneables.doc() = "Tuneable parameters to adjust memory usage and performance. See y5.h for more details.";

    pybind11::enum_<y5::performance::tuneables::compression_levels>(module, "compression_level")
        .value("fastest", y5::performance::tuneables::compression_levels::compression_fastest)
        .value("faster", y5::performance::tuneables::compression_levels::compression_faster)
        .value("default", y5::performance::tuneables::compression_levels::compression_default)
        .value("better", y5::performance::tuneables::compression_levels::compression_better)
        .value("best", y5::performance::tuneables::compression_levels::compression_best);

    pybind11::enum_<y5::format::header::bitflags>(module, "flags")
        .value("transposed", y5::format::header::bitflags::transposed, "Traces are stored in columns")
        .value("ragged", y5::format::header::bitflags::ragged, "Not all traces have the same length")
        .value("uncompressed", y5::format::header::bitflags::uncompressed, "Compression is disabled");

    pybind11::class_<y5::reader>(module, "reader")
        .def(pybind11::init<const std::string &, y5::performance::tuneables>(),
             pybind11::arg("path"),
             pybind11::arg_v("tuneables", y5::performance::tuneables {}, "{}"),
             "Open a Y5 file for reading")
        .def_property_readonly("flags", [](const y5::reader &r) { return r.header().flags; })
        .def_property_readonly("entry_size", [](const y5::reader &r) { return r.header().entry_size; })
        .def_property_readonly("rows", [](const y5::reader &r) { return r.header().rows; })
        .def_property_readonly("max_columns", [](const y5::reader &r) { return r.header().max_columns; })
        .def_property_readonly("current_row", [](const y5::reader &r) { return r.row_index(); })
        .def("seek",
             &y5::reader::seek,
             pybind11::arg("index"),
             "Seek to the start of the specified row")
        .def("read",
             [](y5::reader &r, std::size_t count) -> std::pair<pybind11::array, bool> {
                 pybind11::array space {
                     pybind11::dtype { std::string { "V" } + std::to_string(r.header().entry_size) },
                     std::vector<ssize_t> { 1, static_cast<ssize_t>(count) },
                     nullptr
                 };
                 auto [read, row_done] = r.read_buffer(reinterpret_cast<unsigned char *>(space.mutable_data()), count);
                 space.resize(std::vector<ssize_t> { 1, static_cast<ssize_t>(read) });
                 return { space, row_done };
             },
             pybind11::arg("count") = 1,
             "Read the specified number of elements from the current row into a Numpy array (of type V<entry_size>). Once the row is done, moves to the next row.");

    struct wrapped_writer {
        y5::writer writer;
        std::uint32_t entry_size;
    };

    pybind11::class_<wrapped_writer>(module, "writer")
        .def(pybind11::init([](const std::string &path, std::uint32_t entry_size, bool overwrite, y5::format::header::bitflags flags, y5::performance::tuneables tune) {
                 return wrapped_writer {
                     y5::writer(path, overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, entry_size, flags, y5::format::DEFAULT_TOC_CAPACITY, std::move(tune)),
                     entry_size
                 };
             }),
             pybind11::arg("path"),
             pybind11::arg("entry_size"),
             pybind11::arg("overwrite") = false,
             pybind11::arg_v("flags", y5::format::header::bitflags {}, "<no flags>"),
             pybind11::arg_v("tuneables", y5::performance::tuneables {}, "{}"),
             "Open a Y5 file for writing")
        .def("append",
             [](wrapped_writer &w, const pybind11::array &buf) {
                 Y5_ASSERT(buf.itemsize() == w.entry_size, "Invalid datatype");
                 for (ssize_t i = 0; i < buf.ndim() - 1; ++i)
                    Y5_ASSERT(buf.shape(i) == 1, "Invalid shape (must be a single row of data)");
                 w.writer.append_buffer(reinterpret_cast<const unsigned char *>(buf.data()), buf.shape(buf.ndim() - 1));
             },
             "Append data to the current row")
        .def("end_row",
             [](wrapped_writer &w) { w.writer.end_row(); },
             "End the current row");
}

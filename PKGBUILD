pkgname=whiteboxgrind-git
# For pkgver, see pkgver() below
pkgver=r0.0000000
pkgrel=1
pkgdesc='A program tracing utility based on valgrind for analyzing whitebox cryptography'
arch=('x86_64')
license=('GPL2')
depends=('glibc' 'perl')
makedepends=('git' 'gdb' 'lib32-glibc' 'lib32-gcc-libs')
checkdepends=('procps-ng')
optdepends=('lib32-glibc: 32-bit ABI support for valgrind')
source=(
    "${pkgname}::git+https://gitlab.lrz.de/tueisec/whiteboxgrind.git"
    "https://raw.githubusercontent.com/archlinux/svntogit-packages/d35fd8e20f169e59d477e2dec11d7ae521c5fd0b/trunk/valgrind-3.7.0-respect-flags.patch"
    "https://raw.githubusercontent.com/archlinux/svntogit-packages/d35fd8e20f169e59d477e2dec11d7ae521c5fd0b/trunk/valgrind-3.16-openmpi-4.0.patch"
)
sha256sums=(
    'SKIP'
    '4e6be3a1799c17a6e843ab1966a3a68ac0ffa83d4b2230ce1b607518c42a31a2'
    '10982c9f38cbb84fabe0d7b63f1dd6b0293e1252e42eaae13a10c7ceaac23d45'
)

_prefix=/opt/whiteboxgrind

pkgver() {
    # https://wiki.archlinux.org/index.php/VCS_package_guidelines
    cd "${pkgname}"
    (
        set -o pipefail
        git describe --long 2>/dev/null | sed 's/\([^-]*-g\)/r\1/;s/-/./g' ||
        printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
    )
}

prepare() {
    cd "${pkgname}/valgrind"
    patch -Np1 < ../../valgrind-3.7.0-respect-flags.patch
    patch -Np1 < ../../valgrind-3.16-openmpi-4.0.patch
    autoreconf -ifv
}

build() {
    cd "${pkgname}/valgrind"
    # Build flag changes from the valgrind PKGBUILD
    CPPFLAGS="${CPPFLAGS/-D_FORTIFY_SOURCE=2/}"
    CFLAGS="${CFLAGS/-fno-plt/}"
    CXXFLAGS="${CXXFLAGS/-fno-plt/}"
    # No stack protector because of linking issues
    CPPFLAGS="${CPPFLAGS/-fstack-protector-strong/}"
    CFLAGS="${CPPFLAGS/-fstack-protector-strong/}"
    ./configure --prefix="${_prefix}"
    make
    # NB: This doesn't build the docs (if you want those, install valgrind proper)
}

check() {
    # NB: No checking, since that does weird stuff with xmllint...
    cd "${pkgname}/valgrind"
}

package() {
    pushd "${pkgname}/valgrind"
    make DESTDIR="${pkgdir}" install
    if check_option 'debug' n; then
        find "${pkgdir}/${_prefix}/bin" -type f -executable -exec strip $STRIP_BINARIES {} + || :
    fi
    popd
}

#ifndef __WB_SOCKETS_H
#define __WB_SOCKETS_H

#include "pub_tool_libcbase.h"
#include "pub_tool_vki.h"

extern Int VG_(socket) ( Int domain, Int type, Int protocol ); // Provided by coregrind, we just need to specify the prototype
extern Int VG_(connect) ( Int sockfd, const struct vki_sockaddr* serv_addr, Int addrlen );

#endif

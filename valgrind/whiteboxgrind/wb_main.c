/* Main file for whiteboxgrind */

#include "pub_tool_options.h"
#include "pub_tool_oset.h"
#include "pub_tool_basics.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_machine.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcproc.h"
#include "pub_tool_libcfile.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_clientstate.h"

#define USE_VALGRIND_TYPES
#include "whiteboxgrind_types.h"
#include "wb_sockets.h"
#include "whiteboxgrind.h"

// Traces


static inline enum Endianness wb_end(IREndness end)
{
   switch (end) {
      case Iend_LE: return LittleEndian;
      case Iend_BE: return BigEndian;
      default: return UnknownEndianness;
   }
}
static inline IREndness ir_end(enum Endianness end)
{
   switch (end) {
      case LittleEndian: return Iend_LE;
      case BigEndian: return Iend_BE;
      default:
         VG_(tool_panic)("Requested ir_end conversion of unknown endianness");
         __builtin_unreachable();
   }
}

struct Trace {
   ULong index;
   ULong capacity;
   ULong item_size;
   char  *raw;
   char  *data; // = raw + prefix length
   Int fd;
   ULong prefix_size; // => 0 or sizeof(struct MessagePrefix)
   enum DataType type;
} pc_trace = { .type = DataTypePCTrace }, mem_trace = { .type = DataTypeMemoryTrace };

// Global whiteboxgrind options
struct Options {
   ULong start;
   ULong end;
   const HChar *function;
   const HChar *socket;
   const HChar *out_dir;
   const HChar *out_filename_template;
   Bool enable_pc_trace;
   Bool enable_mem_trace;
   Bool enable_client_requests;
   ULong buffer_size;
} wb_options = {
   .start = 0ul,
   .end = ~0ul,
   .socket = NULL,
   .out_dir = NULL,
   .out_filename_template = "%T-trace-%t-%p-%P.bin",
   .enable_pc_trace = True,
   .enable_mem_trace = True,
   .enable_client_requests = False,
   .buffer_size = 32ul*1024ul*1024ul
};

// Global state
static Bool wb_record = False;
static ULong wb_sync = 0; // Increments with every record written to either trace, to allow merging.

// Filename computation
static void wb_get_filename(char *str, SizeT size, const char *template, const char *prefix, const char *trace_type, unsigned long long timestamp, const char *program_name, unsigned long long pid)
{
   SizeT i, j;
   Bool had_trace_type = False;
   // Generate strings ahead of time (snprintf with positional specifiers seems to not work)
   char timestamp_str[30], pid_str[30];
   VG_(snprintf)(timestamp_str, sizeof(timestamp_str), "%llu", timestamp);
   VG_(snprintf)(pid_str, sizeof(pid_str), "%llu", pid);

   // Find formatting marks and replace accordingly
   j = VG_(strlcpy)(str, prefix, size);
   if (j >= size) VG_(tool_panic)("Filename too long");
   str[j++] = '/';

   for (i = 0; template[i]; ++i) {
      if (j >= size) VG_(tool_panic)("Filename too long");
      if (template[i] == '%') {
         switch (template[i + 1]) {
            case '%': str[j++] = '%'; ++i; break;
            case 'T': j += VG_(strlcpy)(&str[j], trace_type, size - j); ++i; had_trace_type = True; break;
            case 't': j += VG_(strlcpy)(&str[j], timestamp_str, size - j); ++i; break;
            case 'P': j += VG_(strlcpy)(&str[j], program_name, size - j); ++i; break;
            case 'p': j += VG_(strlcpy)(&str[j], pid_str, size - j); ++i; break;
            default: str[j++] = '%'; break;
         }
      } else {
         str[j++] = template[i];
      }
   }
   if (!had_trace_type)
      VG_(tool_panic)("No %T (trace type) format specifier in the template");
   tl_assert(j < size);
   str[j] = 0;
}

// Enable a trace
static void wb_enable_trace(struct Trace *trace, ULong item_size, const char *out_dir, int socket_fd, const char *name, unsigned long long timestamp, const char *program_name, unsigned long long pid)
{
   char filename[VKI_PATH_MAX] = {0};
   if (out_dir) {
      wb_get_filename(filename, sizeof(filename), wb_options.out_filename_template, out_dir, name, timestamp, program_name, pid);
      trace->fd = VG_(fd_open)(filename, VKI_O_CREAT|VKI_O_TRUNC|VKI_O_WRONLY, VKI_S_IRUSR|VKI_S_IWUSR);
      if (trace->fd < 0) {
         VG_(fmsg)("Failed to open trace log at %s\n", filename);
         VG_(exit)(1);
      }
      trace->prefix_size = 0;
   } else if (socket_fd >= 0) {
      trace->fd = socket_fd;
      trace->prefix_size = sizeof(struct MessagePrefix);
   } else {
      VG_(tool_panic)("No output method for trace");
   }
   trace->index = 0;
   trace->raw = VG_(malloc)("wb_enable_trace", wb_options.buffer_size + trace->prefix_size);
   trace->data = trace->raw + trace->prefix_size;
   trace->item_size = item_size;
   trace->capacity = wb_options.buffer_size;
}

// Post-init: set up trace files, log, ...
static void wb_post_clo_init(void)
{
   struct vki_timeval tv;
   char* out_dir;
   struct vki_sockaddr_un socket_addr;
   int socket_fd;
   const char* program_name;
   unsigned long long pid;

   VG_(gettimeofday)(&tv, NULL);

   program_name = VG_(strrchr)(VG_(args_the_exename), '/');
   if (!program_name)
      program_name = VG_(args_the_exename);
   else
      ++program_name;
   if (!program_name[0])
      program_name = "(empty)";

   pid = VG_(getpid)();

   out_dir = NULL;
   socket_fd = -1;
   if (wb_options.socket && wb_options.out_dir) {
      VG_(tool_panic)("Specify at most one of --sock and --out");
   } else if (wb_options.out_dir) {
      // Trim slashes off the end of out_dir
      if (!wb_options.out_dir[0])
         out_dir = VG_(strdup)("wb.out_dir", ".");
      else {
         out_dir = VG_(strdup)("wb.out_dir", wb_options.out_dir);
         char* dir_end = &out_dir[VG_(strlen)(out_dir) - 1];
         while (*dir_end == '/')
            *dir_end-- = 0;
      }
   } else if (wb_options.socket) {
      socket_addr.sun_family = VKI_AF_UNIX;
      VG_(strncpy)(socket_addr.sun_path, wb_options.socket, sizeof(socket_addr.sun_path) - 1);
      socket_fd = VG_(socket)(VKI_AF_UNIX, VKI_SOCK_STREAM, 0);
      if (socket_fd < 0)
         VG_(tool_panic)("Failed to open socket");
      if (VG_(connect)(socket_fd, (const struct vki_sockaddr *) &socket_addr, sizeof(socket_addr)) < 0) {
         VG_(fmsg)("Failed to connect to socket at %s\n", wb_options.socket);
         VG_(exit)(1);
      }
   } else if (wb_options.enable_pc_trace || wb_options.enable_mem_trace) {
      VG_(tool_panic)("Traces enabled but no output method specified");
   }

   if (wb_options.enable_pc_trace)
      wb_enable_trace(&pc_trace, sizeof(struct PCTraceEntry), out_dir, socket_fd, "pc", tv.tv_sec, program_name, pid);

   if (wb_options.enable_mem_trace)
      wb_enable_trace(&mem_trace, sizeof(struct MemoryTraceEntry), out_dir, socket_fd, "mem", tv.tv_sec, program_name, pid);

   if (wb_options.start == 0 && !wb_options.enable_client_requests)
      wb_record = True;
}

// Flush a trace
static void trace_flush(struct Trace *trace)
{
   struct MessagePrefix prefix;
   long written, remaining, offset = 0;
   if (trace->prefix_size) {
      prefix.version = PROTOCOL_VERSION;
      prefix.type = (UChar) trace->type;
      prefix.length = trace->index;
      tl_assert(sizeof(prefix) == trace->prefix_size);
      VG_(memcpy)(trace->raw, &prefix, sizeof(prefix));
   }
   remaining = trace->index + trace->prefix_size;
   while (remaining) {
      written = VG_(write)(trace->fd, trace->raw + offset, remaining);
      tl_assert(written > 0);
      remaining -= written;
      offset += written;
   }
   VG_(memset)(trace->data, 0, trace->index);
   trace->index = 0;
}

// Standard trace pre- and postamble
#define WB_CHECK_RECORD(addr) do { \
   if(!wb_record && (ULong)(addr) == wb_options.start) \
      wb_record = True; \
   if(wb_record && (ULong)(addr) == wb_options.end) \
      wb_record = False; \
   if(!wb_record) return; \
} while (0)

#define WB_TRACE_STORE(trace, entry) do { \
   tl_assert((trace).item_size == sizeof(entry)); \
   VG_(memcpy)(&(trace).data[(trace).index], &(entry), sizeof(entry)); \
   (trace).index += sizeof(entry); \
   if ((trace).index > (trace).capacity - sizeof(entry)) \
      trace_flush(&(trace)); \
} while (0)

// PC tracing
static VG_REGPARM(1) void trace_pc(Addr addr)
{
   struct PCTraceEntry entry;

   WB_CHECK_RECORD(addr);

   entry.sync = wb_sync++;
   entry.pc = (ULong) addr;

   WB_TRACE_STORE(pc_trace, entry);
}

// Memory tracing.
// Arguments should already be evaluated, as per
// https://sourceforge.net/p/valgrind/mailman/message/12964829/
struct MemoryTraceArgs {
   Addr pc;
   enum Endianness endianness;
   UInt width;
   enum AccessMode mode;
};

static VG_REGPARM(3) void trace_mem(HWord args_ptr, Addr mem, HWord value_ptr)
{
   struct MemoryTraceEntry entry;
   struct MemoryTraceArgs* args;
   Bool indirect = False;

   args = (struct MemoryTraceArgs*) args_ptr;

   WB_CHECK_RECORD(args->pc);

   if (args->mode == AccessCAS) {
      // Decode value in value_ptr
      if (!value_ptr)
         VG_(tool_panic)("AccessCAS without value pointer!");
      // TODO: Check whether this is a ptr-ptr or a ptr
      Int count = ((char *) value_ptr)[0];
      Int width = ((char *) value_ptr)[1];
      args->width = count * width + 2;
      // CAS is always indirect, regardless of value size
      indirect = True;
   } else if (args->width > sizeof(value_ptr)) {
      // Overly wide value, also access through pointer; see wb_mem_trace
      indirect = True;
   }

   entry.sync = wb_sync++;
   entry.pc = (ULong) args->pc;
   entry.addr = (ULong) mem;
   entry.mode = (UChar) args->mode;
   entry.endianness = (UChar) args->endianness;
   entry.width = args->width;
   if (entry.width > sizeof(entry.value)) {
      VG_(fmsg)("Value of width %u exceeds available space of %llu bytes\n", entry.width, (ULong) sizeof(entry.value));
      VG_(exit)(1);
   }
   if (indirect) {
      VG_(memcpy)(entry.value, (const void*) value_ptr, entry.width);
   } else {
      VG_(memcpy)(entry.value, (const void*) &value_ptr, entry.width);
   }
   if (entry.width < sizeof(entry.value)) {
      VG_(memset)(&entry.value[entry.width], 0, sizeof(entry.value) - entry.width);
   }

   WB_TRACE_STORE(mem_trace, entry);
}

// Turn non-atoms into atoms via a temporary
static IRExpr* wb_make_atom(IRSB* sbOut, IRExpr* value)
{
   IRTemp temporary;
   if (!isIRAtom(value)) {
      temporary = newIRTemp(sbOut->tyenv, typeOfIRExpr(sbOut->tyenv, value));
      addStmtToIRSB(sbOut, IRStmt_WrTmp(temporary, value));
      value = IRExpr_RdTmp(temporary);
   }
   return value;
}
// Turn large values into pointers
static IRExpr* wb_make_ptr(IRSB* sbOut, IRExpr* guard, IRExpr* value, enum Endianness end)
{
   void* addr;
   IRExpr* addrExpr;

   // TODO: Maybe there's a nicer way to do this via IRExpr_GSPTR(), but I'm not sure what
   //       that structure actually contains (it seems to be architecture-dependent, but I
   //       haven't found any details)
   // Instead, we just create a memory address here, and generate proper Store instructions
   // TODO: Is mixing guest and host addresses like this sane at all, even if they are the
   //       same architecture?
   addr = VG_(malloc)("wb_make_ptr", sizeofIRType(typeOfIRExpr(sbOut->tyenv, value)));
   tl_assert(addr);

   addrExpr = mkIRExpr_HWord((HWord) addr);

   // TODO: Do we need a temporary between the value and the store here too?
   if (guard)
      addStmtToIRSB(sbOut, IRStmt_StoreG(ir_end(end), addrExpr, value, guard));
   else
      addStmtToIRSB(sbOut, IRStmt_Store(ir_end(end), addrExpr, value));

   return addrExpr;
}

// Convert values into pointer-width
static IRExpr* wb_to_ptr_width(IRExpr* value, IRType fromType)
{
   IROp cvt;
   Int width = (fromType == Ity_I1) ? 0x1 : sizeofIRType(fromType);

   tl_assert(isIRAtom(value));
   _Static_assert(sizeof(HWord) < 256, "Massive HWord not supported, increase shift");

   if (fromType != integerIRTypeOfSize(sizeof(HWord))) {
      if (width < sizeof(HWord)) {
         // NB: This never happens in the CAS case, where we deal with a HWord-sized pointer already.
         switch ((width << 8) | sizeof(HWord)) {
            case 0x0102: cvt = Iop_8Uto16; break; // TODO: This case doesn't seem to work for bit types, there's no Iop_1Uto16
            case 0x0104: cvt = (fromType == Ity_I1) ? Iop_1Uto32 : Iop_8Uto32; break;
            case 0x0108: cvt = (fromType == Ity_I1) ? Iop_1Uto64 : Iop_8Uto64; break;
            case 0x0204: cvt = Iop_16Uto32; break;
            case 0x0208: cvt = Iop_16Uto64; break;
            case 0x0408: cvt = Iop_32Uto64; break;
            default:
               VG_(tool_panic)("Unsure how to handle width/host pointer size combination");
         }
      } else {
         ppIRType(fromType); VG_(printf)("\n");
         ppIRExpr(value); VG_(printf)("\n");
         VG_(tool_panic)("wtf");
      }
      // Arguments to Unop need to be atoms...
      value = IRExpr_Unop(cvt, value);
   }
   return value;
}

// Add trace calls to the current IRSB
static void wb_pc_trace(Addr pc, IRSB *sbOut)
{
   IRDirty* di;
   IRExpr** argv;

   argv = mkIRExprVec_1(mkIRExpr_HWord(pc));
   di = unsafeIRDirty_0_N(1, "trace_pc", VG_(fnptr_to_fnentry)(&trace_pc), argv);
   addStmtToIRSB(sbOut, IRStmt_Dirty(di));
}
static void wb_mem_trace(Addr pc, IRSB *sbOut, IRExpr *mem, enum AccessMode mode, IRExpr *guard, IRExpr *value, enum Endianness endianness)
{
   Int width;
   IRType type;
   IRDirty* di;
   IRExpr* valueExpr;
   IRExpr** argv;
   struct MemoryTraceArgs* args;

   if (mode == LogOnly) {
      VG_(fmsg)("Skipping memory access at %#llx (mode is LogOnly)\n", (ULong) pc);
      ppIRExpr(mem);
      VG_(printf)("\n");
      if (value) {
         VG_(printf)("Associated value:\n");
         ppIRExpr(value);
         VG_(printf)("\n");
      }
      return;
   }

   if (!value) {
      VG_(fmsg)("Unknown value and width for memory event at %#llx\n", (ULong) pc);
   }

   type = value ? typeOfIRExpr(sbOut->tyenv, value) : Ity_INVALID;
   width = value ? sizeofIRType(type) : 0;
   if (guard)
      guard = wb_make_atom(sbOut, guard); // Must be an atom, again.

   args = VG_(malloc)("wb_mem_trace", sizeof(*args));
   args->pc = pc;
   args->endianness = endianness;
   args->width = width;
   args->mode = mode;

   if (value) {
      if (mode == AccessCAS || width <= sizeof(HWord)) {
         // Arguments need to be pointer-width apparently (c.f. host_amd64_isel.c:581), so we widen it
         // Arguments also need to be atoms, so we use a temporary here if necessary.
         // This also applies to CAS mode, where the indirect pointer is already created
         valueExpr = wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, value), type));
      } else {
         // Value is too large to fit into the normal register, we need to transform it into a pointer
         valueExpr = wb_make_ptr(sbOut, guard, wb_make_atom(sbOut, value), endianness);
      }
   } else {
      valueExpr = wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_Const(IRConst_U1(False)), Ity_I1)); // Avoid passing NULL, which breaks stuff.
   }

   mem = wb_make_atom(sbOut, wb_to_ptr_width(mem, typeOfIRExpr(sbOut->tyenv, mem)));

   argv = mkIRExprVec_3(
      mkIRExpr_HWord((HWord) args),
      mem,
      valueExpr
   );
   di = unsafeIRDirty_0_N(/* regparms */ 3, "trace_mem", VG_(fnptr_to_fnentry)(&trace_mem), argv);
   if (guard)
      di->guard = guard; // If there is a guard, only do the call if the guard evaluates to true
   addStmtToIRSB(sbOut, IRStmt_Dirty(di));
}

// Walk an IRExpr for memory loads
static void wb_mem_walk_iexpr(Addr current, IRSB *sbOut, IRExpr *expr, enum AccessMode mode, IRExpr *guard)
{
   Int i;

   if (!expr) return;
   switch (expr->tag) {
      case Iex_Binder:
         VG_(fmsg)("Unexpect Iex_Binder tag in IRExpr\n");
         break;
      case Iex_Get: break;
      case Iex_GetI:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.GetI.ix, mode, guard);
         break;
      case Iex_RdTmp: break;
      case Iex_Qop:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Qop.details->arg1, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Qop.details->arg2, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Qop.details->arg3, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Qop.details->arg4, mode, guard);
         break;
      case Iex_Triop:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Triop.details->arg1, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Triop.details->arg2, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Triop.details->arg3, mode, guard);
         break;
      case Iex_Binop:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Binop.arg1, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Binop.arg2, mode, guard);
         break;
      case Iex_Unop:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Unop.arg, mode, guard);
         break;
      case Iex_Load:
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.Load.addr, mode, guard);
         wb_mem_trace(current, sbOut, expr->Iex.Load.addr, mode, guard, expr, wb_end(expr->Iex.Load.end));
         break;
      case Iex_Const: break;
      case Iex_ITE:
         // NB: iftrue and iffalse are evaluated in both cases by VEX!
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.ITE.cond, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.ITE.iftrue, mode, guard);
         wb_mem_walk_iexpr(current, sbOut, expr->Iex.ITE.iffalse, mode, guard);
         break;
      case Iex_CCall:
         for (i = 0; expr->Iex.CCall.args[i]; ++i)
            wb_mem_walk_iexpr(current, sbOut, expr->Iex.CCall.args[i], mode, guard);
         break;
      case Iex_VECRET: break;
      case Iex_GSPTR: break;
      default:
         VG_(fmsg)("Unknown IRExpr tag: %#x\n", (unsigned) expr->tag);
         ppIRExpr(expr);
         VG_(printf)("\n");
         break;
   }
}

// CAS handling
static IRExpr* wb_make_cas_guard(IRSB* sbOut, const IRCAS* cas)
{
   // Get type and size of CAS operation
   IRType type = typeOfIRExpr(sbOut->tyenv, cas->dataLo);
   Int opSize = sizeofIRType(type);
   // Get type and size of pointers, and produce correct offset IRConst
   IRType ptrType = typeOfIRExpr(sbOut->tyenv, cas->addr);
   IRConst offsetConst;
   IROp ptrAddOp;
   switch (sizeofIRType(ptrType)) {
      case 1: ptrAddOp = Iop_Add8; offsetConst.tag = Ico_U8; offsetConst.Ico.U8 = opSize; break;
      case 2: ptrAddOp = Iop_Add16; offsetConst.tag = Ico_U16; offsetConst.Ico.U16 = opSize; break;
      case 4: ptrAddOp = Iop_Add32; offsetConst.tag = Ico_U32; offsetConst.Ico.U32 = opSize; break;
      case 8: ptrAddOp = Iop_Add64; offsetConst.tag = Ico_U64; offsetConst.Ico.U64 = opSize; break;
      default:
         VG_(tool_panic)("Cannot handle pointer sizes != 1/2/4/8 bytes\n");
         break;
   }
   // Make an IRExpr from the offset
   IRExpr* offset = IRExpr_Const(deepCopyIRConst(&offsetConst));
   // Compute correct addresses for CAS halves
   IRExpr* addrHi = (cas->end == Iend_BE ? cas->addr : IRExpr_Binop(ptrAddOp, cas->addr, offset));
   IRExpr* addrLo = (cas->end == Iend_LE ? cas->addr : IRExpr_Binop(ptrAddOp, cas->addr, offset));
   // Select correct comparison operation
   IROp cmpOp;
   switch (opSize) {
      case 1: cmpOp = Iop_CmpEQ8; break;
      case 2: cmpOp = Iop_CmpEQ16; break;
      case 4: cmpOp = Iop_CmpEQ32; break;
      case 8: cmpOp = Iop_CmpEQ64; break;
      default:
         VG_(tool_panic)("Cannot handle CAS of size != 1/2/4/8 bytes\n");
         break;
   }
   // Produce guard expression
   IRExpr* guard = IRExpr_Binop(cmpOp, wb_make_atom(sbOut, IRExpr_Load(cas->end, type, addrLo)), cas->expdLo);
   if (cas->expdHi) {
      IRExpr* hiGuard = IRExpr_Binop(cmpOp, wb_make_atom(sbOut, IRExpr_Load(cas->end, type, addrHi)), cas->expdHi);
      guard = IRExpr_Binop(Iop_And1, wb_make_atom(sbOut, guard), wb_make_atom(sbOut, hiGuard));
   }
   return guard;
}

#define WB_MAKE_CAS_HELPER(width, argt) \
   static VG_REGPARM(2) void* wb_make_cas_value_helper_single_##width(argt oldLo, argt dataLo) \
   { \
      char* value = VG_(malloc)("wb_make_cas_value_helper", (width) * 2 + 2); \
      tl_assert(value); \
      value[0] = 1; /* single */ \
      value[1] = (width); /* width */ \
      VG_(memcpy)(&value[2], &oldLo, sizeof(argt)); \
      VG_(memcpy)(&value[2 + sizeof(argt)], &dataLo, sizeof(argt)); \
      return value; \
   } \
   static void* wb_make_cas_value_helper_pair_##width(argt oldLo, argt oldHi, argt dataLo, argt dataHi) \
   { \
      char* value = VG_(malloc)("wb_make_cas_value_helper", (width) * 4 + 2); \
      tl_assert(value); \
      value[0] = 2; /* pair */ \
      value[1] = (width); /* width */ \
      VG_(memcpy)(&value[2], &oldLo, sizeof(argt)); \
      VG_(memcpy)(&value[2 + sizeof(argt)], &oldHi, sizeof(argt)); \
      VG_(memcpy)(&value[2 + 2 * sizeof(argt)], &dataLo, sizeof(argt)); \
      VG_(memcpy)(&value[2 + 3 * sizeof(argt)], &dataHi, sizeof(argt)); \
      return value; \
   }
WB_MAKE_CAS_HELPER(1, vki_uint8_t)
WB_MAKE_CAS_HELPER(2, vki_uint16_t)
WB_MAKE_CAS_HELPER(4, vki_uint32_t)
WB_MAKE_CAS_HELPER(8, vki_uint64_t)

static IRExpr* wb_make_cas_value(IRSB* sbOut, const IRCAS* cas)
{
   // Merge CAS values (single/pair, individual width, (oldLo, oldHi), (dataLo, dataHi)).
   // This is the value that is _read_ followed by the value that is _written_ if the guard matches
   // This is a little complicated, which is why we have this tagged separately as AccessCAS.
   // Since we don't want to adjust the API for everything else, instead insert an Iex_CCall expression
   // that returns this value as described.
   IRType hostPointerType = integerIRTypeOfSize(sizeof(void*));
   IRType type = typeOfIRExpr(sbOut->tyenv, cas->dataLo);
   Int width = sizeofIRType(type);
   void* fn;
   IRCallee* callee;
   IRExpr** args;
   if (cas->dataHi) {
      switch (width) {
         case 1: fn = wb_make_cas_value_helper_pair_1; break;
         case 2: fn = wb_make_cas_value_helper_pair_2; break;
         case 4: fn = wb_make_cas_value_helper_pair_4; break;
         case 8: fn = wb_make_cas_value_helper_pair_8; break;
         default:
            VG_(tool_panic)("Cannot handle CAS of size != 1/2/4/8 bytes\n");
            break;
      }
      callee = mkIRCallee(0, "wb_make_cas_value_helper", VG_(fnptr_to_fnentry)(fn));
      // Fix up order in big-endian systems
      if (cas->end == Iend_BE) {
         args = mkIRExprVec_4(
            wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_RdTmp(cas->oldHi), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_RdTmp(cas->oldLo), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, cas->dataHi), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, cas->dataLo), type))
         );
      } else {
         args = mkIRExprVec_4(
            wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_RdTmp(cas->oldLo), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_RdTmp(cas->oldHi), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, cas->dataLo), type)),
            wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, cas->dataHi), type))
         );
      }
   } else {
      switch (width) {
         case 1: fn = wb_make_cas_value_helper_single_1; break;
         case 2: fn = wb_make_cas_value_helper_single_2; break;
         case 4: fn = wb_make_cas_value_helper_single_4; break;
         case 8: fn = wb_make_cas_value_helper_single_8; break;
         default:
            VG_(tool_panic)("Cannot handle CAS of size != 1/2/4/8 bytes\n");
            break;
      }
      callee = mkIRCallee(2, "wb_make_cas_value_helper", VG_(fnptr_to_fnentry)(fn));
      args = mkIRExprVec_2(
         wb_make_atom(sbOut, wb_to_ptr_width(IRExpr_RdTmp(cas->oldLo), type)),
         wb_make_atom(sbOut, wb_to_ptr_width(wb_make_atom(sbOut, cas->dataLo), type))
      );
   }
   return IRExpr_CCall(callee, hostPointerType, args);
}

// Instrumentation
static
IRSB* wb_instrument ( VgCallbackClosure* closure,
                      IRSB* bb,
                      const VexGuestLayout* layout,
                      const VexGuestExtents* vge,
                      const VexArchInfo* archinfo_host,
                      IRType gWordTy, IRType hWordTy )
{
   Int i, j;
   IRSB* sbOut;
   Addr current = 0;
   Bool passthrough;

   /* Set up a copy of the input SB without the statements */
   sbOut = deepCopyIRSBExceptStmts(bb);

   // Copy verbatim any IR preamble preceding the first IMark
   i = 0;
   while(i < bb->stmts_used && bb->stmts[i]->tag != Ist_IMark) {
      addStmtToIRSB(sbOut, bb->stmts[i]);
      i++;
   }

   for (/* use current i */; i < bb->stmts_used; i++) {
      IRStmt* st = bb->stmts[i];

      // if statement is noop, ignore it
      if(!st || st->tag == Ist_NoOp) continue;

      passthrough = True;
      switch(st->tag) {
         // Ist_NoOp: no-op
         // Ist_IMark: Start of next raw machine instruction
         case Ist_IMark: {
               current = st->Ist.IMark.addr + st->Ist.IMark.delta;
               if (wb_options.enable_pc_trace) {
                  // call dirty helper to log trace number and address to file.
                  wb_pc_trace(current, sbOut);
               }
            }
            break;
         // Ist_AbiHint: ABI metadata (e.g. undefined memory)
         // Ist_Put: Register write
         case Ist_Put: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.Put.data, AccessRead, NULL);
            }
            break;
         // Ist_PutI: Indirect register write
         case Ist_PutI: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.PutI.details->ix, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.PutI.details->data, AccessRead, NULL);
            }
            break;
         // Ist_WrTmp: SSA temporary write
         case Ist_WrTmp: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.WrTmp.data, AccessRead, NULL);
            }
            break;
         // Ist_Store: Unconditional memory write
         case Ist_Store: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.Store.addr, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.Store.data, AccessRead, NULL);
               wb_mem_trace(current, sbOut, st->Ist.Store.addr, AccessWrite, NULL, st->Ist.Store.data, wb_end(st->Ist.Store.end));
            }
            break;
         // Ist_StoreG: Guarded memory write
         case Ist_StoreG: if (wb_options.enable_mem_trace) {
               // addr and data are fully evaluated regardless of the guard.
               // This means we need to check all three for loads.
               wb_mem_walk_iexpr(current, sbOut, st->Ist.StoreG.details->addr, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.StoreG.details->data, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.StoreG.details->guard, AccessRead, NULL);
               wb_mem_trace(current, sbOut, st->Ist.StoreG.details->addr, AccessWrite, st->Ist.StoreG.details->guard, st->Ist.StoreG.details->data, wb_end(st->Ist.StoreG.details->end));
            }
            break;
         // Ist_LoadG: Guarded memory load
         case Ist_LoadG: if (wb_options.enable_mem_trace) {
               // Both addr and alt are always fully evaluated.
               wb_mem_walk_iexpr(current, sbOut, st->Ist.LoadG.details->addr, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.LoadG.details->alt, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.LoadG.details->guard, AccessRead, NULL);
               // Loading of `alt` if the guard evaluates to false doesn't matter because it is
               // always evaluated anyways.
               IRType loadType, resType;
               typeOfIRLoadGOp(st->Ist.LoadG.details->cvt, &resType, &loadType);
               IRExpr* value = IRExpr_Load(st->Ist.LoadG.details->end, loadType, st->Ist.LoadG.details->addr);
               wb_mem_trace(current, sbOut, st->Ist.LoadG.details->addr, AccessRead, st->Ist.LoadG.details->guard, value, wb_end(st->Ist.LoadG.details->end));
            }
            break;
         // Ist_CAS: Compare-and-swap
         case Ist_CAS: if (wb_options.enable_mem_trace) {
               // expdLo and expdHi must be evaluated to check whether to actually swap.
               wb_mem_walk_iexpr(current, sbOut, st->Ist.CAS.details->addr, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.CAS.details->expdLo, AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.CAS.details->expdHi, AccessRead, NULL);
               // TODO: Are dataLo and dataHi really always evaluated?
               // For now, just log but do not instrument (this may be transformed far enough so there
               // are never any loads in data* anyways, in which case this won't cause any harm).
               wb_mem_walk_iexpr(current, sbOut, st->Ist.CAS.details->dataLo, LogOnly, NULL);
               wb_mem_walk_iexpr(current, sbOut, st->Ist.CAS.details->dataHi, LogOnly, NULL);
               IRExpr* casGuard = wb_make_cas_guard(sbOut, st->Ist.CAS.details);
               // Pass through statement early
               addStmtToIRSB(sbOut, st);
               passthrough = False;
               // Compute CAS value
               IRExpr* casValue = wb_make_cas_value(sbOut, st->Ist.CAS.details);
               wb_mem_trace(current, sbOut, st->Ist.CAS.details->addr, AccessCAS, casGuard, casValue, wb_end(st->Ist.CAS.details->end));
            }
            break;
         // Ist_LLSC: Load-linked or conditional store
         case Ist_LLSC: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.LLSC.addr, AccessRead, NULL);
               IRExpr* value;
               if (st->Ist.LLSC.storedata != NULL) {
                  value = st->Ist.LLSC.storedata;
                  wb_mem_walk_iexpr(current, sbOut, st->Ist.LLSC.storedata, AccessRead, NULL);
               } else {
                  value = IRExpr_Load(st->Ist.LLSC.end, typeOfIRTemp(sbOut->tyenv, st->Ist.LLSC.result), st->Ist.LLSC.addr);
               }
               // TODO: How can we check that the store actually happens?
               wb_mem_trace(current, sbOut, st->Ist.LLSC.addr, st->Ist.LLSC.storedata == NULL ? AccessRead : AccessWrite, NULL, value, wb_end(st->Ist.LLSC.end));
            }
            break;
         // Ist_Dirty: C call with side effects
         case Ist_Dirty: if (wb_options.enable_mem_trace) {
               // Args and mFx are evaluated regardless, with the guard _after_ mFx and the args.
               IRExpr* dirtyGuard = st->Ist.Dirty.details->guard;
               IREffect mFx = st->Ist.Dirty.details->mFx;
               IRExpr* mAddr = st->Ist.Dirty.details->mAddr;
               if (mFx != Ifx_None) {
                  // mAddr is read/written/modified
                  // TODO: This assumes "modify" is r-then-w. Is this correct?
                  wb_mem_walk_iexpr(current, sbOut, mAddr, AccessRead, NULL);
                  if (mFx == Ifx_Read || mFx == Ifx_Modify)
                     wb_mem_trace(current, sbOut, mAddr, AccessRead, dirtyGuard, NULL, UnknownEndianness);
                  if (mFx == Ifx_Write || mFx == Ifx_Modify)
                     wb_mem_trace(current, sbOut, mAddr, AccessWrite, dirtyGuard, NULL, UnknownEndianness);
               }
               for (j = 0; st->Ist.Dirty.details->args[j]; ++j)
                  wb_mem_walk_iexpr(current, sbOut, st->Ist.Dirty.details->args[j], AccessRead, NULL);
               wb_mem_walk_iexpr(current, sbOut, dirtyGuard, AccessRead, NULL);
            }
            break;
         // Ist_MBE: Memory bus events (e.g. fence)
         // Ist_Exit: Conditional exit from the IRSB
         case Ist_Exit: if (wb_options.enable_mem_trace) {
               wb_mem_walk_iexpr(current, sbOut, st->Ist.Exit.guard, AccessRead, NULL);
            }
            break;
         default:
            break;
      }

      // always pass through original instruction
      if (passthrough)
         addStmtToIRSB(sbOut, st);
   }

   return sbOut;
}

static void wb_fini(Int exitcode)
{
   if (wb_options.enable_pc_trace) {
      trace_flush(&pc_trace);
      if (pc_trace.fd != mem_trace.fd)
          VG_(close)(pc_trace.fd);
   }
   if (wb_options.enable_mem_trace) {
      trace_flush(&mem_trace);
      VG_(close)(mem_trace.fd);
   }
}

static Bool wb_handle_client_request(ThreadId tid, UWord *argv, UWord *ret)
{
   if (!wb_options.enable_client_requests)
      return False;
   switch (argv[0]) {
      case VG_USERREQ__START_TRACING:
         VG_(printf)("Client requested tracing start\n");
         wb_record = True;
         *ret = 0;
         return True;
      case VG_USERREQ__END_TRACING:
         VG_(printf)("Client requested tracing end\n");
         wb_record = False;
         *ret = 0;
         return True;
      default:
         return False;
   }
}

static Bool wb_process_cmd_line_option(const HChar* arg)
{
   if VG_STR_CLO(arg, "--sock", wb_options.socket) {}
   if VG_STR_CLO(arg, "--out", wb_options.out_dir) {}
   if VG_STR_CLO(arg, "--out-filename", wb_options.out_filename_template) {}
   if VG_BOOL_CLO(arg, "--client-requests", wb_options.enable_client_requests) {}
   if VG_BOOL_CLO(arg, "--trace-instructions", wb_options.enable_pc_trace) {}
   if VG_BOOL_CLO(arg, "--trace-memory", wb_options.enable_mem_trace) {}
   if VG_BINT_CLO(arg, "--buffer-size", wb_options.buffer_size, 1024, 0xFFFFFFFFFFFFFFFF) {}
   if VG_BHEX_CLO(arg, "--start", wb_options.start, 0, 0xFFFFFFFFFFFFFFFF) {}
   else if VG_BHEX_CLO(arg, "--end", wb_options.end, 0, 0xFFFFFFFFFFFFFFFF)  {}
   else
      return False;

   return True;
}

static void wb_print_usage(void)
{
   VG_(printf)(
"    --start=0x...                    start address (optional)\n"
"    --end=0x...                      end address (optional)\n"
"    --client-requests=yes|no         client can signal start and end of tracing\n"
"    --buffer-size=...                buffer size in bytes (default: 33554432 (32 MiB))\n"
"    --sock=path/to/socket.sock       wrapper socket (optional)\n"
"    --out=outdir/                    output directory (optional)\n"
"    --out-filename=...               output filename (default: %%T-trace-%%t-%%p-%%P.bin)\n"
"                                     supports the following substitutions:\n"
"                                       %%T: trace type (mandatory)\n"
"                                       %%t: timestamp\n"
"                                       %%P: program name\n"
"                                       %%p: PID\n"
"                                       %%%%: a literal %%\n"
"                                     ignored if --out is not given\n"
"    --trace-instructions=yes|no      trace instructions (default: yes)\n"
"    --trace-memory=yes|no            trace memory accesses (default: yes)\n"
   );
}

static void wb_print_debug_usage(void)
{
   VG_(printf)(
"    (none)\n"
   );
}

static void wb_pre_clo_init(void)
{
   VG_(details_name)            ("whiteboxgrind");
   VG_(details_version)         (NULL);
   VG_(details_description)     ("A Valgrind tool to break AES whitebox encryption");
   VG_(details_copyright_author)(
      "Copyright (C) 2020 Katharina Bogad, Tobias Holl");
   VG_(details_bug_reports_to)  (VG_BUGS_TO);

   VG_(details_avg_translation_sizeB) ( 275 );

   VG_(basic_tool_funcs)        (wb_post_clo_init,
                                 wb_instrument,
                                 wb_fini);

   VG_(needs_command_line_options)(wb_process_cmd_line_option,
                                 wb_print_usage,
                                 wb_print_debug_usage);

   VG_(needs_client_requests)   (wb_handle_client_request);

   /* No needs, no core events to track */
}

VG_DETERMINE_INTERFACE_VERSION(wb_pre_clo_init)

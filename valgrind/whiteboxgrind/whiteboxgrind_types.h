#ifndef __WB_TYPES_H
#define __WB_TYPES_H

#ifndef USE_VALGRIND_TYPES
#define ULong unsigned long
#define UInt unsigned int
#define UChar unsigned char
#endif

#define PROTOCOL_VERSION 1
enum DataType {
   DataTypePCTrace,
   DataTypeMemoryTrace,
};

enum Endianness {
   UnknownEndianness,
   LittleEndian,
   BigEndian,
};

enum AccessMode {
   AccessRead = 0,
   AccessWrite = 1,
   AccessCAS = 2,
   LogOnly = 255
};

struct PCTraceEntry {
   ULong sync;
   ULong pc;
} __attribute__((__packed__));

struct MemoryTraceEntry {
   ULong sync;
   ULong pc;
   ULong addr;
   UInt width;
   UChar endianness;
   UChar mode;
   UChar value[34];
} __attribute__((__packed__));

struct MessagePrefix {
   UChar version;
   UChar type;
   UInt length;
} __attribute__((__packed__));

#endif // __WB_TYPES_H

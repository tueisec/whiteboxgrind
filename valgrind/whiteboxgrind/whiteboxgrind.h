#ifndef __WB_API_H
#define __WB_API_H

#include "valgrind.h"

typedef enum {
   VG_USERREQ__START_TRACING = VG_USERREQ_TOOL_BASE('W', 'B'),
   VG_USERREQ__END_TRACING
} Vg_WhiteboxClientRequest;

#define WHITEBOX_START \
   VALGRIND_DO_CLIENT_REQUEST_STMT(VG_USERREQ__START_TRACING, 0, 0, 0, 0, 0)

#define WHITEBOX_END \
   VALGRIND_DO_CLIENT_REQUEST_STMT(VG_USERREQ__END_TRACING, 0, 0, 0, 0, 0)

#endif // __WB_API_H

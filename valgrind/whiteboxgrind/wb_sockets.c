#include "wb_sockets.h"

#include "pub_tool_vkiscnums.h"

// Unfortunately, all the syscall stuff is inaccessible to tools, and I _really_ don't want to have
// to port all the stuff from coregrind/m_syscall.c
// This is why this version is Linux/x86 only - add more whenever you want

#if defined(VGP_x86_linux)
    #define WB_SYSCALL_ASM_3 \
      "int $0x80\n"
    #define WB_SYSCALL_CLOBBER_3 "cc", "memory"
    #define WB_SYSCALL_OUTPUTS_3(_res) \
       [result]"=a"(_res)
    #define WB_SYSCALL_CONSTRAINTS_3(_nr, _a1, _a2, _a3) \
       [number]"a"(_nr), [a1]"b"(_a1), [a2]"c"(_a2), [a3]"d"(_a3)
#elif defined(VGP_amd64_linux)
   #define WB_SYSCALL_ASM_3 \
      "syscall\n"
   #define WB_SYSCALL_CLOBBER_3 "rcx", "r11", "cc", "memory"
   #define WB_SYSCALL_OUTPUTS_3(_res) \
      [result]"=a"(_res)
   #define WB_SYSCALL_CONSTRAINTS_3(_nr, _a1, _a2, _a3) \
      [number]"a"(_nr), [a1]"D"(_a1), [a2]"S"(_a2), [a3]"d"(_a3)
#else
   #error Unsupported architecture, please adjust wb_sockets.c
#endif

#define WB_RETURN_SYSCALL_3(_type, _nr, _a1, _a2, _a3) do { \
   UWord result; \
   UWord a1 = (UWord) _a1, a2 = (UWord) _a2, a3 = (UWord) _a3; \
   __asm__ volatile ( \
      WB_SYSCALL_ASM_3 \
      : WB_SYSCALL_OUTPUTS_3(result) \
      : WB_SYSCALL_CONSTRAINTS_3(_nr, a1, a2, a3) \
      : WB_SYSCALL_CLOBBER_3 \
   ); \
   return (_type) result; \
} while (__builtin_unreachable(), 0)

Int VG_(connect)(Int sockfd, const struct vki_sockaddr* serv_addr, Int addrlen)
{
   WB_RETURN_SYSCALL_3(Int, __NR_connect, sockfd, (UWord) serv_addr, addrlen);
}

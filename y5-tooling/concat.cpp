// Concatenate Y5 files without touching the data
// This is a little hacky but should do the job
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <iostream>
#include <vector>

#include <fcntl.h>
#include <unistd.h>

#include <y5.h>

#define CHECK(cond) do { if (!(cond)) { throw std::runtime_error("Assertion failed: " #cond); } } while (0)

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0] << " <outfile> <infiles...>\n";
        return 1;
    }

    std::vector<y5::format::header> headers;
    y5::format::header header;

    {
        y5::reader r(argv[2]);
        header = r.header();
        headers.push_back(r.header());
    }
    header.toc_capacity = y5::format::DEFAULT_TOC_CAPACITY;
    header.toc_resolution = 1;
    for (int i = 3; i < argc; ++i)
    {
        y5::reader r(argv[i]);
        CHECK(r.header().entry_size == header.entry_size);
        CHECK((r.header().flags & y5::format::header::transposed) == (header.flags & y5::format::header::transposed));
        CHECK((r.header().flags & y5::format::header::uncompressed) == (header.flags & y5::format::header::uncompressed));

        if (r.header().max_columns != header.max_columns)
        {
            header.flags = (y5::format::header::bitflags) (((int) header.flags) | y5::format::header::ragged);
            if (r.header().max_columns > header.max_columns)
                header.max_columns = r.header().max_columns;
        }

        header.rows += r.header().rows;
        headers.push_back(r.header());
    }

    /* Header is ready. Fix up TOC size. */
    std::vector<y5::format::toc_entry> toc(header.toc_capacity);
    while (header.rows / header.toc_resolution > header.toc_capacity)
        header.toc_resolution <<= 1;
    auto toc_bytes = toc.size() * sizeof(toc[0]);

    /* Write header */
    int outfd = open(argv[1], O_WRONLY | O_CREAT | O_EXCL, 0644);
    CHECK(outfd != -1);
    CHECK(write(outfd, &header, sizeof(header)) == sizeof(header));

    /* Write TOC space */
    CHECK(write(outfd, toc.data(), toc_bytes) == toc_bytes);

    /* Start processing files */
    size_t row = 0;
    for (int i = 2; i < argc; ++i)
    {
        int infd = open(argv[i], O_RDONLY);
        off_t data = sizeof(header) + headers[i-2].toc_capacity * sizeof(y5::format::toc_entry);
        CHECK(infd != -1);
        CHECK(lseek(infd, data, SEEK_SET) == data);

        for (size_t in_row = 0; in_row < headers[i-2].rows; ++in_row, ++row)
        {
            if (row % header.toc_resolution == 0)
                toc[row / header.toc_resolution].offset = lseek(outfd, 0, SEEK_CUR);
            y5::format::row_header rh;
            CHECK(read(infd, &rh, sizeof(rh)) == sizeof(rh));
            size_t to_copy = rh.compressed_size + sizeof(rh);
            CHECK(copy_file_range(infd, &data, outfd, nullptr, to_copy, 0) == to_copy);
            CHECK(lseek(infd, rh.compressed_size, SEEK_CUR) != -1);
        }
        close(infd);
    }

    /* Write TOC */
    CHECK(lseek(outfd, sizeof(header), SEEK_SET) == sizeof(header));
    CHECK(write(outfd, toc.data(), toc_bytes) == toc_bytes);
    close(outfd);
}

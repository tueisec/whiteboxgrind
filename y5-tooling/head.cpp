// Grab the first few rows of a Y5 file
#include <y5.h>

#include <iostream>

#define BUFSZ 8192
int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cerr << "Usage: " << argv[0] << " <infile> <rows> <outfile>\n";
        return 1;
    }
    std::string infile(argv[1]);
    std::string outfile(argv[3]);
    int rows = atoi(argv[2]);
    if (rows <= 0)
    {
        std::cerr << "Bad row count: " << rows << "\n";
        return 1;
    }

    y5::reader r(infile);
    y5::writer w(outfile,y5::writer_mode::OVERWRITE,r.header().entry_size,r.header().flags,r.header().toc_capacity);
    auto buf = new unsigned char[BUFSZ * r.header().entry_size];
    for (int row = 0; row < rows; ++row)
    {
        for (;;)
        {
            auto [count, done] = r.read_buffer(buf, BUFSZ);
            w.append_buffer(buf, count);
            if (done)
                break;
        }
        w.end_row();
    }
}

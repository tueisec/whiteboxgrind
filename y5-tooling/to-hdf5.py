#!/usr/bin/python

import argparse
import h5py
import numpy
import y5

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('y5file', help='The Y5 file to convert')
    parser.add_argument('output', help='The HDF5 file to write to')
    parser.add_argument('-f', '--overwrite', help='Overwrite output file', action='store_true')
    parser.add_argument('--dataset', help='Dataset name', default='y5')
    parser.add_argument('--group', help='Group path')
    parser.add_argument('--buffer-size', help='Buffer size', type=int, default=8192)
    parser.add_argument('datatype', help='Python expression that evaluates to a numpy.dtype')
    args = parser.parse_args()

    assert args.buffer_size > 0

    reader = y5.reader(args.y5file)

    rows = reader.rows
    cols = reader.max_columns

    out = h5py.File(args.output, 'w' if args.overwrite else 'a')
    if args.group is None:
        container = out
    elif args.group in out:
        container = out[args.group]
    else:
        container = out.create_group(args.group)

    dtype = eval(args.datatype)
    assert reader.entry_size == dtype.itemsize
    shape = (rows, cols)
    dataset = container.create_dataset(args.dataset, shape, dtype, compression="gzip", maxshape=shape)

    for row in range(reader.rows):
        col = 0
        while True:
            entries, done = reader.read(args.buffer_size)
            count = entries.shape[-1]
            dataset[row:row+1, col:col+count] = entries.view(dtype=dtype)
            col += count
            if done:
                break

    out.close()

#ifndef Y5_H
#define Y5_H

#include <cstdint>
#include <filesystem>
#include <memory>
#include <string>
#include <type_traits>

#if !defined(Y5_NO_ASSERTS) && !defined(Y5_NO_EXCEPTIONS)
    #include <stdexcept>
#elif !defined(Y5_NO_ASSERTS)
    #include <cstdio>
    #include <cstdlib>
#endif

namespace y5
{
    namespace detail
    {
        // Assertions

#if !defined(Y5_NO_ASSERTS) && !defined(Y5_NO_EXCEPTIONS)
        // By default, use exceptions to handle assertion failures
        class failed_assertion : public std::logic_error
        {
        public:
            failed_assertion(const std::string &check, const std::string &message)
                : std::logic_error(std::string { "Assertion failed: " } + check + ": " + message)
            {}
        };
        #define Y5_ASSERT_FAIL(check, message) \
            throw ::y5::detail::failed_assertion(check, message)
#elif !defined(Y5_NO_ASSERTS)
        // However, some people like to compile without exception support
        // Here's an exception-free version - not sure how useful that will be given
        // the internal use of std::function and other things, but I suppose it does
        // no harm either.
        #define Y5_ASSERT_FAIL(check, message) \
            do { fprintf(stderr, "Assertion failed: %s: %s\n", check, message); _Exit(1); } while (0)
#endif

#if !defined(Y5_NO_ASSERTS)
        // Dispatch to Y5_ASSERT_FAIL if the assertion fails
        #define Y5_ASSERT(value, message) \
            do { if (!(value)) { Y5_ASSERT_FAIL(#value, (message)); } } while (0)
#else
        // Assertions are disabled, but compute side effects anyways
        // (Checks _should_ never have side effects, but we want to make sure)
        #define Y5_ASSERT(value, message) \
            do { (void) (value); } while (0)
#endif

        // Make a type conditionally-copyable
        struct empty_base {};
        struct noncopyable
        {
            noncopyable() = default;
            noncopyable(const noncopyable &) = delete;
            noncopyable(noncopyable &&) = default;
            noncopyable &operator=(const noncopyable &) = delete;
            noncopyable &operator=(noncopyable &&) = default;
            ~noncopyable() = default;
        };

        // Raw file descriptors
        using fd_t = int;
        constexpr inline fd_t INVALID_FD = -1;

        // Wraps a file descriptor with the appropriate operations (close, dup, ...)
        // FDs that we are not supposed to copy are protected from misuse at compiletime.
        template <bool Copyable>
        class fd_wrapper : private std::conditional_t<Copyable, empty_base, noncopyable>
        {
        public:
            fd_wrapper(fd_t fd);
            ~fd_wrapper();
            fd_wrapper(const fd_wrapper &);
            fd_wrapper(fd_wrapper &&);
            fd_wrapper &operator=(const fd_wrapper &);
            fd_wrapper &operator=(fd_wrapper &&);

        protected:
            fd_t m_fd;
        };

        class compressor;
        class decompressor;
    } // namespace detail

    namespace performance
    {
        struct tuneables
        {
            #ifndef Y5_MAXIMUM_OVERALLOCATION
            #define Y5_MAXIMUM_OVERALLOCATION 0x2000000 /* 32 MiB */
            #endif
            //! How much should we grow a file by at a single time, even if we don't end up
            //! using that memory (it will be returned to the disk later if it is not needed,
            //! but if you are low on disk space, this may be a useful parameter).
            std::size_t maximum_overallocation = Y5_MAXIMUM_OVERALLOCATION;

            #ifndef Y5_MAXIMUM_HELD_BUFFER
            #define Y5_MAXIMUM_HELD_BUFFER 0x8000000 /* 128 MiB */
            #endif
            //! Once a large part of our internal buffer is used, we will start freeing it
            //! from the start - how much memory should we cache in that way?
            //! This mostly affects RAM usage.
            std::size_t maximum_held_buffer = Y5_MAXIMUM_HELD_BUFFER;

            #ifndef Y5_INITIAL_BUFFER_SIZE
            #define Y5_INITIAL_BUFFER_SIZE 0x10000 /* 64 KiB */
            #endif
            //! This controls the initial size of the buffer. If you have lots of entries
            //! close to that size, there will be fewer reallocation requests and less overhead.
            std::size_t initial_buffer_size = Y5_INITIAL_BUFFER_SIZE;

            enum compression_levels : uint8_t
            {
                compression_fastest = 0x1,
                compression_faster  = 0x4,
                compression_default = 0x8,
                compression_better  = 0xc,
                compression_best    = 0xf,
            };
            #ifndef Y5_COMPRESSION_LEVEL
            #define Y5_COMPRESSION_LEVEL compression_default
            #endif
            //! How well to compress the data. Higher values mean slower but better compression.
            //! Note that the underlying compression library's default settings may differ from
            //! our defaults.
            compression_levels compression_level = Y5_COMPRESSION_LEVEL;
        };
    } // namespace performance

    namespace format
    {
        //! File format identifier
        constexpr inline std::uint32_t MAGIC = 0x056c6f79;
        constexpr inline std::uint8_t  VERSION = 1;
        constexpr inline std::uint32_t DEFAULT_TOC_CAPACITY = 128;

        //! File header
        struct alignas(1) [[gnu::packed]] header
        {
            enum bitflags : std::uint8_t
            {
                none = 0x00,
                transposed = 0x01, // If set, traces are stored in columns
                ragged = 0x02, // If set, not all traces have the same number of entries
                uncompressed = 0x04, // If set, compression is replaced with a simple memcpy.
                unused = 0xf8, // Other bits
            };

            std::uint32_t magic;
            std::uint8_t  version;
            bitflags      flags;
            std::uint16_t entry_size; // Includes padding
            std::uint64_t rows;
            std::uint64_t max_columns;
            std::uint32_t toc_resolution;
            std::uint32_t toc_capacity;
            // ... followed by toc_entry toc[toc_capacity], in which every
            // toc_resolution'th row is stored.
        };

        //! TOC entry
        struct alignas(1) [[gnu::packed]] toc_entry
        {
            std::uint64_t offset;
        };

        //! Block header
        struct alignas(1) [[gnu::packed]] row_header
        {
            std::uint64_t compressed_size;
            std::uint64_t columns;
        };
    } // namespace format

    // By default, files are not threadsafe in C++ (we only have one file descriptor,
    // and that is shared across threads). For performance reasons, we still want to
    // allow efficient parallel reads (in general across multiple rows of data). This
    // leads to the following design decisions:
    //   - Each row of data is compressed separately, so that separate decompression
    //     streams can be used simultaneously.
    //   - Data is _always_ written row-by-row (this leads to the data being transposed
    //     after filtering).
    //   - There is only ever one writer for a file (keeping write locations consistent
    //     in a compressed format is almost impossible).
    //   - There is no explicit support for modifying y5 files (modifying compressed
    //     data in a file is nontrivial, and we may need to shift around lots of
    //     additional data).
    // To write to a file, obtain a y5::writer.
    // To read from a file (possibly in parallel), obtain a y5::reader.

    enum class writer_mode { OVERWRITE, EXCLUSIVE };

    class writer : detail::fd_wrapper</* Copyable = */ false>
    {
    public:
        //! Create a new writer.
        writer(const std::filesystem::path &path, writer_mode mode, std::size_t entry_size, format::header::bitflags flags = format::header::bitflags::none, std::uint32_t toc_capacity = format::DEFAULT_TOC_CAPACITY, performance::tuneables tune = {});

        //! Modify performance settings. Note that these will only become active
        //! with the start of the next row.
        void tune(performance::tuneables tune = {});

        //! Append data to the current row.
        template <typename T>
        void append(const T *data, std::size_t count)
        {
            Y5_ASSERT(sizeof(T) == m_header->entry_size, "Type size does not match entry size");
            append_buffer(reinterpret_cast<const unsigned char *>(data), count);
        }

        //! Append data from a raw memory buffer to the current row
        void append_buffer(const unsigned char *data, std::size_t number_of_entries);

        //! End the current row.
        void end_row();

    private:
        void append_bytes(const unsigned char *data, std::size_t size);
        void start_row();

        std::shared_ptr<format::header> m_header;
        std::shared_ptr<format::toc_entry> m_toc;
        std::shared_ptr<format::row_header> m_current;
        std::shared_ptr<detail::compressor> m_compressor;
        performance::tuneables m_tune;
    };

    class reader : detail::fd_wrapper</* Copyable = */ true>
    {
    public:
        //! Create a new reader
        reader(const std::filesystem::path &path, performance::tuneables tune = {});

        //! Modify performance settings. Note that these will only become active
        //! with the start of the next row.
        void tune(performance::tuneables tune = {});

        //! Indicates how many entries were read, and whether the row is done.
        struct read_result { std::size_t count; bool row_done; };

        //! Reads only from the current row, returns the number of entries read and
        //! whether the row is over
        //! Once the row is over, the next read will start reading the next row.
        template <typename T>
        read_result read(T *into, std::size_t count)
        {
            Y5_ASSERT(sizeof(T) == m_header->entry_size, "Type size does not match entry size");
            return read_buffer(reinterpret_cast<unsigned char *>(into), count);
        }

        //! Reads entries from the current row into a raw memory buffer.
        //! This is useful if you are not working with entries of a known type.
        read_result read_buffer(unsigned char *into, std::size_t number_of_entries);

        //! Access the file headers
        const format::header &header() const;
        //! Access the specified TOC entry (NB: index is not the same as the row number)
        const format::toc_entry &toc_entry(std::size_t index) const;
        //! Access the current row header
        const format::row_header &row_header() const;
        //! Get the index of the current row
        const std::size_t row_index() const;

        //! Seeks the file to the specified row
        void seek(std::size_t row);

        // Reader for a row
        class row_reader
        {
        public:
            //! A row_reader can only be obtained via reader::detach
            //! This is public so make_shared works, but don't rely on this API being stable.
            row_reader(std::shared_ptr<format::header>, std::shared_ptr<format::row_header>, std::shared_ptr<detail::decompressor>, std::size_t);

            //! Reads from the current row, returns the number of entries read and
            //! whether the row is over.
            //! Further reads from this row_reader will fail.
            template <typename T>
            read_result read(T *into, std::size_t count)
            {
                Y5_ASSERT(sizeof(T) == m_header->entry_size, "Type size does not match entry size");
                return read_buffer(reinterpret_cast<unsigned char *>(into), count);
            }

            //! Reads entries from the current row into a raw memory buffer.
            //! This is useful if you are not working with entries of a known type.
            read_result read_buffer(unsigned char *into, std::size_t number_of_entries);

            //! Access the file headers
            const format::header &header() const;
            //! Access the current row header
            const format::row_header &row_header() const;
            //! Get the index of the current row
            const std::size_t row_index() const;

        private:
            friend class reader;
            read_result read_bytes(unsigned char *into, std::size_t size);

            std::shared_ptr<format::header> m_header;
            std::shared_ptr<format::row_header> m_current;
            std::shared_ptr<detail::decompressor> m_decompressor;
            std::size_t m_row_index;
        };

        //! To save on open FDs, this lets you "split off" the reader for the current
        //! row into a separate object and reuse this reader
        std::shared_ptr<row_reader> detach();

    private:
        void start_row();

        std::shared_ptr<format::header> m_header;
        std::shared_ptr<format::toc_entry> m_toc;
        std::shared_ptr<format::row_header> m_current;
        std::shared_ptr<row_reader> m_row_reader;
        std::size_t m_limit;
        std::size_t m_row_index;
        performance::tuneables m_tune;
    };
} // namespace y5

#endif // Y5_H

#include "y5.h"

#include <algorithm>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>

#include <brotli/decode.h>
#include <brotli/encode.h>

namespace
{
    void *brotli_alloc(void * /* opaque */, std::size_t size)
    {
        return new unsigned char[size];
    }

    void brotli_free(void * /* opaque */, void *address)
    {
        delete[] reinterpret_cast<unsigned char *>(address);
    }

    std::size_t get_offset(y5::detail::fd_t fd)
    {
        return lseek(fd, 0, SEEK_CUR);
    }

    std::size_t skip(y5::detail::fd_t fd, std::size_t size)
    {
        return lseek(fd, size, SEEK_CUR);
    }

    std::size_t skip(y5::detail::fd_t fd, off_t off)
    {
        return lseek(fd, off, SEEK_CUR);
    }

    std::size_t set_offset(y5::detail::fd_t fd, std::size_t absolute_offset)
    {
        return lseek(fd, absolute_offset, SEEK_SET);
    }

    std::size_t get_size(y5::detail::fd_t fd)
    {
        auto current = get_offset(fd);
        auto size = lseek(fd, 0, SEEK_END);
        set_offset(fd, current);
        return size;
    }

    struct mmap_result
    {
        void *base;
        std::size_t shift;
        std::size_t length;
    };

    enum class mmap_prot
    {
        read = PROT_READ,
        write = PROT_READ | PROT_WRITE,
    };

    void sync_and_unmap(void *addr, std::size_t length)
    {
#ifdef Y5_FORCE_SYNC_BEFORE_UNMAP
        {
            // MS_SYNC is kind of evil, but we don't want to cache everything in RAM.
            // If this is too much of a performance hit, increase the threshold for shrinking.
            // The kernel should take care of syncing (via overcommit settings) itself though.
            auto err = msync(addr, length, MS_SYNC | MS_INVALIDATE);
            Y5_ASSERT(!err, "Failed to msync block");
        }
#endif
        auto err = munmap(addr, length);
        Y5_ASSERT(!err, "Failed to munmap block");
    }

    [[nodiscard]] mmap_result raw_mmap_block(y5::detail::fd_t fd, std::size_t size, std::size_t offset, mmap_prot prot)
    {
        auto aligned = offset & PAGE_MASK;
        auto shift = offset - aligned;
        auto length = size + shift;
        Y5_ASSERT(length > 0, "Trying to map block of size zero");
        void *result = mmap(nullptr, length, static_cast<int>(prot), MAP_SHARED, fd, aligned);
        Y5_ASSERT(result != MAP_FAILED, "Failed to mmap block");
        return { result, shift, length };
    }

    template <typename T = unsigned char>
    std::shared_ptr<T> mmap_block(y5::detail::fd_t fd, std::size_t size, std::size_t offset, mmap_prot prot)
    {
        auto [result, shift, length] = raw_mmap_block(fd, size, offset, prot);
        return std::shared_ptr<T>(
            reinterpret_cast<T *>(reinterpret_cast<unsigned char *>(result) + shift),
            [result, length](T *)
            {
                sync_and_unmap(result, length);
            }
        );
    }

    template <typename T> T read_object(y5::detail::fd_t fd)
    {
        T object;
        auto bytes_read = read(fd, reinterpret_cast<void *>(&object), sizeof(T));
        Y5_ASSERT(bytes_read == sizeof(T), "Failed to read object");
        return object;
    }

    template <typename T> void write_object(y5::detail::fd_t fd, const T &t)
    {
        auto bytes_written = write(fd, reinterpret_cast<const void *>(&t), sizeof(T));
        Y5_ASSERT(bytes_written == sizeof(T), "Incomplete object write");
    }

    void truncate_after(y5::detail::fd_t fd, std::size_t size)
    {
        auto current_offset = get_offset(fd);
        auto err = ftruncate(fd, current_offset + size);
        Y5_ASSERT(!err, "Failed to add empty space to file");
        skip(fd, size);
    }

    struct memory_mapped_block
    {
        using deleter = std::function<void(unsigned char *ptr)>;

        std::size_t offset;
        std::size_t size;
        std::unique_ptr<unsigned char[], deleter> buffer;
        void *base;

        static deleter make_deleter(void *base, std::size_t length)
        {
            return [base, length](unsigned char *actual)
            {
                Y5_ASSERT((reinterpret_cast<std::uintptr_t>(actual) & PAGE_MASK) == reinterpret_cast<std::uintptr_t>(base), "Deleting wrong pointer with memory_mapped_block::deleter");
                sync_and_unmap(base, length);
            };
        }

        static memory_mapped_block create(y5::detail::fd_t fd, std::size_t size, std::size_t offset, mmap_prot prot)
        {
            auto [base, shift, length] = raw_mmap_block(fd, size, offset, prot);
            auto del = make_deleter(base, length);
            auto buffer = std::unique_ptr<unsigned char[], deleter>(reinterpret_cast<unsigned char *>(base) + shift, std::move(del));

            return { offset, size, std::move(buffer), base };
        }

        static memory_mapped_block create_empty(y5::detail::fd_t fd, std::size_t size, mmap_prot prot)
        {
            auto offset = get_offset(fd);
            truncate_after(fd, size);
            return create(fd, size, offset, prot);
        }

        static memory_mapped_block map(y5::detail::fd_t fd, std::size_t size, mmap_prot prot)
        {
            auto offset = get_offset(fd);
            return create(fd, size, offset, prot);
        }

        void grow_back(std::size_t by_size)
        {
            auto raw_ptr = buffer.release(); // Now the old deleter does nothing anymore
            auto shift = raw_ptr - reinterpret_cast<unsigned char *>(base); // Shift in first mapped page
            auto old_length = size + shift;
            auto new_length = old_length + by_size; // Adjust length of mapping
            base = mremap(base, old_length, new_length, MREMAP_MAYMOVE);
            Y5_ASSERT(base != MAP_FAILED, "Failed to mremap block");

            auto del = make_deleter(base, new_length);
            size += by_size;
            buffer = std::unique_ptr<unsigned char[], deleter>(reinterpret_cast<unsigned char *>(base) + shift, std::move(del));
        }

        bool shrink_front(std::size_t by_size)
        {
            if (by_size == size)
                return false; // Do not shrink a block down to nothing!

            auto raw_ptr = buffer.release(); // Now the old deleter does nothing anymore
            auto shift = raw_ptr - reinterpret_cast<unsigned char *>(base); // Shift in first mapped page
            auto after_shift = shift + by_size;
            auto old_length = size + shift;

            auto remove_pages = after_shift & PAGE_MASK;
            auto new_shift = after_shift - remove_pages;
            size -= by_size;
            offset += by_size;
            if (remove_pages)
            {
                sync_and_unmap(base, remove_pages);
                base = reinterpret_cast<void *>(reinterpret_cast<unsigned char *>(base) + remove_pages);
                auto del = make_deleter(base, old_length - remove_pages);
                buffer = std::unique_ptr<unsigned char[], deleter>(reinterpret_cast<unsigned char *>(base) + new_shift, std::move(del));
            }
            else
            {
                // Still in the same page, only reset block pointer and size
                buffer.reset(raw_ptr + by_size);
            }
            return true; // We did shrink something
        }
    };
} // anonymous namespace

namespace y5
{
    namespace detail
    {

        // A conditionally-copyable wrapper around a file descriptor
        template <bool Copyable>
        fd_wrapper<Copyable>::fd_wrapper(fd_t fd) : m_fd(fd) { Y5_ASSERT(fd != INVALID_FD, "Failed to open file"); }
        template <bool Copyable>
        fd_wrapper<Copyable>::~fd_wrapper() { if (m_fd != INVALID_FD) close(m_fd); m_fd = INVALID_FD; }
        template <bool Copyable>
        fd_wrapper<Copyable>::fd_wrapper(fd_wrapper &&other) : m_fd(other.m_fd) { other.m_fd = INVALID_FD; }
        template <bool Copyable>
        fd_wrapper<Copyable> &fd_wrapper<Copyable>::operator=(fd_wrapper &&other)
        {
            m_fd = other.m_fd;
            other.m_fd = INVALID_FD;
            return *this;
        }
        template <bool Copyable>
        fd_wrapper<Copyable>::fd_wrapper(const fd_wrapper &other)
        {
            Y5_ASSERT(other.m_fd != INVALID_FD, "Copying invalid file descriptor");
            m_fd = dup(other.m_fd);
            Y5_ASSERT(m_fd != INVALID_FD, "Failed to copy file descriptor");
        }
        template <bool Copyable>
        fd_wrapper<Copyable> &fd_wrapper<Copyable>::operator=(const fd_wrapper &other)
        {
            Y5_ASSERT(other.m_fd != INVALID_FD, "Copying invalid file descriptor");
            m_fd = dup(other.m_fd);
            Y5_ASSERT(m_fd != INVALID_FD, "Failed to copy file descriptor");
            return *this;
        }

        template class fd_wrapper<false>;
        template class fd_wrapper<true>;

        // Allows a compressor or decompressor to request more buffer space to be added to the file
        // NB: It is the user's responsibility to ensure no other file writes occur between starting
        // compression and finalizing it.
        using file_growth_request = std::function<void(std::size_t)>;
        using file_shrink_request = std::function<void(std::size_t)>;

        // Compressor
        class compressor
        {
        public:
            compressor(memory_mapped_block block, file_growth_request grow, file_shrink_request shrink, performance::tuneables tune, bool bypass)
                : m_block(std::move(block))
                , m_grow(std::move(grow))
                , m_shrink(std::move(shrink))
                , m_tune(std::move(tune))
                , m_current(m_block.buffer.get())
                , m_remaining(m_block.size)
                , m_bypass(bypass)
            {
                if (m_bypass) [[unlikely]]
                    return;

                m_state = std::shared_ptr<BrotliEncoderState>(
                    ::BrotliEncoderCreateInstance(brotli_alloc, brotli_free, nullptr),
                    ::BrotliEncoderDestroyInstance
                );

                auto set_parameter = [state = m_state.get()](BrotliEncoderParameter param, std::uint32_t value) mutable
                {
                    auto ok = BrotliEncoderSetParameter(state, param, value);
                    Y5_ASSERT(ok == BROTLI_TRUE, "Failed to set parameter");
                };

                switch (tune.compression_level)
                {
                    // NB: Brotli by default uses maximum compression, we don't want that (slow!)
                    static_assert(BROTLI_MIN_QUALITY == 0, "Please update minimum compression quality");
                    static_assert(BROTLI_MAX_QUALITY == 11, "Please update maximum compression quality");
                    case performance::tuneables::compression_fastest:
                        set_parameter(BROTLI_PARAM_QUALITY, BROTLI_MIN_QUALITY);
                        break;
                    case performance::tuneables::compression_faster:
                        set_parameter(BROTLI_PARAM_QUALITY, 3);
                        break;
                    case performance::tuneables::compression_default:
                        set_parameter(BROTLI_PARAM_QUALITY, 6);
                        break;
                    case performance::tuneables::compression_better:
                        set_parameter(BROTLI_PARAM_QUALITY, 9);
                        break;
                    case performance::tuneables::compression_best:
                        set_parameter(BROTLI_PARAM_QUALITY, BROTLI_MAX_QUALITY);
                        break;
                    default:
                        Y5_ASSERT(m_bypass, "Invalid compression level in non-bypass mode");
                        break;
                }
            }

            void append(const unsigned char *data, std::size_t size)
            {
                if (m_bypass) [[unlikely]]
                {
                    while (size)
                    {
                        std::size_t copy = std::min(size, m_remaining);
                        memcpy(m_current, data, copy);
                        m_current += copy;
                        m_remaining -= copy;
                        m_compressed_size += copy;
                        size -= copy;
                        if (size)
                            grow_and_adjust(false);
                    }
                }
                else
                {
                    Y5_ASSERT(!!m_state, "Compressor was already finalized");
                    while (size)
                    {
                        auto result = BrotliEncoderCompressStream(
                            m_state.get(),
                            BROTLI_OPERATION_PROCESS,
                            &size,
                            &data,
                            &m_remaining,
                            &m_current,
                            &m_compressed_size
                        );
                        Y5_ASSERT(result == BROTLI_TRUE, "Compression error");
                        if (size)
                        {
                            // We have remaining data. This should only happen if our output
                            // buffer is full, so we need to map more memory. We can also unmap
                            // the full parts of the buffer.
                            grow_and_adjust(false);
                        }
                    }
                }
            }

            void finalize()
            {
                if (!m_bypass) [[likely]]
                {
                    Y5_ASSERT(!!m_state, "Compressor was already finalized");
                    while (true)
                    {
                        std::size_t available_in = 0;
                        auto result = BrotliEncoderCompressStream(
                            m_state.get(),
                            BROTLI_OPERATION_FINISH,
                            &available_in,
                            nullptr,
                            &m_remaining,
                            &m_current,
                            &m_compressed_size
                        );
                        Y5_ASSERT(result == BROTLI_TRUE, "Compression error");
                        if (BrotliEncoderIsFinished(m_state.get()) && !BrotliEncoderHasMoreOutput(m_state.get()))
                            break;
                        // More output, but probably no more space, resize
                        grow_and_adjust(true);
                    }
                }
                // Return unused buffer
                if (m_remaining)
                    m_shrink(m_remaining);
                m_state = nullptr;
            }

            std::size_t compressed_size() const
            {
                return m_compressed_size;
            }

        private:
            void grow_and_adjust(bool finalizing = false)
            {
                // We need more memory. After a certain point, also unmap old pages
                auto delta = m_current - m_block.buffer.get();
                Y5_ASSERT(delta >= 0, "Negative delta should be impossible");
                Y5_ASSERT(static_cast<std::size_t>(delta) == m_block.size - m_remaining, "Pointers don't match reported remaining space");
                auto growth = std::clamp((m_block.size / 2) & PAGE_MASK, PAGE_SIZE, m_tune.maximum_overallocation);

                m_grow(growth);
                m_block.grow_back(growth);
                m_current = m_block.buffer.get() + delta;
                m_remaining = m_block.size - delta;

                if (m_block.size >= m_tune.maximum_overallocation)
                {
                    if (m_block.shrink_front(delta & PAGE_MASK)) // Try to shrink front pages
                    {
                        delta &= ~PAGE_MASK;
                        m_current = m_block.buffer.get() + delta;
                        m_remaining = m_block.size - delta;
                    }
                }
            }

            memory_mapped_block m_block;
            file_growth_request m_grow;
            file_shrink_request m_shrink;
            performance::tuneables m_tune;
            unsigned char *m_current = nullptr; // points into m_block.block!
            std::size_t m_remaining = 0;
            std::size_t m_compressed_size = 0;
            std::shared_ptr<BrotliEncoderState> m_state = nullptr;
            bool m_bypass = false;
        };

        // Decompressor
        class decompressor
        {
        public:
            decompressor(memory_mapped_block block, performance::tuneables tune, std::size_t input_size, bool bypass)
                : m_block(std::move(block))
                , m_tune(std::move(tune))
                , m_current(m_block.buffer.get())
                , m_remaining(m_block.size)
                , m_available(input_size)
                , m_bypass(bypass)
            {
                if (m_bypass) [[unlikely]]
                    return;

                m_state = std::shared_ptr<BrotliDecoderState>(
                    ::BrotliDecoderCreateInstance(brotli_alloc, brotli_free, nullptr),
                    ::BrotliDecoderDestroyInstance
                );
            }

            y5::reader::read_result read(unsigned char *into, std::size_t size)
            {
                std::size_t previous_processed = m_processed;
                if (m_bypass) [[unlikely]]
                {
                    while (size && m_processed < m_available)
                    {
                        std::size_t copy = std::min(size, m_remaining);
                        memcpy(into, m_current, copy);
                        m_current += copy;
                        m_remaining -= copy;
                        size -= copy;
                        m_processed += copy;
                        if (size && m_processed < m_available)
                            grow_and_adjust();
                    }
                    return { m_processed - previous_processed, m_processed < m_available };
                }
                else
                {
                    Y5_ASSERT(!!m_state && m_processed < m_available, "Decompressor was already done");
                    while (size && m_processed < m_available)
                    {
                        auto result = BrotliDecoderDecompressStream(
                            m_state.get(),
                            &m_remaining,
                            &m_current,
                            &size,
                            &into,
                            &m_processed
                        );
                        Y5_ASSERT(result != BROTLI_DECODER_RESULT_ERROR, std::string { "Decompression error: " } + BrotliDecoderErrorString(BrotliDecoderGetErrorCode(m_state.get())));
                        switch (result)
                        {
                            case BROTLI_DECODER_RESULT_NEEDS_MORE_INPUT:
                                // Oh no, map more memory
                                Y5_ASSERT(m_processed < m_available, "Corrupted file, compressed row not terminated");
                                if (size == 0)
                                    return { m_processed - previous_processed, false };
                                grow_and_adjust();
                                break;
                            case BROTLI_DECODER_RESULT_NEEDS_MORE_OUTPUT:
                                return { m_processed - previous_processed, false };
                            case BROTLI_DECODER_RESULT_SUCCESS:
                                // Row is done
                                return { m_processed - previous_processed, true };
                            case BROTLI_DECODER_RESULT_ERROR:
                                return { m_processed - previous_processed, m_processed < m_available };
                        }
                    }
                }
                Y5_ASSERT(false, "Reached unreachable code");
                return { 0, false };
            }

        private:
            void grow_and_adjust()
            {
                // We need more memory. After a certain point, also unmap old pages
                auto delta = m_current - m_block.buffer.get();
                Y5_ASSERT(delta >= 0, "Negative delta should be impossible");
                Y5_ASSERT(static_cast<std::size_t>(delta) == m_block.size - m_remaining, "Pointers don't match reported remaining space");

                auto processed_in_buffer = m_current - m_block.buffer.get();
                auto buffer_start_in_row = m_processed - processed_in_buffer;
                auto remaining_after_buffer = m_available - buffer_start_in_row - m_block.size;
                auto growth = std::min(m_tune.maximum_overallocation, remaining_after_buffer);

                m_block.grow_back(growth);
                m_current = m_block.buffer.get() + delta;
                m_remaining = m_block.size - delta;

                if (m_block.size >= m_tune.maximum_overallocation)
                {
                    if (m_block.shrink_front(delta & PAGE_MASK)) // Try to shrink front pages
                    {
                        delta &= ~PAGE_MASK;
                        m_current = m_block.buffer.get() + delta;
                        m_remaining = m_block.size - delta;
                    }
                }
            }

            memory_mapped_block m_block;
            performance::tuneables m_tune;
            const unsigned char *m_current = nullptr; // points into m_block.block!
            std::size_t m_remaining = 0; // bytes remaining in the buffer
            std::size_t m_processed = 0; // bytes already processed
            std::size_t m_available = 0; // bytes available in the row as a whole
            std::shared_ptr<BrotliDecoderState> m_state = nullptr;
            bool m_bypass = false;
        };
    } // namespace detail


    // Writer

    writer::writer(const std::filesystem::path &path, writer_mode mode, std::size_t entry_size, format::header::bitflags flags, std::uint32_t toc_capacity, performance::tuneables tune)
        : detail::fd_wrapper<false>(open(path.c_str(), O_CREAT | O_RDWR | (mode == writer_mode::OVERWRITE ? O_TRUNC : O_EXCL), 0644))
    {
        this->tune(std::move(tune));
        Y5_ASSERT(!(flags & format::header::bitflags::unused), "Invalid flag set for writer");
        Y5_ASSERT(entry_size < std::numeric_limits<std::uint16_t>::max(), "Entry size is too large");
        Y5_ASSERT(entry_size > 0, "Entry size is zero");
        // Write header
        write_object(m_fd, format::header {
            .magic = format::MAGIC,
            .version = format::VERSION,
            .flags = flags,
            .entry_size = static_cast<std::uint16_t>(entry_size),
            .rows = 0,
            .max_columns = 0,
            .toc_resolution = toc_capacity ? static_cast<std::uint32_t>(1) : static_cast<std::uint32_t>(0),
            .toc_capacity = toc_capacity
        });
        // Map header
        m_header = mmap_block<format::header>(m_fd, sizeof(format::header), 0, mmap_prot::write);
        // Allocate and map TOC if capacity is not set to zero
        m_toc = nullptr;
        if (toc_capacity)
        {
            auto toc_size = sizeof(format::toc_entry) * toc_capacity;
            truncate_after(m_fd, toc_size);
            m_toc = mmap_block<format::toc_entry>(m_fd, toc_size, sizeof(format::header), mmap_prot::write);
        }
        m_current = nullptr;
    }

    void writer::tune(performance::tuneables tune)
    {
        Y5_ASSERT(tune.maximum_overallocation >= PAGE_SIZE, "Maximum overallocation must be at least the page size");
        Y5_ASSERT(tune.maximum_held_buffer >= PAGE_SIZE, "Maximum held buffer must be at least the page size");
        Y5_ASSERT(tune.initial_buffer_size >= PAGE_SIZE, "Initial buffer size must be at least the page size");
        m_tune = tune;
    }

    void writer::end_row()
    {
        Y5_ASSERT(!!m_header, "No file header");
        if (!m_current) // Empty row, add it first
            start_row();

        m_header->rows++;
        m_compressor->finalize();
        if (m_header->max_columns != m_current->columns && m_header->rows > 1)
            m_header->flags = static_cast<format::header::bitflags>(m_header->flags | format::header::bitflags::ragged);
        if (m_header->max_columns < m_current->columns)
            m_header->max_columns = m_current->columns;
        m_current->compressed_size = m_compressor->compressed_size();
        m_compressor.reset(); // Clear out the memory-mapped buffer
        m_current.reset(); // Row is done
    }

    void writer::append_buffer(const unsigned char *data, std::size_t number_of_entries)
    {
        Y5_ASSERT(!!m_header, "No header present");
        std::size_t data_size;
        if (__builtin_umull_overflow(number_of_entries, m_header->entry_size, &data_size))
            Y5_ASSERT(false, "Size computation overflowed");
        append_bytes(data, data_size);
    }

    void writer::append_bytes(const unsigned char *data, std::size_t size)
    {
        if (!m_current)
            start_row();
        Y5_ASSERT(!!m_compressor, "No compressor");
        m_compressor->append(data, size);
        m_current->columns += size / m_header->entry_size;
    }

    void writer::start_row()
    {
        Y5_ASSERT(!!m_header, "No file header");
        Y5_ASSERT(!m_current, "Row is already started, end it first");
        auto offset = get_offset(m_fd);
        write_object(m_fd, format::row_header {
            .compressed_size = 0,
            .columns = 0
        });
        if (m_toc /* Otherwise, TOC is disabled */ && m_header->rows % m_header->toc_resolution == 0)
        {
            // Add TOC entry
            if (m_header->rows >= m_header->toc_capacity * m_header->toc_resolution)
            {
                // TOC is full, update the resolution
                m_header->toc_resolution = m_header->toc_resolution * 2;
                for (std::uint32_t index = 1; index < m_header->toc_capacity / 2; ++index)
                    m_toc.get()[index] = format::toc_entry { m_toc.get()[index * 2] };
                for (std::uint32_t index = m_header->toc_capacity / 2; index < m_header->toc_capacity; ++index)
                    m_toc.get()[index] = format::toc_entry { 0  };
                // Now only add the entry if this is necessary for the _new_ resolution!
                if (m_header->rows % m_header->toc_resolution == 0)
                    m_toc.get()[m_header->rows / m_header->toc_resolution] = format::toc_entry { offset };
            }
            else
            {
                m_toc.get()[m_header->rows / m_header->toc_resolution] = format::toc_entry { offset };
            }
        }
        m_current = mmap_block<format::row_header>(m_fd, sizeof(format::row_header), offset, mmap_prot::write);
        auto block = memory_mapped_block::create_empty(m_fd, m_tune.initial_buffer_size, mmap_prot::write);
        m_compressor = std::make_shared<detail::compressor>(
            std::move(block),
            [this](std::size_t grow_file_by) mutable { truncate_after(m_fd, grow_file_by); },
            [this](std::size_t shrink_file_by) mutable
            {
                skip(m_fd, -static_cast<off_t>(shrink_file_by));
                truncate_after(m_fd, 0);
            },
            m_tune,
            m_header->flags & format::header::bitflags::uncompressed
        );
    }


    // Reader

    reader::reader(const std::filesystem::path &path, performance::tuneables tune)
        : detail::fd_wrapper<true>(open(path.c_str(), O_RDONLY))
    {
        this->tune(std::move(tune));

        // Map header
        m_header = mmap_block<format::header>(m_fd, sizeof(format::header), 0, mmap_prot::read);
        skip(m_fd, sizeof(format::header));

        // Validate header
        Y5_ASSERT(m_header->magic == format::MAGIC, "Invalid magic byte sequence");
        Y5_ASSERT(m_header->version == format::VERSION, "Incompatible version");
        Y5_ASSERT(!(m_header->flags & format::header::bitflags::unused), "Invalid flag set in file");
        Y5_ASSERT(m_header->entry_size > 0, "Entry size is zero");
        Y5_ASSERT(m_header->toc_resolution > 0, "Invalid TOC resolution");

        // Read TOCs
        m_toc = nullptr;
        auto toc_size = m_header->toc_capacity * sizeof(format::toc_entry);
        if (m_header->toc_resolution && toc_size)
            m_toc = mmap_block<format::toc_entry>(m_fd, toc_size, sizeof(format::header), mmap_prot::read);
        else
            Y5_ASSERT(m_header->toc_capacity == 0, "TOC capacity > 0 without TOC resolution");
        skip(m_fd, toc_size);

        m_limit = get_size(m_fd);
        m_current = nullptr;
        m_row_index = -1;
    }

    void reader::tune(performance::tuneables tune)
    {
        Y5_ASSERT(tune.maximum_overallocation >= PAGE_SIZE, "Maximum overallocation must be at least the page size");
        Y5_ASSERT(tune.maximum_held_buffer >= PAGE_SIZE, "Maximum held buffer must be at least the page size");
        Y5_ASSERT(tune.initial_buffer_size >= PAGE_SIZE, "Initial buffer size must be at least the page size");
        m_tune = tune;
    }

    const format::header &reader::header() const { return *m_header; }
    const format::toc_entry &reader::toc_entry(std::size_t index) const
    {
        Y5_ASSERT(m_toc, "No TOC in this file");
        Y5_ASSERT(index < m_header->toc_capacity, "Index exceeds number of TOC entries");
        return m_toc.get()[index];
    };
    const format::row_header &reader::row_header() const { return *m_current; }
    const std::size_t reader::row_index() const { return m_row_index; }

    const format::header &reader::row_reader::header() const { return *m_header; }
    const format::row_header &reader::row_reader::row_header() const { return *m_current; }
    const std::size_t reader::row_reader::row_index() const { Y5_ASSERT(!!m_current, "No current row!"); return m_row_index; }

    void reader::seek(std::size_t row)
    {
        // Find matching TOC entry (if any)
        Y5_ASSERT(row < m_header->rows, "Row index out of bounds");
        std::size_t base_offset = sizeof(format::header);
        std::size_t base_index = 0;
        if (m_toc)
        {
            std::size_t toc_idx = std::clamp(row / m_header->toc_resolution, static_cast<std::size_t>(0), static_cast<std::size_t>(m_header->toc_capacity - 1));
            base_index = toc_idx * m_header->toc_resolution;
            base_offset = toc_entry(toc_idx).offset;
        }
        set_offset(m_fd, base_offset);
        while (base_index < row)
        {
            auto row_header = read_object<format::row_header>(m_fd);
            skip(m_fd, row_header.compressed_size);
            ++base_index;
        }
        m_row_index = row - 1;
        start_row();
    }

    void reader::start_row()
    {
        // Map header and start of data
        ++m_row_index;
        auto offset = get_offset(m_fd);
        Y5_ASSERT(offset < m_limit, "Reading after end-of-file!");
        skip(m_fd, sizeof(format::row_header));

        m_current = mmap_block<format::row_header>(m_fd, sizeof(format::row_header), offset, mmap_prot::read);
        Y5_ASSERT(m_current->compressed_size, "Row is corrupted (compressed size is zero)");
        auto block = memory_mapped_block::map(m_fd, std::min(m_tune.maximum_overallocation, m_current->compressed_size), mmap_prot::read);

        // Set up decompressor
        auto decompressor = std::make_shared<detail::decompressor>(
            std::move(block),
            m_tune,
            m_current->columns * m_header->entry_size,
            m_header->flags & format::header::bitflags::uncompressed
        );

        // Skip the row in the FD already
        skip(m_fd, m_current->compressed_size);

        // Create the row reader
        m_row_reader = std::make_shared<reader::row_reader>(
            m_header,
            m_current,
            std::move(decompressor),
            m_row_index
        );
    }

    reader::read_result reader::read_buffer(unsigned char *into, std::size_t number_of_entries)
    {
        if (!m_row_reader || !m_current)
            start_row(); // At the start, we don't have a decompressor set up, so the first row does that if necessary

        Y5_ASSERT(!!m_row_reader, "No row to read from");

        auto result = m_row_reader->read_buffer(into, number_of_entries);
        if (result.row_done)
            m_row_reader = nullptr; // Don't clean up the row header yet (user might want it), but kill the reader.

        return result;
    }

    std::shared_ptr<reader::row_reader> reader::detach()
    {
        if (!m_row_reader || !m_current)
            start_row();

        std::shared_ptr<reader::row_reader> empty = nullptr;
        std::swap(empty, m_row_reader);
        return empty;
    }

    reader::row_reader::row_reader(std::shared_ptr<format::header> header, std::shared_ptr<format::row_header> row_header, std::shared_ptr<detail::decompressor> decompressor, std::size_t row_index)
        : m_header(std::move(header))
        , m_current(std::move(row_header))
        , m_decompressor(std::move(decompressor))
        , m_row_index(row_index)
    {}

    reader::read_result reader::row_reader::read_bytes(unsigned char *into, std::size_t size)
    {
        Y5_ASSERT(!!m_decompressor, "No more data to read in this row");

        auto result = m_decompressor->read(into, size);
        if (result.row_done)
            m_decompressor = nullptr; // Don't clean up the row header yet (user might want it), but kill the decompressor.

        return result;
    }

    reader::read_result reader::row_reader::read_buffer(unsigned char *into, std::size_t number_of_entries)
    {
        Y5_ASSERT(!!m_header, "No header present");
        Y5_ASSERT(!!m_decompressor, "No more data to read in this row");
        std::size_t data_size;
        if (__builtin_umull_overflow(number_of_entries, m_header->entry_size, &data_size))
            Y5_ASSERT(false, "Size computation overflowed");
        auto [bytes, end_of_row] = read_bytes(into, data_size);
        Y5_ASSERT(bytes % m_header->entry_size == 0, "Incomplete read");
        return { bytes / m_header->entry_size, end_of_row };
    }
} // namespace y5

// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"

#include <y5.h>

#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <functional>
#include <optional>

#include <sys/random.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <boost/process.hpp>

// Guard the whiteboxgrind types so we don't pollute anything
namespace protocol
{
#include "../valgrind/whiteboxgrind/whiteboxgrind_types.h" // TODO: Set up proper path for this
} // namespace protocol

#define BUFFER_SIZE 65536

[[noreturn]] static void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

std::string now()
{
    std::ostringstream out;
    auto current_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    out << std::put_time(std::localtime(&current_time), "%FT%T%z") << "-";
    return out.str();
}

template <typename T>
class guard
{
public:
    template <typename U, typename Deleter> guard(U &&value, Deleter &&del) : m_value(std::forward<U>(value)), m_deleter(std::move(del)) {}
    ~guard() { m_deleter(std::move(m_value)); }

    operator T() { return m_value; }
    T &get() { return m_value; }

private:
    T m_value;
    std::function<void(T &&)> m_deleter;
};

struct writers
{
    writers(const std::string &prefix, const std::filesystem::path &directory, bool overwrite, std::size_t block_size)
        : pc(directory / (prefix + "pc.y5"), overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, sizeof(protocol::PCTraceEntry))
        , read(directory / (prefix + "read.y5"), overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, sizeof(protocol::MemoryTraceEntry))
        , write(directory / (prefix + "write.y5"), overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, sizeof(protocol::MemoryTraceEntry))
        , cas(directory / (prefix + "cas.y5"), overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, sizeof(protocol::MemoryTraceEntry))
        , values(directory / (prefix + "values.y5"), overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE, block_size * 2)
    {}

    void end_row()
    {
        pc.end_row();
        read.end_row();
        write.end_row();
        cas.end_row();
        values.end_row();
    }

    y5::writer pc;
    y5::writer read;
    y5::writer write;
    y5::writer cas;
    y5::writer values;
};

class value_generator
{
public:
    value_generator(unsigned long count, unsigned long block_size)
        : m_reader(nullptr)
        , m_file()
        , m_count(count)
        , m_block_size(block_size)
    {}

    value_generator(const std::filesystem::path &path, unsigned long block_size)
        : m_reader(nullptr)
        , m_file(path)
        , m_count(0)
        , m_block_size(block_size)
    {
        std::remove_cv_t<decltype(y5::format::MAGIC)> magic;
        m_file.read(reinterpret_cast<char *>(&magic), sizeof(magic));
        bool is_y5 = magic == y5::format::MAGIC;
        if (is_y5)
        {
            // Probably a y5 file
            try
            {
                m_reader = std::make_shared<y5::reader>(path);
                m_count = m_reader->header().rows;
                m_file.close();
            }
            catch (const y5::detail::failed_assertion &assertion)
            {
                // Not actually a y5 file
                is_y5 = false;
            }
        }
        if (!is_y5)
        {
            // Maybe not...
            m_reader = nullptr;
            m_count = std::filesystem::file_size(path) / block_size;
            m_file.seekg(0, std::ifstream::seekdir::_S_beg);
        }
    }

    bool has_next()
    {
        return m_count > 0;
    }

    std::vector<std::uint8_t> next()
    {
        if (!m_count--) die("No more inputs!");
        if (m_reader)
        {
            std::vector<std::uint8_t> space(2 * m_block_size);
            m_reader->read(space.data(), 1);
            space.resize(m_block_size); // Truncate off the result
            return space;
        }
        else if (m_file)
        {
            std::vector<std::uint8_t> space(m_block_size);
            m_file.read(reinterpret_cast<char *>(space.data()), m_block_size);
            return space;
        }
        else
        {
            std::vector<std::uint8_t> space(m_block_size);
            if (getrandom(space.data(), m_block_size, 0) != m_block_size) die("Failed to generate random inputs");
            return space;
        }
#if defined(__GNUC__)
        __builtin_unreachable();
#endif
    }

private:
    std::shared_ptr<y5::reader> m_reader;
    std::ifstream m_file;
    unsigned long m_count;
    unsigned long m_block_size;
};

int main(int argc, const char *argv[])
{
    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "source", .help = "Whitebox source code"});
    parser.add(ArgumentParser::Argument { .name = "output", .help = "Output directory" });
    parser.add(ArgumentParser::Argument { .name = "valgrind", .help = "Valgrind/Whiteboxgrind executable" });

    parser.add(ArgumentParser::Option { .short_name = "-n", .long_name = "--name", .help = "Prefix for output file names", .default_value = now() });
    parser.add(ArgumentParser::Option { .short_name = "-s", .long_name = "--socket", .help = "Socket location" });
    parser.add(ArgumentParser::Option { .short_name = "-i", .long_name = "--inputs", .help = "File containing values or raw input data", .conflicts = {"--count"} });
    parser.add(ArgumentParser::Option { .short_name = "-c", .long_name = "--count", .help = "Number of random inputs to generate", .conflicts = {"--inputs"} });
    parser.add(ArgumentParser::Option { .long_name = "--block-size", .help = "Cipher block size in bytes", .default_value = "16" });
    parser.add(ArgumentParser::Option { .long_name = "--cflags", .help = "Extra compilation flags", .default_value = "" });
    parser.add(ArgumentParser::Option { .long_name = "--args", .help = "Extra whiteboxgrind arguments", .default_value = "" });
    parser.add(ArgumentParser::Option { .long_name = "--resources", .help = "The tracer's resource directory", .default_value = std::filesystem::current_path() });

    parser.add(ArgumentParser::Toggle { .short_name = "-f", .long_name = "--overwrite", .help = "Overwrite the output files if they already exist" });
    parser.add(ArgumentParser::Toggle { .long_name = "--no-include", .help = "Do not automatically derive an include path" });
    auto args = parser.parse(argc, argv);

    // Check that files are in the right places
    std::filesystem::path source = args["source"];
    if (!std::filesystem::exists(source)) die("Source file '"s + args["source"] + "' does not exist");
    std::filesystem::path valgrind = args["valgrind"];
    if (!std::filesystem::exists(valgrind)) die("Valgrind executable '"s + args["valgrind"] + "' does not exist");
    std::filesystem::path output = args["output"];
    if (!std::filesystem::is_directory(output)) die("Output directory '"s + args["output"] + "' is not a directory or does not exist.");

    auto block_size = std::stoul(args["--block-size"]);
    if (block_size <= 0) die("Invalid block size: "s + args["--block-size"]);

    if (args["--inputs"].empty() && args["--count"].empty()) die("No inputs specified - one of [--inputs, --count] is required");
    if (!args["--inputs"].empty())
        if (!std::filesystem::exists(args["--inputs"])) die ("Input file '"s + args["--inputs"] + "' does not exist");
    if (!args["--count"].empty())
        if (std::stoul(args["--count"]) <= 0) die("Invalid number of inputs: "s + args["--count"]);
    value_generator gen = !args["--inputs"].empty() ? value_generator { args["--inputs"], block_size } : value_generator { std::stoul(args["--count"]), block_size };

    std::filesystem::path resource_path = args["--resources"];
    auto makefile = resource_path / "Makefile";
    auto mainfile = resource_path / "main.c";
    if (!std::filesystem::exists(makefile)) die("Resource path '"s + args["--resources"] + "' does not contain 'Makefile', use --resources to override");
    if (!std::filesystem::exists(mainfile)) die("Resource path '"s + args["--resources"] + "' does not contain 'main.c', use --resources to override");

    // Create a temporary working directory
    std::string directory_template = (std::filesystem::temp_directory_path() / "whiteboxgrind-XXXXXX").native();
    guard<std::filesystem::path> temporary_directory {
        mkdtemp(directory_template.data()),
        [](std::filesystem::path &&path) { if (path.native().find("whiteboxgrind") && std::filesystem::exists(path)) std::filesystem::remove_all(path); }
    };
    if (!std::filesystem::is_directory(temporary_directory)) die("Failed to create temporary directory");

    // Copy files
    auto binary_path = temporary_directory / "target";
    std::filesystem::copy(makefile, temporary_directory);
    std::filesystem::copy(mainfile, temporary_directory);
    std::filesystem::copy(source, temporary_directory);

    // Build the binary
    std::optional<std::filesystem::path> include_path = valgrind.parent_path().parent_path() / "include" / "valgrind";
    if (!args["--no-include"].empty() || !std::filesystem::is_directory(*include_path))
        include_path = std::nullopt;

    {
        // TODO: Perhaps this is "cleaner" to do with posix_spawn (no dependency), but then we need to catch SIGCHLD etc.
        auto env = boost::this_process::environment();
        env["MORE_CFLAGS"] = args["--cflags"];
        env["MORE_INCDIR"] = include_path ? *include_path : "";
        auto rc = boost::process::system(boost::process::search_path("make"), "-C", temporary_directory.get().native(), env);
        if (rc != 0) die("Failed to build executable");
    }

    // Open the socket for communication
    guard<int> sock { socket(AF_UNIX, SOCK_STREAM, 0), [](auto fd) { if (fd != -1) close(fd); } };
    if (sock == -1) die("Failed to open socket");

    writers writers(args["--name"], args["output"], !args["--overwrite"].empty(), std::stoul(args["--block-size"]));

    std::filesystem::path socket_path;
    if (args["--socket"].empty())
        socket_path = temporary_directory / "socket";
    else
        socket_path = args["--socket"];

    sockaddr_un address = { .sun_family = AF_UNIX };
    strncpy(address.sun_path, socket_path.native().c_str(), sizeof(sockaddr_un::sun_path));
    address.sun_path[sizeof(sockaddr_un::sun_path) - 1] = 0;
    if (bind(sock, reinterpret_cast<sockaddr *>(&address), sizeof(sockaddr_un)) != 0) die("Failed to bind socket");
    if (listen(sock, 5) != 0) die("Failed to listen on socket");

    // Run the tracer for every input
    std::vector<std::uint8_t> buffer(BUFFER_SIZE);
    std::size_t available = 0;
    std::size_t count = 0;
    while (gen.has_next())
    {
        auto input = gen.next();
        std::cout << ++count << ": ";
        for (const std::uint8_t byte : input)
            std::cout << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned>(byte) << std::dec;
        std::cout << std::endl;

        boost::process::ipstream outputs;
        boost::process::opstream inputs;
        boost::process::child tracer { valgrind.native(), "--tool=whiteboxgrind", "--sock="s + socket_path.native(), "--client-requests=yes", binary_path.native(), boost::process::std_in < inputs, boost::process::std_out > outputs };

        guard<int> rso { accept(sock, NULL, NULL), [](auto fd) { if (fd != -1) close(fd); } };
        if (rso == -1)
            die("Failed to accept connection");

        inputs.write(reinterpret_cast<char *>(input.data()), input.size());
        inputs.close();

        while (true)
        {
            fd_set read_fds;
            FD_ZERO(&read_fds);
            FD_SET(rso, &read_fds);
            timeval timeout { .tv_sec = 0, .tv_usec = 100000 };
            auto result = select(rso + 1, &read_fds, NULL, NULL, &timeout);
            if (!result && tracer.running())
                continue;
            else if (!result)
                break;
            // Only FD that can be ready is the socket
            auto bytes = read(rso, &buffer[available], buffer.size() - available);
            if (bytes == -1 && tracer.running())
                die("Failed to read despite running tracer");
            if (bytes <= 0)
                break;
            // Process the input messages
            // TODO: Dispatch this to a separate thread if necessary
            available += bytes;
            std::size_t offset = 0;
            while (available >= offset + sizeof(protocol::MessagePrefix))
            {
                const protocol::MessagePrefix *prefix = reinterpret_cast<protocol::MessagePrefix *>(&buffer[offset]);
                if (prefix->version != PROTOCOL_VERSION) die("Version mismatch");
                if (offset == 0 && buffer.size() - sizeof(protocol::MessagePrefix) < prefix->length)
                {
                    // Couldn't fit everything, resize the buffer and try again.
                    buffer.resize(buffer.size() * 2);
                    break;
                }
                else if (available - offset - sizeof(protocol::MessagePrefix) < prefix->length)
                {
                    // Not enough data yet (but buffer might be large enough), receive again.
                    break;
                }
                switch (prefix->type)
                {
                    case protocol::DataTypePCTrace:
                        writers.pc.append_buffer(&buffer[offset + sizeof(protocol::MessagePrefix)], prefix->length / sizeof(protocol::PCTraceEntry));
                        break;
                    case protocol::DataTypeMemoryTrace:
                        for (std::size_t i = 0; i < prefix->length; i += sizeof(protocol::MemoryTraceEntry))
                        {
                            auto *entry = reinterpret_cast<protocol::MemoryTraceEntry *>(&buffer[offset + sizeof(protocol::MessagePrefix) + i]);
                            switch (entry->mode)
                            {
                                case protocol::AccessRead:
                                    writers.read.append(entry, 1);
                                    break;
                                case protocol::AccessWrite:
                                    writers.write.append(entry, 1);
                                    break;
                                case protocol::AccessCAS:
                                    writers.cas.append(entry, 1);
                                    break;
                                case protocol::LogOnly:
                                    break;
                                default:
                                    die("Unknown access mode: "s + std::to_string(static_cast<unsigned>(entry->mode)));
                            }
                        }
                        break;
                    default:
                        die("Unknown data type: "s + std::to_string(static_cast<unsigned>(prefix->type)));
                }
                offset += sizeof(protocol::MessagePrefix) + prefix->length;
            }
            if (offset)
            {
                if (available > offset)
                    memmove(&buffer[0], &buffer[offset], available - offset);
                available -= offset;
            }
        }
        input.resize(block_size * 2);
        outputs.read(reinterpret_cast<char *>(&input[block_size]), block_size);
        writers.values.append_buffer(input.data(), 1);
        writers.end_row();
    }

    return EXIT_SUCCESS;
}

/*
 * Copyright 2022 Tobias Holl, Katharina Bogad
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PARALLEL_H
#define PARALLEL_H

#include <boost/asynchronous/algorithm/parallel_for.hpp>
#include <boost/asynchronous/algorithm/parallel_transform.hpp>
#include <boost/asynchronous/extensions/asio/asio_deadline_timer.hpp>
#include <boost/asynchronous/extensions/asio/asio_scheduler.hpp>
#include <boost/asynchronous/helpers/random_provider.hpp>
#include <boost/asynchronous/queue/lockfree_queue.hpp>
#include <boost/asynchronous/scheduler/single_thread_scheduler.hpp>
#include <boost/asynchronous/scheduler/threadpool_scheduler.hpp>
#include <boost/asynchronous/scheduler_shared_proxy.hpp>
#include <boost/asynchronous/servant_proxy.hpp>
#include <boost/asynchronous/trackable_servant.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/thread/shared_mutex.hpp>

#include <fmt/format.h>

#include <y5.h>

#include <cstring>
#include <functional>
#include <iostream>
#include <limits>

#if defined(__clang__)
#define tailcall [[clang::musttail]]
#else
#define tailcall /* No support for this in GCC/MSVC/... */
#endif

namespace parallel
{

[[noreturn]] void fatal(const std::string &message)
{
    // Handle exceptions in parallel code by just aborting
    std::cerr << (message + "\n") << std::flush;
    std::terminate();
}

template <typename T>
auto unwrap(boost::asynchronous::expected<T> res, const std::string &context_name = "")
{
    try
    {
        return res.get();
    }
    catch (const std::logic_error &e)
    {
        fatal(e.what());
    }
    catch (...)
    {
        fatal(std::string { "Unknown exception in " } + context_name);
    }
}

constexpr inline unsigned RETRY_TIMEOUT = 10u; // milliseconds

//! Set up threads as required
auto inline make_single_thread_scheduler()
{
    return boost::asynchronous::make_shared_scheduler_proxy<boost::asynchronous::single_thread_scheduler<boost::asynchronous::lockfree_queue<>>>();
}
auto inline make_threadpool(unsigned long thread_count)
{
    return boost::asynchronous::make_shared_scheduler_proxy<boost::asynchronous::threadpool_scheduler<boost::asynchronous::lockfree_queue<>>>(thread_count);
}
auto inline make_timer(unsigned ms)
{
    return std::make_shared<boost::asynchronous::asio_deadline_timer_proxy>(
        boost::asynchronous::asio_deadline_timer_proxy(
            boost::asynchronous::make_shared_scheduler_proxy<boost::asynchronous::asio_scheduler<>>(1),
            boost::posix_time::milliseconds(ms)
        )
    );
}
using timer_t = std::shared_ptr<boost::asynchronous::asio_deadline_timer_proxy>;

//! Throttle input generation based on "memory usage" (number of blocks in processing)
//! and monitor progress based on that.
class monitor
{
public:
    monitor(std::function<void()> on_done, std::size_t limit_in_flight) : m_in_flight(0), m_source(0), m_sink(0), m_source_complete(false), m_limit(limit_in_flight), m_done(std::move(on_done)) {}

    bool acquire()
    {
        auto old_count = m_in_flight++;
        if (old_count < m_limit)
        {
            ++m_source;
            return true;
        }
        else
        {
            --m_in_flight;
            return false;
        }
    }
    void release()
    {
        ++m_sink;
        --m_in_flight;
        if (m_source_complete && !m_in_flight && m_source == m_sink && m_done)
        {
            // Done!
            m_done();
            m_done = {}; // Only do this once.
        }
    }
    void mark_complete() { m_source_complete = true; }
    std::size_t in_flight() const { return m_in_flight; }

private:
    std::atomic<std::size_t> m_in_flight;
    std::atomic<std::size_t> m_source;
    std::atomic<std::size_t> m_sink;
    std::atomic<bool> m_source_complete;
    std::size_t m_limit;
    std::function<void()> m_done;
};

namespace io
{

struct row_segment
{
    std::unique_ptr<unsigned char[]> data;
    std::size_t entries;
};

//! Reads all rows in parallel
//! NB: Use the tuneables to reduce memory consumption significantly - settings are per row!
class column_reader_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;

    column_reader_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                          boost::asynchronous::any_shared_scheduler_proxy<> pool,
                          std::shared_ptr<y5::reader> reader,
                          std::function<void(std::size_t, std::vector<row_segment>)> consumer,
                          std::shared_ptr<monitor> monitor,
                          unsigned block_size)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_consumer(std::move(consumer))
        , m_monitor(std::move(monitor))
        , m_block_size(block_size)
        , m_block_index(0)
        , m_entry_size(reader->header().entry_size)
        , m_columns_remaining(reader->header().max_columns)
    {
        for (std::size_t i = 0; i < reader->header().rows; ++i)
            m_readers.push_back({ reader->detach(), false });
    }

    void generate()
    {
        auto block_size = m_block_size;
        auto entry_size = m_entry_size;
        auto begin = m_readers.begin();
        auto end = m_readers.end();
        if (m_columns_remaining && m_monitor->acquire())
        {
            m_columns_remaining -= std::min(m_block_size, m_columns_remaining);
            auto index = m_block_index++;
            auto output = std::make_shared<std::vector<row_segment>>(m_readers.size());
            // NB: We need to ensure that we only ever _generate_ one block at a time,
            // otherwise we race on the decompressors.
            this->post_callback(
                [block_size, entry_size, begin, end, output]() mutable
                {
                    return boost::asynchronous::parallel_transform(
                        begin,
                        end,
                        output->begin(),
                        [block_size, entry_size](std::pair<std::shared_ptr<y5::reader::row_reader>, bool> &state) -> row_segment
                        {
                            if (state.second) // already done
                                return { nullptr, 0 };
                            auto allocation = std::make_unique<unsigned char[]>(block_size * entry_size);
                            auto [count, row_done] = state.first->read_buffer(allocation.get(), block_size);
                            if (row_done)
                                state.second = true;
                            return { std::move(allocation), count };
                        },
                        100u,
                        "column_reader_servant::generate::parallel_transform"
                    );
                },
                [this, index, output](auto res) mutable
                {
                    (void) unwrap(res, "column_reader_servant::generate");
                    m_consumer(index, std::move(*output));
                    this->generate(); // Next please
                }
            );
        }
        else if (m_columns_remaining)
        {
            // Ratelimited, try again after the timeout
            m_timer = make_timer(RETRY_TIMEOUT);
            this->async_wait(
                *m_timer,
                [this](auto err) mutable
                {
                    if (!!err) return;
                    this->generate();
                }
            );
        }
        else
        {
            m_monitor->mark_complete();
        }
    }

private:
    std::size_t m_block_size;
    std::size_t m_block_index;
    std::size_t m_entry_size;
    std::size_t m_columns_remaining;
    std::function<void(std::size_t, std::vector<row_segment>)> m_consumer;
    std::shared_ptr<monitor> m_monitor;
    std::vector<std::pair<std::shared_ptr<y5::reader::row_reader>, bool>> m_readers;
    timer_t m_timer;
};

//! Threadsafe proxy for a column_reader_servant
class column_reader_proxy : public boost::asynchronous::servant_proxy<column_reader_proxy, column_reader_servant>
{
public:
    template <typename Scheduler>
    column_reader_proxy(Scheduler scheduler,
                        boost::asynchronous::any_shared_scheduler_proxy<> pool,
                        std::shared_ptr<y5::reader> reader,
                        std::function<void(std::size_t, std::vector<row_segment>)> consumer,
                        std::shared_ptr<monitor> monitor,
                        unsigned block_size)
        : boost::asynchronous::servant_proxy<column_reader_proxy, column_reader_servant>(scheduler, pool, std::move(reader), std::move(consumer), std::move(monitor), block_size)
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("column_reader_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("column_reader_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(generate, "column_reader_servant::generate", 0);
};

//! Read blocks of rows
//! NB: Use the tuneables to reduce memory consumption significantly - settings are per row, and we decompress blocks in parallel!
class row_reader_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;

    row_reader_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                          boost::asynchronous::any_shared_scheduler_proxy<> pool,
                          std::shared_ptr<y5::reader> reader,
                          std::function<void(std::size_t, std::vector<row_segment>)> consumer,
                          std::shared_ptr<monitor> monitor,
                          unsigned block_size)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_block_size(block_size)
        , m_block_index(0)
        , m_entry_size(reader->header().entry_size)
        , m_rows_remaining(reader->header().rows)
        , m_consumer(std::move(consumer))
        , m_monitor(std::move(monitor))
        , m_reader(std::move(reader))
    {}

    void generate()
    {
        auto block_size = m_block_size;
        auto entry_size = m_entry_size;
        if (m_rows_remaining && m_monitor->acquire())
        {
            auto rows_here = std::min(m_block_size, m_rows_remaining);
            m_rows_remaining -= rows_here;
            auto index = m_block_index++;
            auto output = std::make_shared<std::vector<row_segment>>(rows_here);

            auto readers = std::make_shared<std::vector<std::shared_ptr<y5::reader::row_reader>>>();
            for (std::size_t i = 0; i < rows_here; ++i)
                readers->push_back(m_reader->detach());

            // NB: We need to ensure that we only ever _generate_ on block at a time,
            // otherwise we race on the decompressors.
            this->post_callback(
                [entry_size, readers, output]() mutable
                {
                    return boost::asynchronous::parallel_transform(
                        readers->begin(),
                        readers->end(),
                        output->begin(),
                        [entry_size](const std::shared_ptr<y5::reader::row_reader> &reader) -> row_segment
                        {
                            auto entries = reader->row_header().columns;
                            auto remaining = entries;
                            auto allocation = std::make_unique<unsigned char[]>(entry_size * entries);
                            auto pointer = allocation.get();
                            while (true)
                            {
                                auto [count, row_done] = reader->read_buffer(pointer, remaining);
                                if (row_done)
                                    break;
                                pointer += count * entry_size;
                                remaining -= count;
                            }
                            return { std::move(allocation), entries };
                        },
                        100u,
                        "row_reader_servant::generate::parallel_transform"
                    );
                },
                [this, index, readers, output](auto res) mutable
                {
                    (void) unwrap(res, "row_reader_servant::generate");
                    m_consumer(index, std::move(*output));
                    this->generate(); // Next please
                }
            );
        }
        else if (m_rows_remaining)
        {
            // Ratelimited, try again after the timeout
            m_timer = make_timer(RETRY_TIMEOUT);
            this->async_wait(
                *m_timer,
                [this](auto err) mutable
                {
                    if (!!err) return;
                    this->generate();
                }
            );
        }
        else
        {
            m_monitor->mark_complete();
        }
    }

private:
    std::size_t m_block_size;
    std::size_t m_block_index;
    std::size_t m_entry_size;
    std::size_t m_rows_remaining;
    std::function<void(std::size_t, std::vector<row_segment>)> m_consumer;
    std::shared_ptr<monitor> m_monitor;
    std::shared_ptr<y5::reader> m_reader;
    timer_t m_timer;
};

//! Threadsafe proxy for a column_reader_servant
class row_reader_proxy : public boost::asynchronous::servant_proxy<row_reader_proxy, row_reader_servant>
{
public:
    template <typename Scheduler>
    row_reader_proxy(Scheduler scheduler,
                        boost::asynchronous::any_shared_scheduler_proxy<> pool,
                        std::shared_ptr<y5::reader> reader,
                        std::function<void(std::size_t, std::vector<row_segment>)> consumer,
                        std::shared_ptr<monitor> monitor,
                        unsigned block_size)
        : boost::asynchronous::servant_proxy<row_reader_proxy, row_reader_servant>(scheduler, pool, std::move(reader), std::move(consumer), std::move(monitor), block_size)
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("row_reader_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("row_reader_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(generate, "row_reader_servant::generate", 0);
};

//! Order the incoming blocks and write them to a file
class row_writer_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;

    row_writer_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                       boost::asynchronous::any_shared_scheduler_proxy<> pool,
                       std::shared_ptr<y5::writer> writer,
                       std::shared_ptr<monitor> monitor)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_writer(std::move(writer))
        , m_monitor(std::move(monitor))
        , m_next_block(0)
    {}

    void write_rows(std::size_t block_index, std::vector<row_segment> rows)
    {
        if (block_index == m_next_block)
        {
            ++m_next_block;

            try
            {
                // Write the individual rows
                for (auto &row : rows)
                {
                    if (!row.data) // Drop rows that point to nullptr.
                        continue;
                    m_writer->append_buffer(row.data.get(), row.entries);
                    m_writer->end_row();
                }
            }
            catch (const y5::detail::failed_assertion &e)
            {
                fatal(e.what());
            }
            m_monitor->release();

            // Free memory in the pool
            this->post_callback([rows = std::move(rows)]() mutable {}, [](boost::asynchronous::expected<void> res) { (void) unwrap(res, "row_writer_servant::write_rows"); });
        }
        else
        {
            // Not your turn yet, try again after the timeout
            m_pending.push_back({ block_index, std::move(rows) });
            m_timer = make_timer(RETRY_TIMEOUT);
            this->async_wait(
                *m_timer,
                // Extra make_shared to work around an issue with std::vector's static_asserts that try to avoid memcpy() of non-copyable objects
                [this](auto err) mutable
                {
                    if (!!err) return;
                    // Grab pending entries, clear the pending list
                    decltype(m_pending) to_process;
                    std::swap(to_process, m_pending);
                    for (auto &&[block_index, rows] : to_process)
                        this->write_rows(block_index, std::move(rows));
                }
            );
        }
    }

private:
    std::shared_ptr<y5::writer> m_writer;
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_next_block;
    std::vector<std::pair<std::size_t, std::vector<row_segment>>> m_pending;
    timer_t m_timer;
};

//! Threadsafe proxy for a row_writer_servant
class row_writer_proxy : public boost::asynchronous::servant_proxy<row_writer_proxy, row_writer_servant>
{
public:
    template <typename Scheduler>
    row_writer_proxy(Scheduler scheduler, boost::asynchronous::any_shared_scheduler_proxy<> pool, std::shared_ptr<y5::writer> writer, std::shared_ptr<monitor> monitor)
        : boost::asynchronous::servant_proxy<row_writer_proxy, row_writer_servant>(scheduler, pool, std::move(writer), std::move(monitor))
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("row_writer_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("row_writer_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(write_rows, "row_writer_servant::write_rows", 0);
};

} // namespace io

//! Transposes and filters the input data
class transposing_filter_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;
    using filter_function = std::function<bool(const unsigned char *reference, const unsigned char *next)>;
    using transform_function = std::function<void(unsigned char *dst, const unsigned char *src)>;

    static filter_function no_filter()
    {
        return [](const unsigned char *reference, const unsigned char *next) -> bool { return true; };
    }

    static filter_function distinct_filter(std::size_t input_entry_size)
    {
        return [input_entry_size](const unsigned char *reference, const unsigned char *next) -> bool
        {
            return memcmp(reference, next, input_entry_size) != 0;
        };
    }

    static transform_function no_transform(std::size_t entry_size)
    {
        return [entry_size](unsigned char *dst, const unsigned char *src)
        {
            memcpy(dst, src, entry_size);
        };
    }

    transposing_filter_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                               boost::asynchronous::any_shared_scheduler_proxy<> pool,
                               std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                               std::shared_ptr<monitor> monitor,
                               std::size_t input_entry_size,
                               std::size_t output_entry_size,
                               filter_function filter,
                               transform_function transform)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_consumer(std::move(consumer))
        , m_monitor(std::move(monitor))
        , m_input_entry_size(input_entry_size)
        , m_output_entry_size(output_entry_size)
        , m_filter(std::move(filter))
        , m_transform(std::move(transform))
    {}

    void transpose_and_filter(std::size_t block_index, std::vector<io::row_segment> segments)
    {
        auto input = std::make_shared<std::vector<io::row_segment>>(std::move(segments));
        auto begin = input->begin();
        auto end = input->end();
        // Find the smallest range entry in a single thread (usually we don't have that many traces that that would be worth parallelizing)
        std::size_t min_size = std::numeric_limits<std::size_t>::max();
        for (const auto &seg : *input)
            min_size = std::min(min_size, seg.entries);
        // Make output buffer:
        auto output = std::make_shared<std::vector<io::row_segment>>(min_size); // #output_rows = min(#input_cols)
        auto traces = input->size(); // #output_cols = #input_rows
        auto allocation_size = traces * m_output_entry_size;
        // TODO: This discards ragged ends, maybe we should fix that.
        if (traces <= 0)
            throw std::logic_error("Got an empty set of traces to filter");

        // We could use parallel_find_all to discard the empty rows, but since we need to iterate anyways, it seems
        // to be more efficient to just drop them in the writer.
        this->post_callback(
            [input, output, traces, allocation_size, input_entry_size = m_input_entry_size, output_entry_size = m_output_entry_size, filter = m_filter, transform = m_transform]() mutable
            {
                return boost::asynchronous::parallel_for(
                    static_cast<std::size_t>(0), static_cast<std::size_t>(output->size()),
                    [input, output, traces, allocation_size, input_entry_size, output_entry_size, filter = std::move(filter), transform = std::move(transform)](std::size_t index) mutable
                    {
                        // Do the filtering
                        auto &output_segment = output->operator[](index);
                        auto allocation = std::make_unique<unsigned char[]>(allocation_size);
                        auto reference = &input->operator[](0).data[index * input_entry_size];
                        transform(allocation.get(), reference);
                        bool ok = false;
                        for (std::size_t trace = 1; trace < traces; ++trace)
                        {
                            auto entry = &input->operator[](trace).data[index * input_entry_size];
                            if (!ok)
                                ok |= filter(reference, entry);
                            transform(&allocation[trace * output_entry_size], entry);
                        }
                        if (!ok)
                            output_segment = io::row_segment { nullptr, 0 };
                        else
                            output_segment = io::row_segment { std::move(allocation), traces };
                    },
                    100u
                );
            },
            [this, input, output, block_index](boost::asynchronous::expected<void> res)
            {
                (void) unwrap(res, "transposing_filter_servant::transpose_and_filter");
                m_consumer(block_index, std::move(*output));
            }
        );
    }

private:
    std::function<void(std::size_t, std::vector<io::row_segment>)> m_consumer;
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_input_entry_size;
    std::size_t m_output_entry_size;
    filter_function m_filter;
    transform_function m_transform;
};

//! Threadsafe proxy for a transposing_filter_servant
class transposing_filter_proxy : public boost::asynchronous::servant_proxy<transposing_filter_proxy, transposing_filter_servant>
{
public:
    template <typename Scheduler>
    transposing_filter_proxy(Scheduler scheduler,
                             boost::asynchronous::any_shared_scheduler_proxy<> pool,
                             std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                             std::shared_ptr<monitor> monitor,
                             std::size_t input_entry_size,
                             std::size_t output_entry_size,
                             transposing_filter_servant::filter_function filter,
                             transposing_filter_servant::transform_function transform)
        : boost::asynchronous::servant_proxy<transposing_filter_proxy, transposing_filter_servant>(scheduler, pool, std::move(consumer), std::move(monitor), input_entry_size, output_entry_size, std::move(filter), std::move(transform))
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("transposing_filter_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("transposing_filter_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(transpose_and_filter, "transposing_filter_servant::transpose_and_filter", 0);
};


//! Filters the input data by row
class deduplicating_filter_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;

    using hash_type = std::uint32_t;
    using hash_function = std::function<hash_type(const unsigned char *memory, std::size_t entries)>;
    using compare_function = std::function<bool(const unsigned char *original, std::size_t original_entries, const unsigned char *current, std::size_t current_entries)>; // Returns whether this _is_ the same

    static hash_function partial_djb2_hash(std::size_t entry_begin, std::size_t entry_end, std::size_t entry_size)
    {
        const std::size_t per_entry = entry_end - entry_begin;
        const std::size_t entry_skip = (entry_size - entry_end) + entry_begin;
        return [entry_begin, per_entry, entry_skip](const unsigned char *row, std::size_t entries)
        {
            hash_type hash = 5381;
            const unsigned char *ptr = row + entry_begin;
            for (std::size_t index = 0; index < entries; ++index)
            {
                for (std::size_t in_entry = 0; in_entry < per_entry; ++in_entry)
                    hash = ((hash << 5) + hash) + *ptr++;
                ptr += entry_skip;
            }
            return hash;
        };
    }

    static compare_function equality_comparison(std::size_t entry_begin, std::size_t entry_end, std::size_t entry_size)
    {
        const std::size_t entry_comparison = entry_end - entry_begin;
        return [entry_begin, entry_size, entry_comparison](const unsigned char *original, std::size_t original_entries, const unsigned char *current, std::size_t current_entries)
        {
            if (original_entries != current_entries)
                return false;
            for (std::size_t index = 0; index < current_entries; ++index, original += entry_size, current += entry_size)
                if (memcmp(original + entry_begin, current + entry_begin, entry_comparison) != 0)
                    return false; // Not a duplicate
            return true;
        };
    }

    enum class drop_result
    {
        successful,
        different_element,
        no_such_element
    };

    class tree_node_guard
    {
        constexpr static std::size_t STALL_THRESHOLD = 0x8000;
        constexpr static bool STALL_MESSAGE = false;

        std::size_t stalled()
        {
            if (STALL_MESSAGE)
            {
                std::ostringstream message;
                message << "\nWarning: Thread " << std::this_thread::get_id() << " stalling on guard access to address " << reinterpret_cast<void *>(&m_target) << " (value " << m_target.load() << ")\n";
                std::cerr << message.str() << std::flush;
            }
            std::this_thread::yield();
            return 0;
        }

    public:
        constexpr static std::size_t Locked = static_cast<std::size_t>(-1);
        tree_node_guard(std::atomic<std::size_t> &target)
            : m_target(target)
            , m_locked(false)
        {
            std::size_t stall = 0;
            while (true)
            {
                auto value = m_target.load();
                // wait/notify either just spins on a separate spinlock (so is superfluous) or uses futexes (slow).
                if (value != Locked && m_target.compare_exchange_strong(value, value + 1))
                    break;
                if (++stall > STALL_THRESHOLD)
                    stall = stalled();
            }
        }

        ~tree_node_guard()
        {
            if (m_locked)
                fatal("Forgot to unlock a guarded value");
            auto value = --m_target;
            if (value == Locked || value == Locked - 1)
                fatal("Accidentally switched lock states");
        }

        void lock()
        {
            if (m_locked)
                fatal("Cannot lock guarded value twice");
            m_locked = true;
            auto value = --m_target; // Free our own reference to allow another thread to lock first.
            // We will get that one back in unlock().
            if (value == Locked || value == Locked - 1)
                fatal("Accidentally switched lock states");

            std::size_t stall = 0;
            while (true)
            {
                auto value = m_target.load();
                // wait/notify either just spins on a separate spinlock (so is superfluous) or uses futexes (slow).
                if (value == 0 && m_target.compare_exchange_strong(value, Locked))
                    break;
                if (++stall > STALL_THRESHOLD)
                    stall = stalled();
            }
        }

        void unlock()
        {
            if (!m_locked)
                fatal("Cannot unlock guarded value (was never locked)");
            auto expected = Locked;
            if (!m_target.compare_exchange_strong(expected, 1))
                fatal("Failed to unlock guarded value (value was replaced while locked)");
            m_locked = false;
        }

    private:
        std::atomic<std::size_t> &m_target;
        bool m_locked;
    };

    // A dynamic tree that avoids rebalancing.
    template <typename Hash, typename Value, std::size_t BitsPerLayer = 4, std::size_t Used = 0>
    class atomic_hash_tree_node
    {
        constexpr static bool Leaf = Used >= (sizeof(Hash) * 8 - BitsPerLayer);
        constexpr static Hash Shift = sizeof(Hash) * 8 - Used - BitsPerLayer;
        constexpr static Hash Mask = ((static_cast<Hash>(1) << BitsPerLayer) - 1) << Shift;
        constexpr static std::size_t Entries = static_cast<std::size_t>(1) << BitsPerLayer;
        using NodeType = std::conditional_t<Leaf, Value, atomic_hash_tree_node<Hash, Value, BitsPerLayer, Used + BitsPerLayer>>;

        constexpr static inline std::size_t index_of(Hash bits)
        {
            return (bits & Mask) >> Shift;
        }

    public:
        Value *put(Hash bits, Value *value)
        {
            if (!value)
                fatal("Cannot put() an empty value!");
            if constexpr (Leaf)
            {
                // Leaf node, exchange the value that is there always.
                auto result = m_children[index_of(bits)].exchange(std::move(value));
                if (!result) // We actually added an entry.
                    ++m_entries;
                return result;
            }
            else
            {
                // Not a leaf node. Check if we have a value in the correct child.
                std::size_t index = index_of(bits);
                while (true)
                {
                    [[maybe_unused]] tree_node_guard lock { m_consolidating[index] };
                    auto child = m_children[index].load();
                    if (child)
                        return child->put(bits, std::move(value));

                    auto replacement = new NodeType {};
                    if (m_children[index].compare_exchange_strong(child, replacement))
                    {
                        // Previous value was null, else we wouldn't be here.
                        ++m_entries;
                        return replacement->put(bits, std::move(value));
                    }
                    delete replacement;
                }
            }
        }

        Value *get_or_create(Hash bits)
        {
            // Fetch and return if a value is there, or create one.
            std::size_t index = index_of(bits);
            while (true)
            {
                if constexpr (Leaf)
                {
                    auto child = m_children[index].load();
                    if (child)
                        return child;

                    auto replacement = new NodeType {};
                    if (m_children[index].compare_exchange_strong(child, replacement))
                    {
                        // Added an entry.
                        ++m_entries;
                        return replacement;
                    }
                    delete replacement;
                }
                else
                {
                    [[maybe_unused]] tree_node_guard lock { m_consolidating[index] };
                    auto child = m_children[index].load();
                    if (child)
                        return child->get_or_create(bits);

                    auto replacement = new NodeType {};
                    if (m_children[index].compare_exchange_strong(child, replacement))
                    {
                        // Added an entry.
                        ++m_entries;
                        return replacement->get_or_create(bits);
                    }
                    delete replacement;
                }
            }
        }

        Value *drop(Hash bits, bool *left_empty = nullptr)
        {
            if (!m_entries)
            {
                if (left_empty)
                    *left_empty = true;
                return nullptr;
            }
            std::size_t index = index_of(bits);
            if constexpr (Leaf)
            {
                // Try to replace with an empty
                auto result = m_children[index].exchange(nullptr);
                if (result)
                    if (!--m_entries)
                        if (left_empty)
                            *left_empty = true;
                return result;
            }
            else
            {
                tree_node_guard lock { m_consolidating[index] };
                auto child = m_children[index].load();
                if (!child)
                    return nullptr; // No child here means the node doesn't exist
                bool child_now_empty = false;
                auto result = child->drop(bits, &child_now_empty);
                // Clean up empty children now.
                if (child_now_empty)
                {
                    // Here's the problem: We can't remove the child before checking that the entry count is still 0, and we can't guarantee the entry count stays 0 while removing the child.
                    // This means for consolidation we need to lock the current node in a way that doesn't interfere with the fast path.
                    NodeType *empty = nullptr;
                    lock.lock(); // Ensure m_consolidating[index] drops to 0 first. Now everyone should be polling against that.
                    auto actual_value = m_children[index].load();
                    if (actual_value == child && child->is_empty())
                    {
                        if (!m_children[index].compare_exchange_strong(actual_value, empty))
                            fatal("Failed to remove child node");
                        if (m_children[index] != nullptr)
                            fatal("Failed to remove child node");

                        // OK, we removed one of our own children.
                        if (!--m_entries)
                            if (left_empty)
                                *left_empty = true;
                        empty = child;
                    }
                    lock.unlock();
                    if (empty) // Now noone should be holding references to this child anymore. Destroy only after unlock.
                        delete empty;
                }
                return result;
            }
        }

        drop_result drop_if(Hash bits, Value *expected, bool *left_empty = nullptr)
        {
            if (!m_entries)
            {
                if (left_empty)
                    *left_empty = true;
                return drop_result::no_such_element;
            }
            std::size_t index = index_of(bits);
            if constexpr (Leaf)
            {
                // Try to replace with an empty entry
                Value *empty = nullptr;
                if (m_children[index].compare_exchange_strong(expected, empty))
                {
                    // Removed an entry.
                    if (!--m_entries)
                        if (left_empty)
                            *left_empty = true;
                    return drop_result::successful;
                }
                else if (!expected) // No entry there
                    return drop_result::no_such_element;
                else // Other entry there
                    return drop_result::different_element;
            }
            else
            {
                tree_node_guard lock { m_consolidating[index] };
                auto child = m_children[index].load();
                if (!child)
                    return drop_result::no_such_element; // Sequenced at a point where the node does not exist - so we don't have to delete anything!
                bool child_now_empty = false;
                auto result = child->drop_if(bits, std::move(expected), &child_now_empty);
                // Clean up empty children now.
                if (child_now_empty)
                {
                    // Here's the problem: We can't remove the child before checking that the entry count is still 0, and we can't guarantee the entry count stays 0 while removing the child.
                    // This means for consolidation we need to lock the current node in a way that doesn't interfere with the fast path.
                    NodeType *empty = nullptr;
                    lock.lock(); // Ensure m_consolidating[index] drops to 0 first. Now everyone should be polling against that.
                    auto actual_value = m_children[index].load();
                    if (actual_value == child && child->is_empty())
                    {
                        if (!m_children[index].compare_exchange_strong(actual_value, empty))
                            fatal("Failed to remove child node");
                        if (m_children[index] != nullptr)
                            fatal("Failed to remove child node");

                        // OK, we removed one of our own children.
                        if (!--m_entries)
                            if (left_empty)
                                *left_empty = true;
                        empty = child;
                    }
                    lock.unlock();
                    if (empty) // Now noone should be holding references to this child anymore. Destroy only after unlock.
                        delete empty;
                }
                return result;
            }
        }

        std::pair<Hash, Value *> minimum()
        {
            if (!m_entries)
                return { static_cast<Hash>(0), nullptr }; // Fast path without locking.

            for (std::size_t index = 0; index < Entries; ++index)
            {
                if constexpr (Leaf)
                {
                    auto child = m_children[index].load();
                    if (!child)
                        continue;
                    return { index << Shift, child };
                }
                else
                {
                    [[maybe_unused]] tree_node_guard lock { m_consolidating[index] };
                    auto child = m_children[index].load();
                    if (!child)
                        continue;

                    auto [hash, value] = child->minimum();
                    if (!value)
                        continue;

                    return { (index << Shift) | hash, value };
                }
            }
            return { static_cast<Hash>(0), nullptr };
        }

        bool is_empty() const
        {
            return !m_entries;
        }

    private:
        std::array<std::atomic<NodeType *>, Entries> m_children = {};
        std::atomic<std::size_t> m_entries = 0;
        std::array<std::atomic<std::size_t>, Leaf ? 0 : Entries> m_consolidating = {}; // We only need this in non-leaf nodes
    };

    template <typename Hash, typename Value, typename Comparison = std::equal_to<Value>>
    class atomic_lru_cache
    {
        constexpr static std::size_t NOT_IN_LRU = 0;
        constexpr static std::size_t LRU_RESCALE_LIMIT = static_cast<std::size_t>(0xFFFFFFFF) << 32;
        constexpr static std::size_t CACHE_NODE_MAGIC = 0x656863616375726c;
        constexpr static std::size_t CONTENTION_LIMIT = 4;

        struct cache_node
        {
            std::atomic<std::size_t> magic = CACHE_NODE_MAGIC;
            Hash hash = 0;
            std::atomic<std::size_t> lru = NOT_IN_LRU;
            boost::shared_mutex node_mutex = {};
            std::vector<Value> values = {};
            std::atomic<std::uint16_t> users = 0;
            constexpr static std::uint16_t REMOVING = static_cast<std::uint16_t>(-1);
        };

        void update_lru(cache_node *node)
        {
            auto old_index = node->lru.exchange(NOT_IN_LRU);
            if (old_index != NOT_IN_LRU)
                if (m_lru.drop_if(old_index, node) == drop_result::different_element)
                    fatal("Inconsistent LRU state");
            auto new_index = ++m_lru_counter;
            if (new_index > LRU_RESCALE_LIMIT) // This will never happen. 2**64 - 2**32 is way more than we'll ever need.
                fatal("LRU would wrap 'soon', support for that is not implemented");

            auto current_index = NOT_IN_LRU;
            if (!node->lru.compare_exchange_strong(current_index, new_index))
                return; // We didn't get to do the update, someone else did. To keep the tree consistent, don't mess with it anymore.
            if (m_lru.put(new_index, node) != nullptr)
                fatal("LRU tried to reuse index");
            if (node->lru.load() != new_index) // Already updated again.
                m_lru.drop_if(new_index, node);
        }

        cache_node *get_eviction_target()
        {
            while (true)
            {
                auto [index, least_recent] = m_lru.minimum();
                if (!least_recent)
                {
                    if (m_lru.is_empty())
                        return {}; // OK, cache is (still) empty...
                    if (m_size)
                        continue;
                    else
                        return {};
                }

                // Wait for that node to be out of use.
                while (true)
                {
                    std::uint16_t expected_user_count = 0;
                    if (least_recent->users == cache_node::REMOVING)
                        break; // Node is already being removed, we need to evict again.
                    if (least_recent->lru != index)
                        break; // Node shifted.
                    if (least_recent->magic != CACHE_NODE_MAGIC)
                        break; // Got an invalid node

                    if (least_recent->users.compare_exchange_strong(expected_user_count, cache_node::REMOVING))
                    {
                        // OK, we set the node to "removing" state. This may be "after free" (but see below), so we need to check the magic again.
                        if (least_recent->magic != CACHE_NODE_MAGIC) {
                            least_recent->users = expected_user_count;
                            (void) m_lru.drop(index);
                            // NB: Do _NOT_ delete the dropped entry here - this may already be a UaF! I'd rather leak some memory (this is rare) than crash
                            break; // Got an invalid node
                        }
                        if (least_recent->lru != index) {
                            ++least_recent->users; // This is more likely to occur during some form of "race", so increment to counteract the move from 0 to REMOVING (-1) instead of resetting.
                            if (least_recent->lru < index) // Cache index decreased (probably UaF), but we can drop it anyways since we select the minimum always
                                (void) m_lru.drop(least_recent->lru);
                            (void) m_lru.drop(index); // Remove the old entry just in case
                            // NB: Do _NOT_ delete the dropped entry here - this may already be a UaF! I'd rather leak some memory (this is rare) than crash
                            break; // Node shifted (second check, after "locking").
                        }
                        return least_recent;
                    }
                }
            }
        }

        void evict_lru()
        {
            cache_node *least_recent;
            least_recent = get_eviction_target();
            if (!least_recent)
                return; // Cache is empty?

            // No matter whether we manage to properly drop this entry
            // or get a new one back: this one is gone from the LRU and
            // now also the tree.
            m_root.drop_if(least_recent->hash, least_recent);
            if (m_lru.drop_if(least_recent->lru, least_recent) == drop_result::different_element)
                fatal("Inconsistent LRU state during eviction");

            // Overwrite the LRU cache index and magic number so that any pending eviction will not use this node again, even if we do race against it
            least_recent->lru = NOT_IN_LRU;
            least_recent->magic = 0;

            // Now noone is using it. Grab the lock once anyways, for good measure.
            try
            {
                boost::unique_lock node_lock(least_recent->node_mutex);
                m_size -= least_recent->values.size();
            }
            catch (const boost::lock_error &error)
            {
                fatal(fmt::format("Failed to lock node {} for eviction: {}", reinterpret_cast<void *>(least_recent), error.what()));
            }

            // NB: After free(), this could be occupied by anything! If it's not a cache node, the magic number will be wrong so we
            // won't try to evict it again. If it is a cache node, the LRU index (monotonic) will be wrong, so we also won't be using it.
            delete least_recent;
        }

    public:
        atomic_lru_cache(std::size_t capacity, Comparison comparison)
            : m_compare(std::move(comparison))
            , m_capacity(capacity)
            , m_size(0)
        {}

        template <typename Insertable>
        bool lookup_and_cache(Hash hash, Insertable &&value)
        {
            cache_node *node;

            do { node = m_root.get_or_create(hash); } while (!node || node->users == cache_node::REMOVING);

            while (true)
            {
                auto expected_user_count = node->users.load();
                if (expected_user_count == cache_node::REMOVING)
                    tailcall return lookup_and_cache(hash, std::forward<Insertable>(value));
                if (node->users.compare_exchange_strong(expected_user_count, expected_user_count + 1))
                    break;
            }
            node->hash = hash; // Always written to the same value.

            update_lru(node);

            try
            {
                boost::upgrade_lock node_lock(node->node_mutex); // This reader lock prevents reallocation / modification during lookup

                if (!node->values.empty())
                {
                    // Walk the list with only this node locked.
                    // This should greatly reduce overhead.
                    for (std::size_t index = 0; index < node->values.size(); ++index)
                    {
                        const auto &entry = node->values.operator[](index);
                        if (m_compare(entry, value))
                        {
                            --node->users;
                            return true; // Not unique, this is a duplicate
                        }
                    }
                }

                // This one is not in the list.
                try
                {
                    boost::upgrade_to_unique_lock upgraded_node_lock(node_lock);
                    node->values.push_back(std::forward<Insertable>(value));
                }
                catch (const boost::lock_error &error)
                {
                    fatal(fmt::format("Failed to upgrade lock of node {} for modification: {}", reinterpret_cast<void *>(node), error.what()));
                }
            }
            catch (const boost::lock_error &error)
            {
                fatal(fmt::format("Failed to lock node {} for lookup: {}", reinterpret_cast<void *>(node), error.what()));
            }

            --node->users;

            if (++m_size >= m_capacity)
            {
                m_did_discard_cache_entries = true;
                evict_lru();
            }

            return false;
        }

        bool did_discard_cache_entries() const
        {
            return m_did_discard_cache_entries;
        }

    private:
        atomic_hash_tree_node<Hash, cache_node> m_root;
        atomic_hash_tree_node<std::size_t, cache_node> m_lru;
        Comparison m_compare;
        std::size_t m_capacity;
        std::atomic<std::size_t> m_size = 0;
        std::atomic<std::size_t> m_lru_counter = 0;
        std::atomic_bool m_did_discard_cache_entries = false;
    };

    struct row_segment_view
    {
        unsigned char *data;
        std::size_t entries;
        std::size_t entry_size;

        operator io::row_segment() const
        {
            auto memory = std::make_unique<unsigned char[]>(entry_size * entries);
            memcpy(memory.get(), data, entry_size * entries);
            return io::row_segment { std::move(memory), entries };
        }

        struct equals
        {
            compare_function m_cmp;

            bool operator()(const io::row_segment &rs, const row_segment_view &rsv)
            {
                return m_cmp(rs.data.get(), rs.entries, rsv.data, rsv.entries);
            }
        };

    };

    using cache_t = atomic_lru_cache<hash_type, io::row_segment, row_segment_view::equals>;

    deduplicating_filter_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                                 boost::asynchronous::any_shared_scheduler_proxy<> pool,
                                 std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                                 std::shared_ptr<monitor> monitor,
                                 std::size_t entry_size,
                                 std::size_t cache_capacity,
                                 hash_function hash,
                                 compare_function compare)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_consumer(std::move(consumer))
        , m_monitor(std::move(monitor))
        , m_entry_size(entry_size)
        , m_hash(std::move(hash))
        , m_cache(std::make_shared<cache_t>(cache_capacity, row_segment_view::equals { std::move(compare) }))
    {}

    void deduplicate(std::size_t block_index, std::vector<io::row_segment> segments)
    {
        auto input = std::make_shared<std::vector<io::row_segment>>(std::move(segments));
        auto entries = input->size();
        auto output = std::make_shared<std::vector<io::row_segment>>(entries);

        this->post_callback(
            [input, entries, output, entry_size = m_entry_size, hash_fn = m_hash, cache = m_cache]() mutable
            {
                return boost::asynchronous::parallel_for(
                    static_cast<std::size_t>(0), static_cast<std::size_t>(entries),
                    [input, output, entry_size, hash_fn, cache = std::move(cache)](std::size_t index) mutable {
                        auto &in = input->operator[](index);
                        auto &out = output->operator[](index);

                        hash_type hash = hash_fn(in.data.get(), in.entries);
                        bool is_duplicate = cache->lookup_and_cache(hash, row_segment_view { in.data.get(), in.entries, entry_size });
                        if (is_duplicate)
                            out = { nullptr, 0 };
                        else
                            out = std::move(in);
                    },
                    20u
                );
            },
            [this, block_index, input, output](boost::asynchronous::expected<void> res) mutable
            {
                (void) unwrap(res, "deduplicating_filter_servant::deduplicate");
                m_consumer(block_index, std::move(*output));
            }
        );
    }

    bool did_discard_cache_entries() const
    {
        return m_cache->did_discard_cache_entries();
    }

private:
    std::function<void(std::size_t, std::vector<io::row_segment>)> m_consumer;
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_entry_size;
    hash_function m_hash;
    std::shared_ptr<cache_t> m_cache;
};

//! Threadsafe proxy for a deduplicating_filter_servant
class deduplicating_filter_proxy : public boost::asynchronous::servant_proxy<deduplicating_filter_proxy, deduplicating_filter_servant>
{
public:
    template <typename Scheduler>
    deduplicating_filter_proxy(Scheduler scheduler,
                               boost::asynchronous::any_shared_scheduler_proxy<> pool,
                               std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                               std::shared_ptr<monitor> monitor,
                               std::size_t entry_size,
                               std::size_t cache_capacity,
                               deduplicating_filter_servant::hash_function hash,
                               deduplicating_filter_servant::compare_function compare)
        : boost::asynchronous::servant_proxy<deduplicating_filter_proxy, deduplicating_filter_servant>(scheduler, pool, std::move(consumer), std::move(monitor), entry_size, cache_capacity, std::move(hash), std::move(compare))
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("deduplicating_filter_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("deduplicating_filter_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(deduplicate, "deduplicating_filter_servant::deduplicate", 0);
    BOOST_ASYNC_FUTURE_MEMBER_LOG(did_discard_cache_entries, "deduplicating_filter_servant::did_discard_cache_entries", 0);
};


//! Transforms the input data row by row
class transformer_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;
    using transform_function = std::function<void(unsigned char *dst, const unsigned char *src)>;

    transformer_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                        boost::asynchronous::any_shared_scheduler_proxy<> pool,
                        std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                        std::shared_ptr<monitor> monitor,
                        std::size_t input_entry_size,
                        std::size_t output_entry_size,
                        transform_function transform)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_consumer(std::move(consumer))
        , m_monitor(std::move(monitor))
        , m_input_entry_size(input_entry_size)
        , m_output_entry_size(output_entry_size)
        , m_transform(std::move(transform))
    {}

    void transform(std::size_t block_index, std::vector<io::row_segment> segments)
    {
        auto input = std::make_shared<std::vector<io::row_segment>>(std::move(segments));
        // Make output buffer:
        auto output = std::make_shared<std::vector<io::row_segment>>(input->size()); // #output_rows = #input_rows
        this->post_callback(
            [input, output, input_entry_size = m_input_entry_size, output_entry_size = m_output_entry_size, transform = m_transform]() mutable
            {
                return boost::asynchronous::parallel_for(
                    static_cast<std::size_t>(0), static_cast<std::size_t>(input->size()),
                    [input, output, input_entry_size, output_entry_size, transform = std::move(transform)](std::size_t index) mutable
                    {
                        // Do the transformation
                        auto &input_segment = input->operator[](index);
                        auto entries = input_segment.entries;
                        auto allocation = std::make_unique<unsigned char[]>(entries * output_entry_size);
                        for (std::size_t entry = 0; entry < entries; ++entry)
                            transform(&allocation[entry * output_entry_size], &input_segment.data[entry * input_entry_size]);
                        // Store
                        auto &output_segment = output->operator[](index);
                        output_segment.data = std::move(allocation);
                        output_segment.entries = entries;
                    },
                    100u
                );
            },
            [this, input, output, block_index](boost::asynchronous::expected<void> res)
            {
                (void) unwrap(res, "transformer_servant::transform");
                m_consumer(block_index, std::move(*output));
            }
        );
    }

private:
    std::function<void(std::size_t, std::vector<io::row_segment>)> m_consumer;
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_input_entry_size;
    std::size_t m_output_entry_size;
    transform_function m_transform;
};

//! Threadsafe proxy for a transformer_servant
class transformer_proxy : public boost::asynchronous::servant_proxy<transformer_proxy, transformer_servant>
{
public:
    template <typename Scheduler>
    transformer_proxy(Scheduler scheduler,
                      boost::asynchronous::any_shared_scheduler_proxy<> pool,
                      std::function<void(std::size_t, std::vector<io::row_segment>)> consumer,
                      std::shared_ptr<monitor> monitor,
                      std::size_t input_entry_size,
                      std::size_t output_entry_size,
                      transformer_servant::transform_function transform)
        : boost::asynchronous::servant_proxy<transformer_proxy, transformer_servant>(scheduler, pool, std::move(consumer), std::move(monitor), input_entry_size, output_entry_size, std::move(transform))
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("transformer_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("transformer_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(transform, "transformer_servant::transform", 0);
};


//! Logs useful progress information
class progress_log_servant : public boost::asynchronous::trackable_servant<>
{
public:
    using requires_weak_scheduler = std::true_type;

    progress_log_servant(boost::asynchronous::any_weak_scheduler<> scheduler, boost::asynchronous::any_shared_scheduler_proxy<> pool,
                         std::shared_ptr<monitor> monitor, std::size_t block_size, std::size_t blocks, std::string message)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_monitor(monitor)
        , m_block_size(block_size)
        , m_total(blocks)
        , m_action_message(message)
    {}

    void progress_reader(std::size_t block_index)
    {
        m_reader_progress = block_index * m_block_size;
        print_progress();
    }

    void progress_transform(std::size_t block_index)
    {
        m_transform_progress = block_index * m_block_size;
        print_progress();
    }

    void progress_writer(std::size_t block_index)
    {
        m_writer_progress = block_index * m_block_size;
        print_progress();
    }

protected:
    void print_progress() const
    {
        std::cout << fmt::format("{:>10} / {:>10} ({:5.1f}%) ({:>2} in flight, {:>10} read, {:>10} {})\r",
                                 m_writer_progress, m_total, 100.0 * m_writer_progress / m_total,
                                 m_monitor->in_flight(), m_reader_progress, m_transform_progress, m_action_message)
                  << std::flush;
    }

private:
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_block_size;
    std::size_t m_total;
    std::size_t m_reader_progress = 0;
    std::size_t m_transform_progress = 0;
    std::size_t m_writer_progress = 0;
    std::string m_action_message;
};

class progress_log_proxy : public boost::asynchronous::servant_proxy<progress_log_proxy, progress_log_servant>
{
public:
    template <typename Scheduler>
    progress_log_proxy(Scheduler scheduler, boost::asynchronous::any_shared_scheduler_proxy<> pool, std::shared_ptr<monitor> monitor, std::size_t block_size, std::size_t blocks, std::string message = "processed")
        : boost::asynchronous::servant_proxy<progress_log_proxy, progress_log_servant>(scheduler, pool, monitor, block_size, blocks, message)
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("progress_log_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("progress_log_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(progress_reader, "progress_log_servant::progress_reader", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(progress_transform, "progress_log_servant::progress_transform", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(progress_writer, "progress_log_servant::progress_writer", 0);
};


} // namespace parallel

#endif // PARALLEL_H

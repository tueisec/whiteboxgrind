/*
 * Copyright 2022 Tobias Holl, Katharina Bogad
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FRONTEND_H
#define FRONTEND_H

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <sys/prctl.h>

using namespace std::literals::string_literals;

class ArgumentParser
{
public:
    struct Option
    {
        std::string short_name = "";
        std::string long_name = "";
        std::string help = "";
        std::string default_value = "";
        std::string variable_name = "";
        bool required = false;
        std::set<std::string> conflicts = {};
    };
    struct Toggle
    {
        std::string short_name = "";
        std::string long_name = "";
        std::string help = "";
        std::set<std::string> conflicts = {};
        // Toggle values will be empty if not present, or the canonical name if present
    };
    struct Argument
    {
        std::string name = "";
        std::string help = "";
        std::string default_value = "";
    };
    using Result = std::map<std::string, std::string>; // Also contains default values

    ArgumentParser(const std::string &program_name)
        : m_program(program_name)
    {}

    void add(Option option)
    {
        if (option.short_name.empty() && option.long_name.empty())
            throw std::logic_error("Options must have at least either a short or long name");

        if (!option.short_name.empty())
            validate_short_name(option.short_name);
        if (!option.long_name.empty())
            validate_long_name(option.long_name);
        if (option.variable_name.empty())
            option.variable_name = default_variable_name(option.long_name);

        // If there is no long name, we use the short name as the long (canonical) name.
        if (option.long_name.empty())
            option.long_name = option.short_name;

        if (!option.short_name.empty() && m_option_lookup.count(option.short_name))
            throw std::logic_error("Duplicate option name: "s + option.short_name);
        if (m_option_lookup.count(option.long_name))
            throw std::logic_error("Duplicate option name: "s + option.long_name);

        m_options.push_back(option);
        m_option_lookup[option.long_name] = m_options.back();
        if (!option.short_name.empty())
            m_option_lookup[option.short_name] = m_options.back();
        if (option.required)
            m_required_options.insert(option.long_name);
    }

    void add(Toggle toggle)
    {
        if (toggle.short_name.empty() && toggle.long_name.empty())
            throw std::logic_error("Toggles must have at least either a short or long name");

        if (!toggle.short_name.empty())
            validate_short_name(toggle.short_name);
        if (!toggle.long_name.empty())
            validate_long_name(toggle.long_name);

        // If there is no long name, we use the short name as the long (canonical) name.
        if (toggle.long_name.empty())
            toggle.long_name = toggle.short_name;

        if (!toggle.short_name.empty() && m_toggle_lookup.count(toggle.short_name))
            throw std::logic_error("Duplicate toggle name: "s + toggle.short_name);
        if (m_toggle_lookup.count(toggle.long_name))
            throw std::logic_error("Duplicate toggle name: "s + toggle.long_name);

        m_toggles.push_back(toggle);
        m_toggle_lookup[toggle.long_name] = m_toggles.back();
        if (!toggle.short_name.empty())
            m_toggle_lookup[toggle.short_name] = m_toggles.back();
    }

    void add(Argument argument, bool required = true)
    {
        if (argument.name.empty())
            throw std::logic_error("Arguments cannot have an empty name");
        validate_argument_name(argument.name);

        m_arguments.push_back(argument);
        if (required)
            m_trailing_optional_arguments = 0;
        else
            ++m_trailing_optional_arguments;
        m_argument_lookup[argument.name] = m_arguments.back();
    }

    Result parse(int argc, const char *argv[]) const
    {
        std::vector<std::string> args { &argv[1], &argv[argc] };
        return parse(args);
    }

    Result parse(const std::vector<std::string> &args) const
    {
        std::string optional_key;
        std::size_t positional_counter = 0;
        bool disable_optional = false;

        Result result;

        // Set defaults
        for (const auto &option : m_options)
            result[option.long_name] = option.default_value;
        for (const auto &toggle: m_toggles)
            result[toggle.long_name] = "";
        for (const auto &argument: m_arguments)
            result[argument.name] = argument.default_value;

        std::set<std::string> expected = m_required_options;

        std::set<std::string> conflicting = {};
        auto conflict_check_for_option = [&, this](const auto &option) mutable
        {
            if (conflicting.count(option.long_name))
            {
                std::cerr << "Cannot use " << option.long_name << " together with any of [";
                for (auto jt = option.conflicts.cbegin(); jt != option.conflicts.cend(); ++jt)
                {
                    if (conflicting.count(option.long_name))
                    {
                        if (jt != option.conflicts.cbegin())
                            std::cerr << ", ";
                        std::cerr << *jt;
                    }
                }
                std::cerr << "].\n";
                die_with_usage(EXIT_FAILURE);
            }
        };
        auto conflict_check_by_name = [&, this](const std::string &arg) mutable
        {
            if (auto it = m_option_lookup.find(arg); it != m_option_lookup.end())
                conflict_check_for_option(it->second);
            else if (auto it = m_toggle_lookup.find(arg); it != m_toggle_lookup.end())
                conflict_check_for_option(it->second);
        };

        // Parse
        for (const auto &arg : args)
        {
            if (arg == "--")
            {
                disable_optional = true;
            }
            else if (!disable_optional && (arg == "-h" || arg == "--help"))
            {
                die_with_usage(EXIT_SUCCESS);
            }
            else if (!disable_optional && (arg[0] == '-'))
            {
                // Try to find the argument
                if (!optional_key.empty())
                {
                    std::cerr << "Warning: Treating possible option " << arg << " as an argument to " << optional_key << ".\n";
                    // Conflict checking already done
                    result[optional_key] = arg;
                    expected.erase(optional_key);
                    optional_key = {};
                }
                else if (auto offset = arg.find('='); offset != std::string::npos)
                {
                    std::string value = arg.substr(offset + 1);
                    std::string actual_arg = arg.substr(0, offset);
                    if (auto it = m_option_lookup.find(actual_arg); it != m_option_lookup.end())
                    {
                        conflict_check_for_option(it->second);
                        conflicting.insert(it->second.conflicts.cbegin(), it->second.conflicts.cend());
                        result[it->second.long_name] = value;
                        expected.erase(it->second.long_name);
                    }
                    else if (auto it = m_toggle_lookup.find(actual_arg); it != m_toggle_lookup.end())
                    {
                        std::cerr << "Cannot assign value " << value << " to binary toggle " << actual_arg << ".\n";
                        die_with_usage(EXIT_FAILURE);
                    }
                    else
                    {
                        std::cerr << "Unknown option: " << actual_arg << " (hint: use '--' to terminate option processing)\n";
                        die_with_usage(EXIT_FAILURE);
                    }
                }
                else
                {
                    if (auto it = m_option_lookup.find(arg); it != m_option_lookup.end())
                    {
                        optional_key = it->second.long_name;
                        conflict_check_for_option(it->second);
                        conflicting.insert(it->second.conflicts.cbegin(), it->second.conflicts.cend());
                    }
                    else if (auto it = m_toggle_lookup.find(arg); it != m_toggle_lookup.end())
                    {
                        conflict_check_for_option(it->second);
                        conflicting.insert(it->second.conflicts.cbegin(), it->second.conflicts.cend());
                        result[it->second.long_name] = it->second.long_name;
                    }
                    else
                    {
                        std::cerr << "Unknown option: " << arg << " (hint: use '--' to terminate option processing)\n";
                        die_with_usage(EXIT_FAILURE);
                    }
                }
            }
            else
            {
                if (!optional_key.empty())
                {
                    result[optional_key] = arg;
                    conflict_check_by_name(optional_key);
                    if (auto it = m_option_lookup.find(optional_key); it != m_option_lookup.end())
                        conflicting.insert(it->second.conflicts.cbegin(), it->second.conflicts.cend());
                    expected.erase(optional_key);
                    optional_key = {};
                }
                else
                {
                    // Positional argument
                    if (positional_counter > m_arguments.size())
                    {
                        std::cerr << "Unexpected positional argument: " << arg << "\n";
                        die_with_usage(EXIT_FAILURE);
                    }
                    result[m_arguments[positional_counter++].name] = arg;
                }
            }
        }

        if (!optional_key.empty())
        {
            std::cerr << "Missing value for option " << optional_key << ".\n";
            die_with_usage(EXIT_FAILURE);
        }

        if (!expected.empty())
        {
            std::cerr << "Missing required option " << *expected.begin() << ".\n";
            die_with_usage(EXIT_FAILURE);
        }

        if (positional_counter < m_arguments.size() - m_trailing_optional_arguments)
        {
            std::cerr << "Missing positional argument " << m_arguments[positional_counter].name << "\n";
            die_with_usage(EXIT_FAILURE);
        }

        return result;
    }

    std::string usage() const
    {
        std::ostringstream stream;
        stream << "Usage:\n"
               << "  " << m_program;
        for (const auto &toggle : m_toggles)
            stream << " [" << (toggle.short_name.empty() ? toggle.long_name : toggle.short_name) << "]";
        for (const auto &option : m_options)
        {
            if (option.required)
                stream << " " << (option.short_name.empty() ? option.long_name : option.short_name)
                       << " " << option.variable_name;
            else
                stream << " [" << (option.short_name.empty() ? option.long_name : option.short_name)
                       << " " << option.variable_name << "]";
        }
        unsigned index = 0;
        for (const auto &argument : m_arguments)
        {
            if ((m_arguments.size() - m_trailing_optional_arguments) <= index)
                stream << " [" << argument.name << "]";
            else
                stream << " " << argument.name;
            ++index;
        }
        stream << "\n\n";
        stream << "Arguments:\n";
        for (const auto &argument : m_arguments)
            stream << "  " << std::setw(30) << std::right << argument.name
                   << "  " << argument.help << "\n";
        stream << "Options:\n";
        for (const auto &toggle : m_toggles)
            if (!toggle.help.empty())
                stream << "  " << std::setw(30) << std::right << toggle.long_name
                       << "  " << toggle.help << "\n";
        for (const auto &option : m_options)
        {
            if (!option.help.empty())
            {
                stream << "  " << std::setw(30) << std::right << option.long_name
                       << "  " << option.help;
                if (!option.required && !option.default_value.empty())
                    stream << " [default: " << option.default_value << "]";
                stream << "\n";
            }
        }
        stream << "\n";
        return stream.str();
    }

private:
    [[noreturn]] void die_with_usage(int exit_code) const
    {
        std::cerr << usage();
        std::exit(exit_code);
    }
    void validate_short_name(const std::string &short_name)
    {
        if (short_name.empty())
            return;
        if (short_name.size() < 2 || short_name[0] != '-' || short_name[1] == '-' || short_name.find('=') != std::string::npos)
            throw std::logic_error("Bad short name for  "s + short_name);
    }
    void validate_long_name(const std::string &long_name)
    {
        if (long_name.empty())
            return;
        if (long_name.size() < 3 || long_name[0] != '-' || long_name[1] != '-' || long_name [2] == '-' || long_name.find('=') != std::string::npos)
            throw std::logic_error("Bad long name for option: "s + long_name);
    }
    void validate_argument_name(const std::string &arg_name)
    {
        if (arg_name[0] == '-')
            throw std::logic_error("Bad argument name: "s + arg_name);
    }
    std::string default_variable_name(std::string base_name)
    {
        // Strip -- / -
        auto off = base_name.find_first_not_of('-');
        base_name = base_name.substr(off);
        // Replace - with _
        std::replace(base_name.begin(), base_name.end(), '-', '_');
        // Uppercase all letters
        std::transform(base_name.begin(), base_name.end(), base_name.begin(), ::toupper);
        return base_name;
    }

    std::string m_program;
    std::vector<Option> m_options;
    std::vector<Toggle> m_toggles;
    std::vector<Argument> m_arguments;
    std::map<std::string, Option> m_option_lookup;
    std::map<std::string, Toggle> m_toggle_lookup;
    std::map<std::string, Argument> m_argument_lookup;
    std::set<std::string> m_required_options;
    unsigned m_trailing_optional_arguments = 0;
};

void make_debuggable()
{
    if (prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY, 0, 0, 0))
        throw std::runtime_error("Failed to allow ptracing");
}

#endif // FRONTEND_H

/*
 * Copyright 2022 Tobias Holl, Katharina Bogad
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef CPA_H
#define CPA_H

#include "parallel.h"
#include "leakage.h"

#include <boost/asynchronous/algorithm/all_of.hpp>
#include <boost/asynchronous/algorithm/parallel_for_each.hpp>
#include <boost/asynchronous/algorithm/parallel_reduce.hpp>
#include <boost/asynchronous/algorithm/then.hpp>

#if !defined(CPA_USE_FLOAT)
using number_t = double;
#else
using number_t = float;
#endif

namespace cpa
{

constexpr static inline std::size_t block_size = 16;
constexpr static inline std::size_t key_bytes = 16;

struct [[gnu::packed]] aes_value
{
    std::array<unsigned char, block_size> plaintext;
    std::array<unsigned char, block_size> ciphertext;
};
static_assert(sizeof(aes_value) == block_size * 2, "aes_value not properly aligned (size is not double the block size)");
static_assert(offsetof(aes_value, plaintext) == 0, "aes_value not properly aligned (offset of plaintext is not 0)");
static_assert(offsetof(aes_value, ciphertext) == block_size, "aes_value not properly aligned (offset of ciphertext is not the block size)");

constexpr static std::uint8_t AES_SBOX[] = {
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
};

constexpr static std::uint8_t AES_INVERSE_SBOX[] = {
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d,
};

using selection_function = std::function<std::uint8_t(std::size_t key_byte, const aes_value &value, std::size_t guess)>;

selection_function hamming_weight_first_round()
{
    return [](std::size_t key_byte, const aes_value &value, std::uint8_t guess)
    {
        return leakage::STATIC_HAMMING_WEIGHT_TABLE[AES_SBOX[value.plaintext[key_byte] ^ guess]];
    };
}

selection_function hamming_weight_last_round()
{
    return [](std::size_t key_byte, const aes_value &value, std::uint8_t guess)
    {
        return leakage::STATIC_HAMMING_WEIGHT_TABLE[AES_INVERSE_SBOX[value.ciphertext[key_byte] ^ guess]];
    };
}

// We want to precompute the matrices H'_j (256 x #values) and the vector hN_j (256)
//   H'_j[guess][value] = normalized(selection_function(j, value, guess)).
//   hN_j[guess] = euclidean_norm(H'_j[guess][...])

struct cpa_state
{
    cpa_state(std::size_t values_count, std::size_t key_byte_count)
        : H_prime(key_byte_count)
        , hN(key_byte_count)
    {
        for (auto &H_prime_j : H_prime)
            for (std::size_t guess = 0; guess < H_prime_j.size(); ++guess)
                H_prime_j[guess] = std::vector<number_t>(values_count);
    }

    std::vector<std::array<std::vector<number_t>, 256>> H_prime;
    std::vector<std::array<number_t, 256>> hN;
};

} // namespace cpa

namespace parallel
{

auto parallel_init_cpa(cpa::selection_function fn, std::shared_ptr<std::vector<cpa::aes_value>> values, std::size_t key_byte_begin, std::size_t key_byte_end)
{
    auto values_count = values->size();
    auto state = std::make_shared<cpa::cpa_state>(values_count, key_byte_end - key_byte_begin);
    // First, apply the selection function to almost compute H'. Remember:
    //   H'_j[guess][value] = normalized(selection_function(j, value, guess)).
    //   hN_j[guess] = euclidean_norm(H'_j[guess][...])
    return boost::asynchronous::then(
        boost::asynchronous::then(
            boost::asynchronous::parallel_for(
                static_cast<std::size_t>(0), static_cast<std::size_t>(values_count),
                [fn = std::move(fn), values, key_byte_begin, key_byte_end, state](const std::size_t &index)
                {
                    for (std::size_t key_byte = key_byte_begin; key_byte < key_byte_end; ++key_byte)
                        for (unsigned guess = 0; guess < 256; ++guess)
                            state->H_prime[key_byte - key_byte_begin][guess][index] = fn(key_byte, values->operator[](index), guess);
                },
                20u
            ),
            [values_count, state, key_byte_begin, key_byte_end](boost::asynchronous::expected<void> res) mutable
            {
                (void) unwrap(res, "parallel_init_cpa: (1) parallel_transform");
                // Remove the means from H' to normalize the values.
                std::vector<boost::asynchronous::detail::callback_continuation<void>> continuations;
                for (std::size_t key_byte_index = 0; key_byte_index < key_byte_end - key_byte_begin; ++key_byte_index)
                {
                    // Do this in parallel for all key bytes and guesses
                    for (unsigned guess = 0; guess < 256; ++guess)
                    {
                        auto cont = boost::asynchronous::then(
                            boost::asynchronous::parallel_reduce(
                                state->H_prime[key_byte_index][guess].begin(),
                                state->H_prime[key_byte_index][guess].end(),
                                [](const number_t &a, const number_t &b) { return a + b; },
                                100u
                            ),
                            [key_byte_index, guess, values_count, state](boost::asynchronous::expected<number_t> res) mutable
                            {
                                auto mean = unwrap(res, "parallel_init_cpa: (2) parallel_reduce") / values_count;
                                return boost::asynchronous::parallel_for(
                                    state->H_prime[key_byte_index][guess].begin(),
                                    state->H_prime[key_byte_index][guess].end(),
                                    [mean](number_t &a) { a -= mean; },
                                    100u
                                );
                            }
                        );
                        continuations.push_back(cont);
                    }
                }
                return boost::asynchronous::all_of(std::move(continuations));
            }
        ),
        [values_count, state, key_byte_begin, key_byte_end](auto res) mutable
        {
            (void) unwrap(res, "parallel_init_cpa: (2) all_of");
            // Now, compute the norms across all values. First, we need to square each element, then sum them up
            std::vector<boost::asynchronous::detail::callback_continuation<number_t>> continuations;
            std::vector<std::shared_ptr<std::vector<number_t>>> keepalive;
            for (std::size_t key_byte_index = 0; key_byte_index < key_byte_end - key_byte_begin; ++key_byte_index)
            {
                for (unsigned guess = 0; guess < 256; ++guess)
                {
                    auto squared = std::make_shared<std::vector<number_t>>(values_count);
                    auto cont = boost::asynchronous::then(
                        boost::asynchronous::parallel_transform(
                            state->H_prime[key_byte_index][guess].begin(),
                            state->H_prime[key_byte_index][guess].end(),
                            squared->begin(),
                            [](const number_t &a) { return a * a; },
                            100u
                        ),
                        [squared](auto res) mutable
                        {
                            (void) unwrap(res, "parallel_init_cpa: (3) parallel_transform");
                            return boost::asynchronous::parallel_reduce(
                                squared->begin(),
                                squared->end(),
                                [](const number_t &a, const number_t &b) { return a + b; },
                                100u
                            );
                        }
                    );
                    keepalive.push_back(squared);
                    continuations.push_back(cont);
                }
            }
            return boost::asynchronous::then(
                boost::asynchronous::all_of(std::move(continuations)),
                [state, key_byte_begin, key_byte_end, _ = std::move(keepalive)](auto res) mutable
                {
                    auto results = unwrap(res, "parallel_init_cpa: (3) reduce/all_of");
                    if (results.size() != (key_byte_end - key_byte_begin) * 256)
                        fatal("parallel_init_cpa: (3) inconsistent number of values");
                    for (std::size_t key_byte_index = 0; key_byte_index < key_byte_end - key_byte_begin; ++key_byte_index)
                        for (unsigned guess = 0; guess < 256; ++guess)
                            state->hN[key_byte_index][guess] = std::sqrt(results[key_byte_index * 256 + guess]);
                    return state;
                }
            );
        }
    );
}


//! Runs the CPA
class cpa_servant : public boost::asynchronous::trackable_servant<>
{
public:
    struct best_correlation_result
    {
        std::array<number_t, 256> best_correlation = {};
        std::array<std::size_t, 256> best_sample_index = {};
        std::size_t best_guess = 0;

        void merge(const best_correlation_result &other)
        {
            if (other.best_correlation[other.best_guess] > best_correlation[best_guess])
                best_guess = other.best_guess;

            for (std::size_t i = 0; i < 256; ++i)
            {
                if (other.best_correlation[i] > best_correlation[i])
                {
                    best_correlation[i] = other.best_correlation[i];
                    best_sample_index[i] = other.best_sample_index[i];
                }
            }
        }
    };

    struct best_correlation
    {
        void operator()(std::size_t custom_index)
        {
            std::size_t guess = custom_index % 256;
            std::size_t sample = custom_index / 256;

            const auto &H_row = state->H_prime[key_byte_index][guess];
            const auto &T_row = (*T_prime)[sample];
            if (H_row.size() != T_row.size())
                fatal("Size mismatch in matrix multiplication");

            number_t correlation = 0.0;
            for (std::size_t i = 0; i < H_row.size(); ++i)
                correlation += H_row[i] * T_row[i];
            correlation /= state->hN[key_byte_index][guess] * (*tN)[sample];
            correlation = std::fabs(correlation);
            if (std::isnan(correlation))
                correlation = 0.0;

            number_t prev_best = result.best_correlation[result.best_guess];
            if (correlation > result.best_correlation[guess])
            {
                result.best_correlation[guess] = correlation;
                result.best_sample_index[guess] = sample;
            }
            if (correlation > prev_best)
                result.best_guess = guess;
        }

        void merge(const best_correlation &other)
        {
            result.merge(other.result);
        }

        std::size_t key_byte_index;
        const std::vector<std::vector<number_t>> *T_prime;
        const std::vector<number_t> *tN;
        const cpa::cpa_state *state;

        best_correlation_result result = {};
    };

    using consumer_function = std::function<void(std::size_t block_index, std::vector<best_correlation_result> results, std::vector<best_correlation_result> this_block)>;
    using requires_weak_scheduler = std::true_type;

    cpa_servant(boost::asynchronous::any_weak_scheduler<> scheduler,
                boost::asynchronous::any_shared_scheduler_proxy<> pool,
                consumer_function consumer,
                std::shared_ptr<monitor> monitor,
                std::size_t entry_size,
                std::size_t block_size,
                cpa::selection_function selection_function,
                std::vector<cpa::aes_value> values,
                std::pair<std::size_t, std::size_t> key_byte_range)
        : boost::asynchronous::trackable_servant<>(scheduler, pool)
        , m_monitor(std::move(monitor))
        , m_entry_size(entry_size)
        , m_block_size(block_size)
        , m_values(std::make_shared<std::vector<cpa::aes_value>>(std::move(values)))
        , m_selection_function(std::move(selection_function))
        , m_state(nullptr)
        , m_records(key_byte_range.second - key_byte_range.first)
        , m_key_byte_range(key_byte_range)
        , m_consumer(std::move(consumer))
        , m_next_block(0)
    {}

    void update(std::size_t block_index, std::vector<io::row_segment> segments)
    {
        auto input = std::make_shared<std::vector<io::row_segment>>(std::move(segments));

        // We have rows containing leakage information for a single trace point.
        // On the first run, apply the selection function to the values, otherwise just continue
        if (!!m_state)
            return remove_means(block_index, std::move(input));

        // If this is not the first run but we don't have state yet, wait.
        if (block_index != 0)
            return delay_update(block_index, std::move(input));

        auto values = m_values;
        this->post_callback(
            [values, fn = m_selection_function, key_byte_range = m_key_byte_range]() mutable
            {
                return parallel_init_cpa(std::move(fn), std::move(values), key_byte_range.first, key_byte_range.second);
            },
            [this, block_index, input, values /* keepalive */](auto res) mutable
            {
                this->m_state = unwrap(res, "cpa_servant::update: callback");
                if (!this->m_state)
                    fatal("cpa_servant::update: Got empty state");
                this->remove_means(block_index, std::move(input));
            }
        );
    }

protected:
    void delay_update(std::size_t block_index, std::shared_ptr<std::vector<io::row_segment>> input)
    {
        if (!!m_state)
            return remove_means(block_index, std::move(input));

        // Wait for the state to be ready.
        m_pending_updates.push_back({ block_index, std::move(input) });
        m_update_timer = make_timer(RETRY_TIMEOUT);
        this->async_wait(
            *m_update_timer,
            [this](auto err) mutable
            {
                if (!!err) return;
                // Replay
                decltype(m_pending_updates) to_process;
                std::swap(to_process, m_pending_updates);
                for (auto [block_index, input] : to_process)
                    this->delay_update(block_index, std::move(input));
            }
        );
    }

    void remove_means(std::size_t block_index, std::shared_ptr<std::vector<io::row_segment>> input)
    {
        // This normalizes the trace data:
        //   T'[sample][value] = normalize(T[sample][value])
        //   tN[sample] = euclidean_norm(T'[sample][...])
        auto empty_vector = std::vector<number_t>(input->operator[](0).entries);
        auto T_prime = std::make_shared<std::vector<std::vector<number_t>>>(input->size(), std::move(empty_vector));
        auto tN = std::make_shared<std::vector<number_t>>(input->size());
        auto sums = std::make_shared<std::vector<number_t>>(input->size());
        auto value_count = m_values->size();

        this->post_callback(
            [input, sums, T_prime, tN, value_count]() mutable
            {
                return boost::asynchronous::then(
                    boost::asynchronous::then(
                        boost::asynchronous::parallel_for(
                            static_cast<std::size_t>(0), static_cast<std::size_t>(input->size()),
                            [input, sums, T_prime, value_count](std::size_t sample) mutable
                            {
                                for (std::size_t value_index = 0; value_index < value_count; ++value_index)
                                {
                                    std::uint8_t entry = input->operator[](sample).data.get()[value_index];
                                    sums->operator[](sample) += entry;
                                    T_prime->operator[](sample)[value_index] = entry;
                                }
                            },
                            100u
                        ),
                        [sums, T_prime, value_count](auto res) mutable
                        {
                            (void) unwrap(res, "cpa_servant::remove_means: parallel_for #1");
                            return boost::asynchronous::parallel_for(
                                static_cast<std::size_t>(0), static_cast<std::size_t>(value_count * T_prime->size()),
                                [sums, T_prime, value_count](std::size_t index) mutable
                                {
                                    auto which_input = index / value_count;
                                    auto which_value = index % value_count;
                                    T_prime->operator[](which_input)[which_value] -= sums->operator[](which_input) / value_count; // Average across all traces
                                },
                                100u
                            );
                        }
                    ),
                    [T_prime, tN, value_count](auto res) mutable
                    {
                        (void) unwrap(res, "cpa_servant::remove_means: parallel_for #2");
                        return boost::asynchronous::parallel_for(
                            static_cast<std::size_t>(0), static_cast<std::size_t>(T_prime->size()),
                            [T_prime, tN, value_count](std::size_t sample) mutable
                            {
                                number_t norm = 0.0f;
                                for (std::size_t value_index = 0; value_index < value_count; ++value_index)
                                {
                                    auto t = T_prime->operator[](sample)[value_index];
                                    norm += t * t;
                                }
                                tN->operator[](sample) = std::sqrt(norm);
                            },
                            100u
                        );
                    }
                );
            },
            [this, block_index, T_prime, tN, /* keepalive */ input, sums](auto res) mutable
            {
                (void) unwrap(res, "cpa_servant::remove_means: callback");
                this->compute_correlation(block_index, std::move(T_prime), std::move(tN));
            }
        );
    }

    void compute_correlation(std::size_t block_index, std::shared_ptr<std::vector<std::vector<number_t>>> T_prime, std::shared_ptr<std::vector<number_t>> tN)
    {
        // Compute CPA for these:
        // For each key byte, we must compute the correlation
        //   C = |(H' @ T'^t) / (hN x tN)|, where @ is matrix multiplication, ^t is transposition, and x is the outer product
        // Then, we want the index of the row containing the maximum value.
        // We can win here by optimizing and only keeping the maximum around.
        // H: 256 x #values, T: #samples x #values => C: 256 x #samples
        auto state = m_state;
        this->post_callback(
            [state, T_prime, tN, key_byte_range = m_key_byte_range]() mutable
            {
                std::vector<boost::asynchronous::detail::callback_continuation<best_correlation>> continuations;
                for (std::size_t key_byte_index = 0; key_byte_index < key_byte_range.second - key_byte_range.first; ++key_byte_index)
                {
                    auto cont = boost::asynchronous::parallel_for_each(
                        static_cast<std::size_t>(0), static_cast<std::size_t>(T_prime->size() * 256),
                        best_correlation { .key_byte_index = key_byte_index, .T_prime = T_prime.get(), .tN = tN.get(), .state = state.get() },
                        100u
                    );
                    continuations.push_back(std::move(cont));
                }
                return boost::asynchronous::all_of(std::move(continuations));
            },
            [this, block_index, /* keepalive */ state, T_prime, tN](auto res)
            {
                auto best = unwrap(res, "cpa_servant::compute_correlation: callback");
                std::vector<best_correlation_result> best_results;
                for (const auto &entry : best)
                {
                    auto best_result = entry.result;
                    for (std::size_t guess = 0; guess < 256; ++guess)
                        best_result.best_sample_index[guess] += block_index *  m_block_size;
                    best_results.push_back(std::move(best_result));
                }
                this->update_results(block_index, std::move(best_results));
            }
        );
    }

    void update_results(std::size_t block_index, std::vector<best_correlation_result> best)
    {
        // Ensure ordering of the updates that go to the logger
        if (block_index == m_next_block)
        {
            ++m_next_block;
            m_monitor->release();
            for (std::size_t key_byte_index = 0; key_byte_index < m_key_byte_range.second - m_key_byte_range.first; ++key_byte_index)
                m_records[key_byte_index].merge(best[key_byte_index]);
            m_consumer(block_index, m_records, std::move(best));
        }
        else
        {
            m_pending_results.push_back({ block_index, std::move(best) });
            m_result_timer = make_timer(RETRY_TIMEOUT);
            this->async_wait(
                *m_result_timer,
                [this](auto err) mutable
                {
                    if (!!err) return;
                    // Replay
                    decltype(m_pending_results) to_process;
                    std::swap(to_process, m_pending_results);
                    for (auto [block_index, best] : to_process)
                        this->update_results(block_index, std::move(best));
                }
            );
        }
    }

private:
    std::shared_ptr<monitor> m_monitor;
    std::size_t m_entry_size;
    std::size_t m_block_size;
    std::shared_ptr<std::vector<cpa::aes_value>> m_values;
    cpa::selection_function m_selection_function;
    std::shared_ptr<cpa::cpa_state> m_state;
    std::vector<best_correlation_result> m_records;
    std::pair<std::size_t, std::size_t> m_key_byte_range;
    consumer_function m_consumer;
    std::size_t m_next_block;
    std::vector<std::pair<std::size_t, std::shared_ptr<std::vector<io::row_segment>>>> m_pending_updates;
    std::vector<std::pair<std::size_t, std::vector<best_correlation_result>>> m_pending_results;
    timer_t m_update_timer;
    timer_t m_result_timer;
};


//! Threadsafe proxy for a cpa_servant
class cpa_proxy : public boost::asynchronous::servant_proxy<cpa_proxy, cpa_servant>
{
public:
    template <typename Scheduler>
    cpa_proxy(Scheduler scheduler,
              boost::asynchronous::any_shared_scheduler_proxy<> pool,
              cpa_servant::consumer_function consumer,
              std::shared_ptr<monitor> monitor,
              std::size_t entry_size,
              std::size_t block_size,
              cpa::selection_function selection_function,
              std::vector<cpa::aes_value> values,
              std::pair<std::size_t, std::size_t> key_byte_range)
        : boost::asynchronous::servant_proxy<cpa_proxy, cpa_servant>(scheduler, pool, std::move(consumer), std::move(monitor), entry_size, block_size, std::move(selection_function), std::move(values), std::move(key_byte_range))
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("cpa_servant::ctor", 0);
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("cpa_servant::dtor", 0);
    BOOST_ASYNC_POST_MEMBER_LOG(update, "cpa_servant::update", 0);
};

} // namespace parallel

#endif // CPA_H

// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"
#include "leakage.h"
#include "parallel.h"

#include <y5.h>
#include <fmt/format.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <future>
#include <fstream>

// Guard the whiteboxgrind types so we don't pollute anything
namespace protocol
{
#include "../valgrind/whiteboxgrind/whiteboxgrind_types.h" // TODO: Set up proper path for this
} // namespace protocol

namespace leakage
{
    struct leakage_model
    {
        std::size_t input_entry_size;
        std::size_t output_entry_size;
        std::function<void(unsigned char *dst, const unsigned char *src)> apply;
    };

    static std::map<std::string, leakage_model> models = {
        { "hamming_weight", {
            sizeof(protocol::MemoryTraceEntry),
            sizeof(std::uint8_t),
            [](unsigned char *dst, const unsigned char *src)
            {
                auto *input = reinterpret_cast<const protocol::MemoryTraceEntry *>(src);
                std::uint8_t hw = 0; // TODO: This is limited to 255 bit => 32 bytes - 1 bit, not 34 bytes - but I think we only have 34 bytes in CAS entries, which are really rare anyways
                for (std::size_t index = 0; index < sizeof(input->value); ++index)
                    hw += STATIC_HAMMING_WEIGHT_TABLE[input->value[index]];
                *dst = hw;
            }
        }}
    };
} // namespace leakage

[[noreturn]] void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

int main(int argc, const char *argv[])
{
    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "input", .help = "Source traces in Y5 format"});
    parser.add(ArgumentParser::Argument { .name = "output", .help = "Output file (in Y5 format)" });
    parser.add(ArgumentParser::Option { .short_name = "-j", .long_name = "--threads", .help = "Number of threads", .default_value = std::to_string(std::thread::hardware_concurrency()) });
    parser.add(ArgumentParser::Option { .long_name = "--model", .help = "Leakage model", .default_value = "hamming_weight" });
    parser.add(ArgumentParser::Toggle { .long_name = "--overwrite", .help = "Overwrite the output file" });
    parser.add(ArgumentParser::Option { .long_name = "--block-size", .help = "Block size for performance tuning", .default_value = "8192" });
    parser.add(ArgumentParser::Option { .long_name = "--max-row-memory", .help = "Maximum buffer size per row (in KiB)", .default_value = "8" });
    parser.add(ArgumentParser::Option { .long_name = "--max-live-blocks", .help = "Maximum number of live blocks in processing", .default_value = "256" });
    auto args = parser.parse(argc, argv);

    std::string input_path = args.at("input");
    std::string output_path = args.at("output");
    std::string model_name = args.at("--model");
    bool overwrite = !args.at("--overwrite").empty();
    unsigned long threads = std::clamp(std::stoul(args.at("--threads")), 4ul, 1024ul);
    unsigned long block_size = std::clamp(std::stoul(args.at("--block-size")), 1ul, ULONG_MAX);
    unsigned long max_row_memory_kb = std::clamp(std::stoul(args.at("--max-row-memory")), 1ul, ULONG_MAX);
    unsigned long max_live_blocks = std::clamp(std::stoul(args.at("--max-live-blocks")), 1ul, ULONG_MAX);

    if (!leakage::models.count(model_name)) die("Unknown leakage model: "s + model_name);
    auto model = leakage::models.at(model_name);

    // Y5 IO
    y5::performance::tuneables input_tuneables = {
        .maximum_overallocation = max_row_memory_kb * 1024ul,
        .maximum_held_buffer = max_row_memory_kb * 1024ul
    };

    auto input = std::make_shared<y5::reader>(input_path, input_tuneables);
    if (!(input->header().flags & y5::format::header::transposed))
        die("Input data is not filtered");
    if (input->header().flags & y5::format::header::ragged)
        die("Input data is ragged (support for differing line lengths not yet implemented!)");
    if (input->header().entry_size != model.input_entry_size)
        die("Invalid trace for this leakage model (incorrect entry size: "s + std::to_string(input->header().entry_size) + ", expected " + std::to_string(model.input_entry_size) + ")");
    auto total = input->header().rows;

    auto output = std::make_shared<y5::writer>(
        output_path,
        overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE,
        model.output_entry_size,
        input->header().flags
    );

    // Synchronization foo
    auto promise = std::make_shared<std::promise<void>>();
    auto future = promise->get_future();
    auto monitor = std::make_shared<parallel::monitor>([promise = std::move(promise)]() mutable { promise->set_value(); }, max_live_blocks);

    // Set up the parallel pipeline
    auto unused_pool = parallel::make_single_thread_scheduler(); // Shared by all servants that don't actually use it
    parallel::progress_log_proxy logger(parallel::make_single_thread_scheduler(), unused_pool, monitor, block_size, total);

    parallel::io::row_writer_proxy writer(parallel::make_single_thread_scheduler(), unused_pool, std::move(output), monitor);
    auto do_write = [writer, logger, block_size, total](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_transform(block_index);
        writer.write_rows(block_index, std::move(rows));
        logger.progress_writer(block_index); // TODO: Should be moved to writer callback
    };

    // No transposition (we have lots of rows, so reading them all at the same time means trouble), so use a transformer_servant instead of a transposing_filter_servant.
    parallel::transformer_proxy transformer(
        parallel::make_single_thread_scheduler(),
        parallel::make_threadpool(threads),
        std::move(do_write),
        monitor,
        model.input_entry_size,
        model.output_entry_size,
        model.apply
    );
    auto do_transform = [transformer, logger](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_reader(block_index);
        transformer.transform(block_index, std::move(rows));
    };

    parallel::io::row_reader_proxy reader(parallel::make_single_thread_scheduler(), parallel::make_threadpool(threads), std::move(input), std::move(do_transform), monitor, block_size);

    // Run the pipeline
    reader.generate();

    // Wait for everything to be done
    future.get();
    return EXIT_SUCCESS;
}

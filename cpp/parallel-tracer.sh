#!/bin/bash
set -eu -o pipefail

# This is an ugly hack based on y5-tooling/concat.cpp that generates traces in parallel
# This will pass an extra -i <input> option to the tracer, and insert the output directory
# at the second-to-last position (!)
if [ "$#" -lt 4 ] || [ "$1" = "-h" ]; then
    echo "usage: $0 <outdir> <concat-binary> <threads> <inputs> [tracer_command ... <source> <valgrind>]"
    exit 1
fi

FINAL_OUT="$1"
CONCAT_BINARY="$2"
shift 2

THREADS="$(("$1" + 0))"
INPUTS="$2"
shift 2

PRE_ARGS=()
while [ "$#" -gt 1 ]; do
    PRE_ARGS+=("$1")
    shift
done
VALGRIND="$1"

INPUT_BYTES="$(wc -c "$INPUTS" | cut -f1 -d' ')"
STEP_SIZE="$(((("$INPUT_BYTES" / 16) + "$THREADS" - 1) / "$THREADS" * 16))"

OUT_DIRS=()
function finish {
    rm -rf -- "${OUT_DIRS[@]}"
}

trap finish EXIT
for THREAD in $(seq 0 "$(("$THREADS" - 1))"); do
    DIR="$(mktemp -d)"
    OUT_DIRS+=("$DIR")
    dd if="$INPUTS" of="${DIR}/inputs" count=1 bs="$STEP_SIZE" skip="$THREAD" >/dev/null 2>/dev/null
    if [ "$THREAD" -eq 0 ]; then
        echo "Running tracer:" "${PRE_ARGS[@]}" -i "<input>" "<outdir>" "$VALGRIND"
    fi
    "${PRE_ARGS[@]}" -i "${DIR}/inputs" "${DIR}" "$VALGRIND" >"${DIR}/log" 2>&1 &
done
wait

# Concatenate all the files
NAMES=()
for DIR in "${OUT_DIRS[@]}"; do
    while IFS= read -r -d $'\0' FILE; do
        FILE="$(basename $FILE)"
        IS_KNOWN=0
        for KNOWN in "${NAMES[@]}"; do
            if [ "$KNOWN" = "$FILE" ]; then
                IS_KNOWN=1
            fi
        done
        if [ "$IS_KNOWN" -eq 0 ]; then
            NAMES+=("$FILE")
        fi
    done < <(find "${DIR}" -name '*.y5' -type f -print0)
done
for NAME in "${NAMES[@]}"; do
    TFD=("${OUT_DIRS[@]/%//$NAME}")
    echo "Concatenating ${FINAL_OUT}/${NAME}"
    "$CONCAT_BINARY" "${FINAL_OUT}/${NAME}" "${TFD[@]}"
done
# wait

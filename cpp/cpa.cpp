// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"
#include "parallel.h"
#include "cpa.h"

#include <y5.h>

#include <openssl/aes.h>

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <future>
#include <fstream>

[[noreturn]] static void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

std::vector<cpa::aes_value> read_values(const std::filesystem::path &path)
{
    // Read all values into memory
    // We don't usually have that many, and they are really small.
    y5::performance::tuneables value_tuneables = {
        .maximum_overallocation = std::max(0x1000ul, cpa::block_size * 4),
        .maximum_held_buffer = std::max(0x1000ul, cpa::block_size * 4),
        .initial_buffer_size = std::max(0x1000ul, cpa::block_size * 4)
    };

    y5::reader value_reader(path, value_tuneables);

    if (value_reader.header().entry_size != sizeof(cpa::aes_value))
        die("Unknown data stored in values file: bad entry size");
    if (value_reader.header().flags & (y5::format::header::ragged | y5::format::header::transposed))
        die("Invalid flags (ragged or transposed data) in values file header");
    if (value_reader.header().max_columns != 1)
        die("Unknown data stored in values file: more than one entry per row");

    std::vector<cpa::aes_value> values(value_reader.header().rows);
    for (std::size_t row = 0; row < value_reader.header().rows; ++row)
    {
        auto [read, done] = value_reader.read(&values[row], 1);
        if (!done || read != 1)
            die(std::string { "Failed to read value for row " } + std::to_string(row));
    }

    return values;
}

enum class key_byte_status
{
    unknown,
    in_progress,
    known
};

int main(int argc, const char *argv[])
{
    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "input", .help = "Leakage values in Y5 format"});
    parser.add(ArgumentParser::Argument { .name = "values", .help = "Values (in Y5 format)" });
    parser.add(ArgumentParser::Option { .short_name = "-j", .long_name = "--threads", .help = "Number of threads", .default_value = std::to_string(std::thread::hardware_concurrency()) });
    parser.add(ArgumentParser::Option { .long_name = "--block-size", .help = "Block size for performance tuning", .default_value = "1024" });
    parser.add(ArgumentParser::Option { .long_name = "--max-live-blocks", .help = "Maximum number of live blocks in processing", .default_value = "64" });
    parser.add(ArgumentParser::Option { .long_name = "--chunk-key-bytes", .help = "Process key bytes in chunks of this size", .default_value = std::to_string(cpa::key_bytes) });
    parser.add(ArgumentParser::Option { .long_name = "--selection-function", .help = "Selection function to use (hamming_weight_first, hamming_weight_last)", .default_value = "hamming_weight_first" });
    parser.add(ArgumentParser::Toggle { .long_name = "--validate", .help = "Validate the recovered key against the provided values" });
    parser.add(ArgumentParser::Option { .short_name = "-g", .long_name = "--graph", .help = "Dump the best correlation per key byte and block to a Y5 file in this directory (<key byte>.y5)" });
    parser.add(ArgumentParser::Toggle { .long_name = "--overwrite-graph", .help = "Overwrite the graph data if it already exists" });
    parser.add(ArgumentParser::Option { .short_name = "-o", .long_name = "--output", .help = "CSV filename to dump correlation results to", .default_value = "" });

    auto args = parser.parse(argc, argv);

    std::string input_path = args.at("input");
    std::string values_path = args.at("values");
    unsigned long threads = std::clamp(std::stoul(args.at("--threads")), 4ul, 1024ul);
    unsigned long block_size = std::clamp(std::stoul(args.at("--block-size")), 1ul, ULONG_MAX);
    unsigned long max_live_blocks = std::clamp(std::stoul(args.at("--max-live-blocks")), 1ul, ULONG_MAX);
    unsigned long chunk_key_bytes = std::clamp(std::stoul(args.at("--chunk-key-bytes")), 1ul, 16ul);

    std::string selection_function_name = args.at("--selection-function");
    cpa::selection_function selection_function;
    if (selection_function_name == "hamming_weight_first")
        selection_function = cpa::hamming_weight_first_round();
    else if (selection_function_name == "hamming_weight_last")
        selection_function = cpa::hamming_weight_last_round();
    else
        die(std::string { "Unknown selection function: '" } + selection_function_name + "'");

    bool validate = !args.at("--validate").empty();
    std::string output_path = args.at("--output");

    bool graph = !args.at("--graph").empty();
    bool overwrite_graph = !args.at("--overwrite-graph").empty();
    if (graph)
        std::filesystem::create_directories(std::filesystem::path { args.at("--graph") });

    // Read values from Y5 file first
    auto values = read_values(values_path);

    // Clean up and start again for every key byte chunk
    std::array<parallel::cpa_servant::best_correlation_result, cpa::key_bytes> best;
    std::array<key_byte_status, cpa::key_bytes> status;
    std::fill(status.begin(), status.end(), key_byte_status::unknown);

    for (std::size_t key_byte_start = 0; key_byte_start < cpa::key_bytes; key_byte_start += chunk_key_bytes)
    {
        auto key_byte_end = std::min(cpa::key_bytes, key_byte_start + chunk_key_bytes);

        // Set up the trace reader
        y5::performance::tuneables tuneables = {
            .maximum_overallocation = 8 * 1024ul,
            .maximum_held_buffer = 8 * 1024ul,
            .initial_buffer_size = 8 * 1024ul,
        };

        auto input = std::make_shared<y5::reader>(input_path, tuneables);
        if (!(input->header().flags & y5::format::header::transposed))
            die("Input data is not yet filtered");
        if (input->header().flags & y5::format::header::ragged)
            die("Input data is ragged (support for differing line lengths not yet implemented!)");
        auto total = input->header().rows;
        auto entry_size = input->header().entry_size;
        if (entry_size != sizeof(std::uint8_t))
            die("Input data has unsupported leakage format (hamming weight leakage should have one byte per entry)");

        // Set up the graph writers if desired
        // NB: We cheat here a little bit by only keeping track of the maximum correlation per block rather than across all samples.
        // However, you won't be able to render the per-sample graph in a reasonable way anyways.
        std::vector<std::shared_ptr<y5::writer>> graph_writers;
        if (graph)
        {
            for (std::size_t key_byte = key_byte_start; key_byte < key_byte_end; ++key_byte)
            {
                graph_writers.push_back(std::make_shared<y5::writer>(
                    std::filesystem::path { args.at("--graph") } / (std::to_string(key_byte) + ".y5"),
                    overwrite_graph ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE,
                    sizeof(number_t),
                    y5::format::header::uncompressed, // Save on compression here - since we don't have them in separate servants, this needs to be _fast_ for now (and per-block this should be OK).
                    y5::format::DEFAULT_TOC_CAPACITY, // We only have 1 row, but sure, we can use the default
                    tuneables
                ));
            }
        }

        // Synchronization foo
        auto promise = std::make_shared<std::promise<void>>();
        auto future = promise->get_future();
        auto monitor = std::make_shared<parallel::monitor>([promise = std::move(promise)]() mutable { promise->set_value(); }, max_live_blocks);

        // Set up the parallel pipeline
        auto pool = parallel::make_threadpool(threads);

        std::fill(status.begin() + key_byte_start, status.begin() + key_byte_end, key_byte_status::in_progress);
        auto on_update = [key_byte_start, key_byte_end, block_size, total, graph, &graph_writers, &status, &best](std::size_t block_index, std::vector<parallel::cpa_servant::best_correlation_result> results, std::vector<parallel::cpa_servant::best_correlation_result> this_block) mutable
        {
            // NB: This is a cross-thread access, but it's fine because _NOONE_ else can access it at the same time
            // This _ONLY_ works because the cpa_servant runs in a single_thread_scheduler!
            std::size_t current_done = std::min(total, (block_index + 1) * block_size);
            std::string progress_color = "\x1b[33m";
            if (!results.empty())
            {
                // Update values
                if (results.size() != key_byte_end - key_byte_start)
                    die("Incorrect number of results!");
                for (std::size_t index = 0; index < results.size(); ++index)
                    best[key_byte_start + index] = results[index];

                // Write the graph data if desired
                for (std::size_t index = 0; graph && index < key_byte_end - key_byte_start; ++index)
                    graph_writers[index]->append(&this_block[index].best_correlation[this_block[index].best_guess], 1);
            }
            else
            {
                // After-completion print request
                current_done = total;
                progress_color = "\x1b[32m";

                if (graph)
                    for (auto &writer : graph_writers)
                        writer->end_row();
            }
            std::cout << fmt::format("{:>10} / {:>10} ({:5.1f}%)   ", current_done, total, 100.0 * current_done / total);
            for (std::size_t key_byte = 0; key_byte < cpa::key_bytes; ++key_byte)
            {
                switch (status[key_byte])
                {
                    case key_byte_status::unknown:
                        std::cout << " \x1b[31m??\x1b[0m";
                        break;
                    case key_byte_status::in_progress:
                        std::cout << fmt::format(" {}{:02x}\x1b[0m", progress_color, best[key_byte].best_guess);
                        break;
                    case key_byte_status::known:
                        std::cout << fmt::format(" {:02x}", best[key_byte].best_guess);
                        break;
                }
            }
            if (!results.empty())
            {
                std::cout << "\r" << std::flush;
            }
            else
            {
                // Round is over, print more information for each new key byte we got
                std::cout << std::endl;
                for (std::size_t key_byte = 0; key_byte < cpa::key_bytes; ++key_byte)
                {
                    if (status[key_byte] != key_byte_status::in_progress)
                        continue;
                    const auto &best_here = best[key_byte];
                    const auto best_guess = best_here.best_guess;
                    std::cout << fmt::format("  {:02x}: \x1b[32m{:02x}\x1b[0m  (sample {:>10}, correlation {:6.4f})", key_byte, best_guess, best_here.best_sample_index[best_guess], best_here.best_correlation[best_guess]) << std::endl;
                    status[key_byte] = key_byte_status::known;
                }
            }
        };

        parallel::cpa_proxy cpa(
            parallel::make_single_thread_scheduler(),
            pool,
            std::move(on_update),
            monitor,
            entry_size,
            block_size,
            selection_function,
            values, // Make a copy, don't move - we may need multiple iterations!
            { key_byte_start, key_byte_end }
        );

        parallel::io::row_reader_proxy reader(
            parallel::make_single_thread_scheduler(),
            pool,
            std::move(input),
            [cpa](auto &&...args) mutable { cpa.update(std::forward<decltype(args)>(args)...); },
            monitor,
            block_size
        );

        // Run the pipeline
        reader.generate();

        // Wait for everything to be done
        future.get();

        // Print a final update
        on_update(0, {}, {});
    }

    if (!output_path.empty())
    {
        std::ofstream csv_out(output_path);
        csv_out << "key_byte,guess,best_correlation,best_sample\n";
        for (std::size_t key_byte = 0; key_byte < cpa::key_bytes; ++key_byte)
        {
            const auto &best_here = best[key_byte];
            for (std::size_t guess = 0; guess < 256; ++guess)
                csv_out << key_byte << "," << guess << "," << best_here.best_correlation[guess] << "," << best_here.best_sample_index[guess] << "\n";
        }
    }

    if (validate)
    {
        // Validate the recovered key
        std::array<std::uint8_t, 16> key_bytes;
        std::transform(best.begin(), best.end(), key_bytes.begin(), [](const parallel::cpa_servant::best_correlation_result &res) { return res.best_guess; });

        AES_KEY actual;
        if (AES_set_encrypt_key(key_bytes.data(), 128, &actual) != 0)
            die("Failed to set up encryption key");

        std::cout << "\n\n            Plaintext            |            Ciphertext            | Status\n";
        for (const auto &value : values)
        {
            std::array<std::uint8_t, cpa::block_size> real_result;
            AES_encrypt(value.plaintext.data(), real_result.data(), &actual);

            for (const auto &byte : value.plaintext)
                std::cout << fmt::format("{:02x}", byte);
            std::cout << " | ";
            for (const auto &byte : value.ciphertext)
                std::cout << fmt::format("{:02x}", byte);
            std::cout << " | "
                      << (std::equal(real_result.begin(), real_result.end(), value.ciphertext.begin()) ? "\x1b[32;1m  OK  \x1b[0m" : "\x1b[31;1m FAIL \x1b[0m")
                      << std::endl;
        }
    }

    return EXIT_SUCCESS;
}

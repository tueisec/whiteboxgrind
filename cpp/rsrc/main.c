#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#if __has_include(<whiteboxgrind.h>)
#include <whiteboxgrind.h>
#else
#warning "Could not find whiteboxgrind.h, disabling tracing requests"
#define WHITEBOX_START do {} while (0)
#define WHITEBOX_END do {} while (0)
#endif

void AES_128_encrypt(unsigned char ciphertext[16], unsigned char plaintext[16]);


int main(void) {
    
    uint8_t in[16]  = { 0 };
    uint8_t enc[16];

    fread(in, sizeof(uint8_t), sizeof(in), stdin);

    WHITEBOX_START;
    AES_128_encrypt(enc, in);
    WHITEBOX_END;

    fwrite(enc, sizeof(uint8_t), sizeof(enc), stdout);
}


// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"
#include "parallel.h"

#include <y5.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <future>
#include <fstream>

[[noreturn]] static void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

int main(int argc, const char *argv[])
{
    make_debuggable();

    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "input", .help = "Filtered traces in Y5 format"});
    parser.add(ArgumentParser::Argument { .name = "output", .help = "Output file (in Y5 format)" });
    parser.add(ArgumentParser::Option { .short_name = "-j", .long_name = "--threads", .help = "Number of threads", .default_value = std::to_string(std::thread::hardware_concurrency()) });
    parser.add(ArgumentParser::Toggle { .long_name = "--overwrite", .help = "Overwrite the output file" });
    parser.add(ArgumentParser::Option { .long_name = "--block-size", .help = "Block size for performance tuning", .default_value = "1024" });
    parser.add(ArgumentParser::Option { .long_name = "--cache-size", .help = "Maximum number of entries in the cache (-1: cache everything and use all the memory)", .default_value = "-1" });
    parser.add(ArgumentParser::Option { .long_name = "--max-live-blocks", .help = "Maximum number of live blocks in processing", .default_value = "64" });
    parser.add(ArgumentParser::Option { .long_name = "--compare-begin", .help = "Offset at which to start computing hashes for deduplication", .default_value = "8" }); // NB: Default is at 8 because all of our data starts with 8 bytes of index for synchronization.
    parser.add(ArgumentParser::Option { .long_name = "--compare-end", .help = "Offset at which to stop computing hashes for deduplication (-1: at the end)", .default_value = "-1" });

    auto args = parser.parse(argc, argv);

    std::string input_path = args.at("input");
    std::string output_path = args.at("output");
    bool overwrite = !args.at("--overwrite").empty();
    unsigned long threads = std::clamp(std::stoul(args.at("--threads")), 4ul, 1024ul);
    unsigned long block_size = std::clamp(std::stoul(args.at("--block-size")), 1ul, ULONG_MAX);
    unsigned long max_live_blocks = std::clamp(std::stoul(args.at("--max-live-blocks")), 1ul, ULONG_MAX);

    long compare_begin = std::stol(args.at("--compare-begin"));
    long compare_end = std::stol(args.at("--compare-end"));

    long cache_size = std::stol(args.at("--cache-size"));
    if (cache_size < -1)
        die("Invalid negative cache size");
    std::size_t cache_capacity = cache_size < 0 ? std::numeric_limits<std::size_t>::max() : static_cast<std::size_t>(cache_size);

    // Y5 IO
    y5::performance::tuneables input_tuneables = {
        .maximum_overallocation = 8 * 1024ul,
        .maximum_held_buffer = 8 * 1024ul
    };

    auto input = std::make_shared<y5::reader>(input_path, input_tuneables);
    if (!(input->header().flags & y5::format::header::transposed))
        die("Input data is not yet filtered");
    if (input->header().flags & y5::format::header::ragged)
        die("Input data is ragged (support for differing line lengths not yet implemented!)");
    auto total = input->header().rows;
    auto entry_size = input->header().entry_size;

    if (compare_end == -1)
        compare_end = static_cast<long>(entry_size);
    if (compare_end < compare_begin || compare_begin < 0 || compare_end < 0 || compare_begin >= entry_size || compare_end > entry_size)
        die("Invalid boundaries for deduplication");

    auto output = std::make_shared<y5::writer>(
        output_path,
        overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE,
        input->header().entry_size,
        static_cast<y5::format::header::bitflags>(input->header().flags)
    );

    // Synchronization foo
    auto promise = std::make_shared<std::promise<void>>();
    auto future = promise->get_future();
    auto monitor = std::make_shared<parallel::monitor>([promise = std::move(promise)]() mutable { promise->set_value(); }, max_live_blocks);

    // Set up the parallel pipeline
    auto unused_pool = parallel::make_single_thread_scheduler(); // Shared by all servants that don't actually use it
    parallel::progress_log_proxy logger(parallel::make_single_thread_scheduler(), unused_pool, monitor, block_size, total);

    parallel::io::row_writer_proxy writer(parallel::make_single_thread_scheduler(), unused_pool, std::move(output), monitor);
    auto do_write = [writer, logger, block_size, total](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_transform(block_index);
        writer.write_rows(block_index, std::move(rows));
        logger.progress_writer(block_index); // TODO: Should be moved to writer callback
    };

    parallel::deduplicating_filter_proxy filter(
        parallel::make_single_thread_scheduler(),
        parallel::make_threadpool(threads),
        std::move(do_write),
        monitor,
        entry_size,
        cache_capacity,
        parallel::deduplicating_filter_servant::partial_djb2_hash(compare_begin, compare_end, entry_size),
        parallel::deduplicating_filter_servant::equality_comparison(compare_begin, compare_end, entry_size)
    );
    auto do_deduplicate = [filter, logger](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_reader(block_index);
        filter.deduplicate(block_index, std::move(rows));
    };

    parallel::io::row_reader_proxy reader(parallel::make_single_thread_scheduler(), parallel::make_threadpool(threads), std::move(input), std::move(do_deduplicate), monitor, block_size);

    // Run the pipeline
    reader.generate();

    // Wait for everything to be done
    future.get();
    if (filter.did_discard_cache_entries().get())
        std::cerr << "\n\x1b[33;1mWARNING\x1b[0m: Some cache entries were discarded. To guarantee uniqueness, increase --cache-size, or run unique again on the output with a randomized cache eviction policy.\n";
    return EXIT_SUCCESS;
}

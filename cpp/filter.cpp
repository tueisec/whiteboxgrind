// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"
#include "parallel.h"

#include <y5.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <future>
#include <fstream>

[[noreturn]] static void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

int main(int argc, const char *argv[])
{
    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "input", .help = "Source traces in Y5 format"});
    parser.add(ArgumentParser::Argument { .name = "output", .help = "Output file (in Y5 format)" });
    parser.add(ArgumentParser::Option { .short_name = "-j", .long_name = "--threads", .help = "Number of threads", .default_value = std::to_string(std::thread::hardware_concurrency()) });
    parser.add(ArgumentParser::Toggle { .long_name = "--overwrite", .help = "Overwrite the output file" });
    parser.add(ArgumentParser::Option { .long_name = "--block-size", .help = "Block size for performance tuning", .default_value = "1024" });
    parser.add(ArgumentParser::Option { .long_name = "--max-row-memory", .help = "Maximum buffer size per row (in KiB)", .default_value = "8192" });
    parser.add(ArgumentParser::Option { .long_name = "--max-live-blocks", .help = "Maximum number of live blocks in processing", .default_value = "64" });
    auto args = parser.parse(argc, argv);

    std::string input_path = args.at("input");
    std::string output_path = args.at("output");
    bool overwrite = !args.at("--overwrite").empty();
    unsigned long threads = std::clamp(std::stoul(args.at("--threads")), 4ul, 1024ul);
    unsigned long block_size = std::clamp(std::stoul(args.at("--block-size")), 1ul, ULONG_MAX);
    unsigned long max_row_memory_kb = std::clamp(std::stoul(args.at("--max-row-memory")), 1ul, ULONG_MAX);
    unsigned long max_live_blocks = std::clamp(std::stoul(args.at("--max-live-blocks")), 1ul, ULONG_MAX);

    // Y5 IO
    y5::performance::tuneables input_tuneables = {
        .maximum_overallocation = max_row_memory_kb * 1024ul,
        .maximum_held_buffer = max_row_memory_kb * 1024ul
    };

    auto input = std::make_shared<y5::reader>(input_path, input_tuneables);
    if (input->header().flags & y5::format::header::transposed)
        die("Input data is already filtered");
    if (input->header().flags & y5::format::header::ragged)
        die("Input data is ragged (support for differing line lengths not yet implemented!)");
    auto total = input->header().max_columns;
    auto entry_size = input->header().entry_size;

    // Since we read rows in parallel, adjust vm.max_map_count with sysctl if necessary if mmap fails
    std::ifstream sysctl("/proc/sys/vm/max_map_count");
    std::size_t max_map_count;
    if (!(sysctl >> max_map_count)) die("Failed to read vm.max_map_count from /proc/sys/vm/max_map_count");
    if (max_map_count / 2 - 1 - 1000 /* Just to make sure! */ < input->header().rows) // We need 2 mappings per row (header + data) plus one for the header, plus the other mappings the system usually has.
        std::cerr << "\x1b[33;1mWARNING\x1b[0m: vm.max_map_count = " << max_map_count << " might be too small; if necessary, increase it with `sysctl -w vm.max_map_count=...`\n";

    auto output = std::make_shared<y5::writer>(
        output_path,
        overwrite ? y5::writer_mode::OVERWRITE : y5::writer_mode::EXCLUSIVE,
        input->header().entry_size,
        static_cast<y5::format::header::bitflags>(input->header().flags | y5::format::header::transposed)
    );

    // Synchronization foo
    auto promise = std::make_shared<std::promise<void>>();
    auto future = promise->get_future();
    auto monitor = std::make_shared<parallel::monitor>([promise = std::move(promise)]() mutable { promise->set_value(); }, max_live_blocks);

    // Set up the parallel pipeline
    auto unused_pool = parallel::make_single_thread_scheduler();
    parallel::progress_log_proxy logger(parallel::make_single_thread_scheduler(), unused_pool, monitor, block_size, total);

    parallel::io::row_writer_proxy writer(parallel::make_single_thread_scheduler(), unused_pool, std::move(output), monitor);
    auto do_write = [writer, logger, block_size, total](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_transform(block_index);
        writer.write_rows(block_index, std::move(rows));
        logger.progress_writer(block_index); // TODO: Should be moved to writer callback
    };

    parallel::transposing_filter_proxy filter(
        parallel::make_single_thread_scheduler(),
        parallel::make_threadpool(threads),
        std::move(do_write),
        monitor,
        entry_size,
        entry_size,
        parallel::transposing_filter_servant::distinct_filter(entry_size),
        parallel::transposing_filter_servant::no_transform(entry_size)
    );
    auto do_filter = [filter, logger](std::size_t block_index, std::vector<parallel::io::row_segment> rows) mutable
    {
        logger.progress_reader(block_index);
        filter.transpose_and_filter(block_index, std::move(rows));
    };

    parallel::io::column_reader_proxy reader(parallel::make_single_thread_scheduler(), parallel::make_threadpool(threads), std::move(input), std::move(do_filter), monitor, block_size);

    // Run the pipeline
    reader.generate();

    // Wait for everything to be done
    future.get();
    return EXIT_SUCCESS;
}

// Copyright 2022 Tobias Holl, Katharina Bogad
// SPDX-License-Identifier: GPL-3.0-or-later

#include "frontend.h"

#include <y5.h>
#include <fmt/format.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <future>
#include <fstream>

[[noreturn]] void die(const std::string &message) { std::cerr << message << "\n"; exit(EXIT_FAILURE); }

int main(int argc, const char *argv[])
{
    ArgumentParser parser(argv[0]);
    parser.add(ArgumentParser::Argument { .name = "input", .help = "Y5 file to show information on" });
    parser.add(ArgumentParser::Toggle { .short_name = "-t", .long_name = "--toc", .help = "Show TOC entries" });
    parser.add(ArgumentParser::Toggle { .short_name = "-r", .long_name = "--rows", .help = "Show each row" });
    parser.add(ArgumentParser::Toggle { .short_name = "-q", .long_name = "--quiet", .help = "Show less information" });
    auto args = parser.parse(argc, argv);

    std::string input_path = args.at("input");

    auto input = std::make_shared<y5::reader>(input_path);
    std::cout << input_path << ":\n";
    std::cout << "  Flags:          ";
    if (input->header().flags & y5::format::header::transposed)
        std::cout << " TRANSPOSED";
    if (input->header().flags & y5::format::header::ragged)
        std::cout << " RAGGED";
    if (input->header().flags & y5::format::header::uncompressed)
        std::cout << " UNCOMPRESSED";
    if (input->header().flags & y5::format::header::unused)
        std::cout << " [unknown flags]";
    if (input->header().flags == 0)
        std::cout << " none";
    std::cout << "\n";
    std::cout << "  Entry size:      " << input->header().entry_size << "\n";
    std::cout << "  Rows:            " << input->header().rows << "\n";
    std::cout << "  Maximum columns: " << input->header().max_columns << "\n";
    std::cout << "  TOC resolution:  " << input->header().toc_resolution << "\n";
    std::cout << "  TOC capacity:    " << input->header().toc_capacity << "\n";

    if (!args.at("--toc").empty())
    {
        std::cout << "  TOC:\n";
        for (std::size_t i = 0; i < input->header().toc_capacity; ++i)
        {
            const auto &entry = input->toc_entry(i);
            if (entry.offset == 0)
                break;
            std::cout << fmt::format("    {:3d}: {:16x}\n", i, entry.offset);
        }
        std::cout << "\n";
    }

    if (args.at("--quiet").empty())
    {
        if (!args.at("--rows").empty())
            std::cout << "  Rows:\n";
        std::size_t compressed_size = 0, total_size = 0;
        for (std::size_t i = 0; i < input->header().rows; ++i)
        {
            input->seek(i); // Seek to the next row
            if (!args.at("--rows").empty())
            {
                std::cout << fmt::format("    {:8d}: {:8d} columns, {:8d} bytes\n", i, input->row_header().columns, input->row_header().compressed_size);
            }
            compressed_size += input->row_header().compressed_size;
            total_size += input->row_header().columns * input->header().entry_size;
        }
        if (!args.at("--rows").empty())
            std::cout << "\n";
        std::cout << "  Compressed size: " << compressed_size << "\n";
        std::cout << "  Total size:      " << total_size << "\n";
    }
    std::cout << "  File size:       " << std::filesystem::file_size(input_path) << "\n";

    return EXIT_SUCCESS;
}

![Whitebox Cryptography is a sane thing to do and other hilarious jokes you can tell yourself, Volume II.](meme.jpg)

Whiteboxgrind is a full analysis toolchain for whitebox cryptography implementations. It collects program traces using a Valgrind-based tracer and attacks the implementation using differential computation analysis (see e.g. [here](https://eprint.iacr.org/2017/355.pdf)). The paper can be found [here](https://link.springer.com/chapter/10.1007/978-3-031-29497-6_11).

## valgrind stuff

Valgrind uses automake and should build on anything. Debian works, and we also have a PKGBUILD for ArchLinux soon (tm).

To build the `whiteboxgrind` tracing tool, run `./configure && make -j && make install` as usual. You probably want to specify a `--prefix` to `configure` to install to some local directory.

In theory (though we haven't tested it lately) the PKGBUILD should also work on ArchLinux (via `makepkg -sfi`); this will install our modified Valgrind to `/opt/whiteboxgrind/`. On ArchLinux, you may need to apply patches to be compatible with newer MPI versions; see the PKGBUILD for details.

If you _really_ want to, you can run the tool directly (`valgrind --tool=whiteboxgrind /your/binary`), but we would recommend using our tracing wrapper. As always, you can use `--help` to get more information.


## liby5

To read and write `.y5` files, build the liby5 library in the `y5` directory with CMake (see below), or use the PKGBUILD (`makepkg -sfi`). This is a dependency for the other tools (they might build liby5 automatically, but don't rely on it). You can build this as a static library as well, just specify `-DBUILD_SHARED_LIBS=OFF` on the CMake command line.

You need to have `brotli` (libbrotlienc and libbrotlidec) installed to build `y5`.


## tools

Our tooling is written in C++20 (i.e. use a modern compiler if you encounter issues). Again, this should build with CMake; for a static binaries (useful for deployment) look at the commands that run in the normal build (`make V=1` for more details); you'll need static library versions of all the dependencies.

Dependencies are Boost, y5 (which in turn needs brotli for compression), and fmtlib. For the `cpa` tool, we also require OpenSSL (libcrypto). Remember to pull the submodules to get Asynchronous (`git submodule update`), which we use for multithreading stuff.

`--help` works on all the tools.

By default, `make` will build all the tools; you can specify individual tools if you only want some of them.

#### tracer

```
Usage:
  ./tracer [-f] [--no-include] [-n NAME] [-s SOCKET] [-i INPUTS] [-c COUNT] [--block-size BLOCK_SIZE] [--cflags CFLAGS] [--args ARGS] [--resources RESOURCES] source output valgrind

Arguments:
                          source  Whitebox source code
                          output  Output directory
                        valgrind  Valgrind/Whiteboxgrind executable
Options:
                     --overwrite  Overwrite the output files if they already exist
                    --no-include  Do not automatically derive an include path
                          --name  Prefix for output file names [default: ...]
                        --socket  Socket location
                        --inputs  File containing values or raw input data
                         --count  Number of random inputs to generate
                    --block-size  Cipher block size in bytes [default: 16]
                        --cflags  Extra compilation flags
                          --args  Extra whiteboxgrind arguments
                     --resources  The tracer's resource directory [default: ...]
```

This will automatically build the AES implementation with the `main.c` file in the resources directory (hint: see `cpp/rsrc` in this repo) and trace it. You can customize some things, but the only really important features are `--name` (to identify your output files) and `--inputs` (to use fixed inputs in liby5 or raw binary format) or `--count` (to use `COUNT` random inputs).

#### filter

```
Usage:
  ./filter [--overwrite] [-j THREADS] [--block-size BLOCK_SIZE] [--max-row-memory MAX_ROW_MEMORY] [--max-live-blocks MAX_LIVE_BLOCKS] input output

Arguments:
                           input  Source traces in Y5 format
                          output  Output file (in Y5 format)
Options:
                     --overwrite  Overwrite the output file
                       --threads  Number of threads [default: 12]
                    --block-size  Block size for performance tuning [default: 1024]
                --max-row-memory  Maximum buffer size per row (in KiB) [default: 8192]
               --max-live-blocks  Maximum number of live blocks in processing [default: 64]
```

`filter` will remove samples that do not depend on the inputs.

#### unique

```
Usage:
  ./unique [--overwrite] [-j THREADS] [--block-size BLOCK_SIZE] [--cache-size CACHE_SIZE] [--max-live-blocks MAX_LIVE_BLOCKS] [--compare-begin COMPARE_BEGIN] [--compare-end COMPARE_END] input output

Arguments:
                           input  Filtered traces in Y5 format
                          output  Output file (in Y5 format)
Options:
                     --overwrite  Overwrite the output file
                       --threads  Number of threads [default: 12]
                    --block-size  Block size for performance tuning [default: 1024]
                    --cache-size  Maximum number of entries in the cache (-1: cache everything and use all the memory) [default: -1]
               --max-live-blocks  Maximum number of live blocks in processing [default: 64]
                 --compare-begin  Offset at which to start computing hashes for deduplication [default: 8]
                   --compare-end  Offset at which to stop computing hashes for deduplication (-1: at the end) [default: -1]
```

`unique` will remove samples that occur repeatedly. You can use `--compare-begin` and `--compare-end` to specify parts of the sample that should be ignored (the defaults use this to ignore the sample index at the start of each entry).

#### leakage

```
Usage:
  ./leakage [--overwrite] [-j THREADS] [--model MODEL] [--block-size BLOCK_SIZE] [--max-row-memory MAX_ROW_MEMORY] [--max-live-blocks MAX_LIVE_BLOCKS] input output

Arguments:
                           input  Source traces in Y5 format
                          output  Output file (in Y5 format)
Options:
                     --overwrite  Overwrite the output file
                       --threads  Number of threads [default: 12]
                         --model  Leakage model [default: hamming_weight]
                    --block-size  Block size for performance tuning [default: 8192]
                --max-row-memory  Maximum buffer size per row (in KiB) [default: 8]
               --max-live-blocks  Maximum number of live blocks in processing [default: 256]
```

Precomputes the leakage values. Use `--model` to specify the leakage model (we only have `hamming_weight` right now).

#### cpa

```
Usage:
  ./cpa [--validate] [--overwrite-graph] [-j THREADS] [--block-size BLOCK_SIZE] [--max-live-blocks MAX_LIVE_BLOCKS] [--chunk-key-bytes CHUNK_KEY_BYTES] [--selection-function SELECTION_FUNCTION] [-g GRAPH] [-o OUTPUT] input values

Arguments:
                           input  Leakage values in Y5 format
                          values  Values (in Y5 format)
Options:
                      --validate  Validate the recovered key against the provided values
               --overwrite-graph  Overwrite the graph data if it already exists
                       --threads  Number of threads [default: 12]
                    --block-size  Block size for performance tuning [default: 1024]
               --max-live-blocks  Maximum number of live blocks in processing [default: 64]
               --chunk-key-bytes  Process key bytes in chunks of this size [default: 16]
            --selection-function  Selection function to use (hamming_weight_first, hamming_weight_last) [default: hamming_weight_first]
                         --graph  Dump the best correlation per key byte and block to a Y5 file in this directory (<key byte>.y5)
                        --output  CSV filename to dump correlation results to
```

This one does the actual attack. If things use too much memory or are too slow, try out `--chunk-key-bytes`, though it shouldn't be necessary. `--selection-function` selects the target (first or last round). If specified, `--graph` will dump the best correlation values per key byte in each block to a `.y5` file (for nice plots); `--output` will write the best correlation results for each key byte and value to a final CSV file.

#### info

```
Usage:
  ./info [-t] [-r] [-q] input

Arguments:
                           input  Y5 file to show information on
Options:
                           --toc  Show TOC entries
                          --rows  Show each row
                         --quiet  Show less information
```

`info` just gives an overview over a `.y5` file. This isn't specific to our tool chain, but sometimes helpful (e.g. if you want to know how many samples are in a trace).

## building with cmake

```
mkdir build
cd build
cmake ..
make -j
```

That's it. Things should also work with `-G Ninja` or other CMake-compatible build tooling of your choice.


## test programs

We pull all challenges from the 2019 whitebox contest github (`grab.py`), compile them and write their compile log and some information about symbols (`extract.py`).
These informations are searched for stuff in `analyze.py` which prints out a summary.

## tl;dr (Installing on ArchLinux)

If all of this is too much info, here's how to install our tooling on a reasonably up-to-date ArchLinux system:

```
# Install dependencies
pacman -S --needed base-devel boost cmake fmt git

# Build and install the patched valgrind
makepkg -sfi

# Build and install liby5
cd y5
makepkg -sfi

# Build the C++ tooling
cd ../cpp
cmake -DCMAKE_BUILD_TYPE=Release -H. -Bbuild
make -Cbuild -j
```

## tl;dr (Debian)

If all of this is too much info, here's how to install our tooling on a Debian system (tested on Debian 11 "bullseye"):

```
# Install dependencies
apt-get install build-essential cmake git libboost-all-dev libbrotli-dev libfmt-dev libssl-dev pkg-config

# Build and install the patched valgrind
cd valgrind
./configure --prefix=/opt/whiteboxgrind
make -j && sudo make install

# Build and install liby5
cd ../y5
cmake -DCMAKE_BUILD_TYPE=Release -H. -Bbuild
cd build
make -j && sudo make install

# Build our tooling
mkdir ../cpp
cmake -DCMAKE_BUILD_TYPE=Release -H. -Bbuild
make -Cbuild -j
```

## tl;dr (full attack run)

After installing (see above), here's an example run that attacks the `peaceful_williams` implementation with 500 random inputs:

```
mkdir /tmp/attack
./build/tracer --count 500 --name "peaceful_williams-" --resources ../cpp/rsrc \
               ../test-programs/peaceful_williams/peaceful_williams.c \
               /tmp/attack \
               /opt/whiteboxgrind/bin/valgrind
./build/filter /tmp/attack/peaceful_williams-read.y5 /tmp/attack/peaceful_williams-read-filtered.y5
./build/unique /tmp/attack/peaceful_williams-read-filtered.y5 /tmp/attack/peaceful_williams-read-unique.y5
./build/leakage /tmp/attack/peaceful_williams-read-unique.y5 /tmp/attack/peaceful_williams-read-leakage.y5
./build/cpa --validate --output /tmp/attack/results.csv \
            /tmp/attack/peaceful_williams-read-leakage.y5 \
            /tmp/attack/peaceful_williams-values.y5
```

# licensing

- `y5` and the associated tooling is released under the MIT license (SPDX: `MIT`).
- Valgrind (and the whiteboxgrind tracer built on top of it) is licensed under GPL v2 (and only version 2; SPDX: `GPL-2.0-only`).
- The actual attack tooling in `cpp` is released under GPL v3, or any later version of the GPL (SPDX: `GPL-3.0-or-later`).

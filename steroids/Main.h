#pragma once
#include <windows.h>
#include <d2d1.h>
#include <d2d1helper.h>

#include "Plot.h"


class SteroidsRenderer
{
public:
    SteroidsRenderer();
    ~SteroidsRenderer();

    // Register the window class and call methods for instantiating drawing resources
    HRESULT Initialize(WCHAR *filepath);

    // Process and dispatch messages
    void RunMessageLoop();

private:
    // Initialize device-independent resources.
    HRESULT CreateDeviceIndependentResources();

    // Initialize device-dependent resources.
    HRESULT CreateDeviceResources();

    // Release device-dependent resource.
    void DiscardDeviceResources();

    // Draw content.
    HRESULT OnRender();

    void RenderPlot(Plot* plot, bool dataOnly);
    void SetPlotsSizes(FLOAT width, FLOAT height);

    // Resize the render target.
    void OnResize(
        UINT width,
        UINT height
    );

    void HandleKeyUp(WPARAM vKeyCode);
    void HandleKeyDown(WPARAM vKeyCode);
    void HandleLMouseButton(FLOAT x, FLOAT y);

    // The windows procedure.
    static LRESULT CALLBACK WndProc(
        HWND hWnd,
        UINT message,
        WPARAM wParam,
        LPARAM lParam
    );

    HWND m_hwnd;
    WCHAR* filepath;
    H5File *hfile;

    ID2D1Factory* m_pDirect2dFactory;
    ID2D1HwndRenderTarget* m_pRenderTarget;
    ID2D1SolidColorBrush* m_pLightSlateGrayBrush;
    ID2D1SolidColorBrush* m_pBlackBrush;
    ID2D1SolidColorBrush* m_pCornflowerBlueBrush;
    IDWriteTextFormat* m_pTitleTextFormat;
    IDWriteTextFormat* m_pYAxisTextFormat;
    IDWriteTextFormat* m_pXAxisTextFormat;
    IDWriteTextFormat* m_pXAxisLeftTextFormat;
    ID2D1Factory* pD2DFactory_;
    IDWriteFactory* pDWriteFactory_;

    Plot* PcPlot;
    Plot* HeapPlot;
    Plot* DataPlot;

};

template<class Interface>
inline void SafeRelease(
    Interface** ppInterfaceToRelease
)
{
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}


#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif


inline std::string StringFromWide(LPCWSTR input) {
    ULONG bytes = WideCharToMultiByte(CP_UTF8, 0, input, -1, NULL, 0, NULL, NULL);
    std::vector<char> raw(bytes);
    if (WideCharToMultiByte(CP_UTF8, 0, input, -1, raw.data(), bytes, NULL, NULL) == 0)
        throw;
    return std::string(raw.begin(), raw.end());
}


#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif

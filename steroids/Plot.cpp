#include "Plot.h"
#include <stdio.h>
#include <algorithm>
#include <cmath>

#include<H5Cpp.h>

using namespace H5;

// fix 200 units for y label area
const FLOAT ylabel_area = 150.0f;
const FLOAT xlabel_area = 20.0f;

herr_t
file_info(hid_t loc_id, const char* name, const H5L_info_t* linfo, void* opdata)
{
	hid_t group;
	auto group_names = reinterpret_cast<std::vector<std::string>*>(opdata);
	//group = H5Gopen2(loc_id, name, H5P_DEFAULT);
	//do stuff with group object, if needed
	//group_names->push_back(name);
	//H5Gclose(group);
	return 0;
}


Plot::Plot(WCHAR* label, H5File *fp, const std::string &dset, size_t entry_size, ExtractorFunction extract_value, UINT64 min_addr, UINT64 max_addr) : name(NULL), zoom_x(NULL), zoom_y(NULL), pos_x(NULL), pos_y(NULL), width(NULL), height(NULL), entry_size(entry_size), extract_value(std::move(extract_value)), data(NULL), backdata(NULL), brush(NULL), x_axis(std::nullopt), sync_plot(nullptr) {
	this->name = label;
	this->xlabels_current = std::vector<UINT64>(XLABEL_COUNT, 0);
	this->xlabels_next = std::vector<UINT64>(XLABEL_COUNT, 0);
	this->file = fp;

	// set initial zoom to 1.0
	this->zoom_x = 1.0;
	this->zoom_y = 1.0;
	this->pan_x = this->pan_y = 0;
	this->data_changed = true;
	this->zoom_step_y = 0;

	this->data = new std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>();
	this->backdata = new std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>();

	this->data_minaddr = min_addr;
	this->data_maxaddr = max_addr;

	if (this->file == NULL)
		return;

	auto path = dset;

	auto group = new Group(this->file->openGroup("most-recent"));
	auto pos = path.find("/");

	while (pos != std::string::npos) {
		auto str = path.substr(0, pos);
		group = new Group(group->openGroup(str.c_str()));
		path = path.substr(pos + 1);
		pos = path.find("/");
	}

	this->dset = new DataSet(group->openDataSet(path.c_str()).getId());
	this->calcYLabels();
}

Plot::~Plot() { 
}

WCHAR* Plot::GetName() {
	return this->name;
}

FLOAT Plot::GetXZoom() { return zoom_x; }
FLOAT Plot::GetYZoom() { return zoom_y; }
void  Plot::SetZoom(FLOAT x, FLOAT y)
{
	this->zoom_x = x;
	this->zoom_y = y;

	this->calcYLabels();

	this->data_changed = true;
}

void Plot::ZoomYIn() {
	this->zoom_y = this->calcZoomValue(this->zoom_step_y++);
	this->calcYLabels();

	this->data_changed = true;
}
void Plot::ZoomYOut() {
	this->zoom_y = this->calcZoomValue(this->zoom_step_y--);
	this->calcYLabels();

	this->data_changed = true;
}

FLOAT Plot::calcZoomValue(UINT step) {
	if (step == 0)
		return 1.0f;

	auto ds = static_cast<DOUBLE>(step);

	auto exp = -1 / ds;
	auto adj = std::pow(20, exp);

	if (step > 0)
		return 1.0f - adj;
	
	return 1.0f + adj;
}

UINT Plot::getStepForZoomValue(FLOAT zoom) {
	if (zoom == 1)
		return 0;

	DOUBLE adj;
	if (zoom > 1)
		adj = zoom - 1.0f;
	else
		adj = (zoom - 1.0f) * -1.0f;

	auto exp = std::log(adj) / std::log(20);
	return static_cast<UINT>(std::floor(-1 / exp));
}

void Plot::getMinMaxFromData() {

	UINT64 min = UINT64_MAX, max = 0;

	for (const auto& elem : *this->data) {
		if (std::get<2>(elem) == 0)
			continue;

		if (std::get<1>(elem) < min) { min = std::get<1>(elem); }
		if (std::get<1>(elem) > max) { max = std::get<1>(elem); }
	}

	auto newmid = min + (max - min) / 2;
	this->pan_y = newmid - this->adj_midaddr;
	this->zoom_step_y = this->getStepForZoomValue(static_cast<DOUBLE>(max - newmid) / static_cast<DOUBLE>((this->data_maxaddr - this->data_minaddr) / 2));
	this->zoom_y = this->calcZoomValue(this->zoom_step_y);

	this->calcYLabels();

	this->data_changed = true;
}


FLOAT Plot::GetXPan() { return pan_x; }
FLOAT Plot::GetYPan() { return pan_y; }
void  Plot::SetPan(FLOAT x, FLOAT y)
{
	this->pan_x = x;
	this->pan_y = y;

	this->calcYLabels();

	this->data_changed = true;
}

FLOAT Plot::GetXPos() { return pos_x; }
FLOAT Plot::GetYPos() { return pos_y; }
void  Plot::SetPos(FLOAT x, FLOAT y)
{
	this->pos_x = x;
	this->pos_y = y;
}

void Plot::calcYLabels() {
	auto axheight = ((this->data_maxaddr - this->data_minaddr) / 2) * this->zoom_y; 
	this->adj_midaddr = static_cast<DOUBLE>(this->data_minaddr + (this->data_maxaddr - this->data_minaddr) / 2) + this->pan_y;
		
	this->adj_maxaddr = this->adj_midaddr + axheight;
	this->adj_minaddr = this->adj_midaddr - axheight;

}

FLOAT Plot::GetWidth() { return width; }
FLOAT Plot::GetHeight() { return height; }
void Plot::SetDimensions(FLOAT width, FLOAT height) 
{
	this->width = width;
	this->height = height;

	this->data_changed = true;
}

void Plot::GetPlotArea(FLOAT* x, FLOAT* y, FLOAT* w, FLOAT* h) {

	*x = this->pos_x + ylabel_area;
	*y = this->pos_y + xlabel_area;
	*w = this->width - ylabel_area;
	*h = this->height - 2 * xlabel_area;

}

void Plot::GetPlotTitleBoundingBox(FLOAT* x, FLOAT* y, FLOAT* w, FLOAT* h) {
	*x = this->pos_x + ylabel_area;
	*y = this->pos_y;
	*w = this->width - ylabel_area;
	*h = xlabel_area;
}

bool Plot::Collides(FLOAT x, FLOAT y) {
	return (x >= this->pos_x && x <= (this->pos_x + this->width) && 
	        y >= this->pos_y && y <= (this->pos_y + this->height));
}

void Plot::PrintPlotArea() {
	printf("Set PCPlot %f %f %f %f", this->pos_x, this->pos_y, this->width, this->height);
}

void Plot::GetYAxisLables(WCHAR* bottom, WCHAR* mid, WCHAR* top) 
{
	
	WCHAR buf[LABEL_BUFSIZE + 1] = { 0 };
	// SABBEL NIT, DAT GOIT
	auto max = static_cast<UINT64>(adj_maxaddr);
	auto middle = static_cast<UINT64>(adj_midaddr);
	auto min = static_cast<UINT64>(adj_minaddr);
	wsprintf(buf, L"0x%x%08x", max >> 32, max & UINT_MAX);
	wcscpy_s(bottom, LABEL_BUFSIZE, buf);
	wsprintf(buf, L"0x%x%08x", middle >> 32, middle & UINT_MAX);
	wcscpy_s(mid, LABEL_BUFSIZE, buf);
	wsprintf(buf, L"0x%x%08x", min >> 32, min & UINT_MAX);
	wcscpy_s(top, LABEL_BUFSIZE, buf);
}

const std::vector<UINT64>& Plot::GetXAxisLables() const {
	return this->xlabels_current;
}

D2D1_RECT_F Plot::GetYAxisLabelBoundingBox(YLABEL_POSITION pos) 
{
	FLOAT border_right = this->pos_x + ylabel_area - 10.0f;
	switch (pos) 
	{
	case YLABEL_TOP:
		return D2D1::RectF(this->pos_x, this->pos_y + this->height - xlabel_area - (LABEL_FONTSIZE / 2.0f), border_right , this->pos_y + this->height - xlabel_area + (LABEL_FONTSIZE / 2));
	case YLABEL_MID:
		return D2D1::RectF(this->pos_x, this->pos_y + (this->height - LABEL_FONTSIZE) / 2.0f , border_right , this->pos_y + (this->height + LABEL_FONTSIZE) / 2.0f);
	case YLABEL_BOTTOM:
		return D2D1::RectF(this->pos_x, this->pos_y + xlabel_area - (LABEL_FONTSIZE / 2.0f), border_right, this->pos_y + xlabel_area + LABEL_FONTSIZE / 2.0f);
	}
}

D2D1_RECT_F Plot::GetXAxisLabelBoundingBox(UINT pos) 
{
	FLOAT xlabel_top = this->pos_y + this->height - xlabel_area + 4.0f;
	FLOAT xlabel_bot = xlabel_top + LABEL_FONTSIZE;
	FLOAT boxwidth = XLABEL_SIZE;
	FLOAT left = 0.0f;

	// first and last bounding box are special;
	// although they have the same bounding box, they have different fonts for leading/trailing alignment.
	if (pos == 0) {
		left = this->pos_x + ylabel_area;
		return D2D1::RectF(left, xlabel_top, left + boxwidth, xlabel_bot);
	}
	if (pos == XLABEL_COUNT - 1) {
		left = this->pos_x + this->width - boxwidth;
		return D2D1::RectF(left, xlabel_top, left + boxwidth, xlabel_bot);
	}

	// the rest is centered around the calculated middle point and will be center aligned.

	FLOAT xlabel_offset = (this->width - ylabel_area) / (XLABEL_COUNT-1);

	left = this->pos_x + ylabel_area + xlabel_offset * pos - boxwidth / 2;
	return D2D1::RectF(left, xlabel_top, left + boxwidth, xlabel_bot);

}

std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>* Plot::GetDataForPlot() {
	if (this->data_changed) {
		this->fetchData();
	}
	return this->data;
}

void Plot::fetchData() {
	size_t points = static_cast<size_t>(this->width - ylabel_area);

	if (this->file == NULL)
		return;

	this->backdata->clear();

	// we need to fetch points values from the dataset,
	// all equally spaced
	// TODO: around the center, scaled to this->zoom_x
	// TODO: offset by whatever offset we currently have.

	hsize_t size[2];
	
	DataSpace fspace = this->dset->getSpace();
	auto edims = fspace.getSimpleExtentDims(size, NULL);


	hsize_t dims[2];
	dims[0] = 1;
	dims[1] = 1;

	hsize_t offset[2];
	hsize_t noffset[2];
	hsize_t count[2];
	count[0] = 1;
	count[1] = 1;
	offset[0] = 0;
	noffset[0] = noffset[1] = 0;

	// size[0] x size[1]
	// size[1] is total number of plot rows

	hsize_t midpoint, space;
	ssize_t leftmost, rightmost;
	leftmost = rightmost = -1;
	DOUBLE adj_minaddr, adj_maxaddr;
	size_t left, right;

	// always account the midpoint to the left side.
	left = points / 2 - (points % 2 == 0 ? 1 : 0);
	right = points / 2 -1 ;

	do {

		if (leftmost != -1 && rightmost != -1) {
			if (leftmost < 0)
				this->pan_x += 1;
			else
				this->pan_x -= 1;
		}

		midpoint = size[1] / 2;

		space = static_cast<hsize_t>(static_cast<DOUBLE>(midpoint / left) * this->zoom_x);
		midpoint += this->pan_x;

		leftmost = midpoint - (left * space);
		rightmost = midpoint + (right * space);
	} while (leftmost < 0 || rightmost > size[1]);

	void *dout = alloca(this->entry_size);

	DataSpace mspace(2, count, count);
	
	mspace.selectAll();

	auto GetEntry = [&](UINT64 off) {
		offset[1] = off;

		fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
		this->dset->read(dout, H5Dget_type(this->dset->getId()), mspace, fspace);
		FLOAT render_height = 0, render_width = -1;
		UINT64 x = 0, y = 0;

		std::tie(x, y) = this->extract_value(dout);
		if (y >= static_cast<UINT64>(this->adj_minaddr) && y < static_cast<UINT64>(this->adj_maxaddr)) {
			render_height = static_cast<DOUBLE>(y - static_cast<uint64_t>(this->adj_minaddr)) / static_cast<DOUBLE>(this->adj_maxaddr - this->adj_minaddr);
			render_height = (this->height - 2 * xlabel_area) * render_height;
		}
		if (this->x_axis && this->x_axis->first <= x && this->x_axis->second > x) {
			render_width = static_cast<DOUBLE>(x - this->x_axis->first) / static_cast<DOUBLE>(this->x_axis->second - this->x_axis->first);
			render_width *= (this->width - ylabel_area);
		}
		return std::make_tuple(x, y, render_height, render_width);
	};

	if (!x_axis)
		x_axis = { std::get<0>(GetEntry(0)), std::get<0>(GetEntry(size[1] - 1)) };

	for (size_t i = 0; i < left; i++)
		this->backdata->push_back(GetEntry(leftmost + i * space));
	this->backdata->push_back(GetEntry(midpoint));
	for (size_t j = 1; j < right; j++)
		this->backdata->push_back(GetEntry(midpoint + j * space));

	// calculate xlables
	if (!this->sync_plot) {
		this->xlabels_next[0] = std::get<0>(this->backdata->at(0));
		this->xlabels_next[XLABEL_COUNT - 1] = std::get<0>(this->backdata->at(this->backdata->size() - 1));

		uint64_t lspace = (this->backdata->size() / XLABEL_COUNT - 1);

		for (unsigned int i = 1; i < XLABEL_COUNT - 1; i++) {
			this->xlabels_next[i] = std::get<0>(this->backdata->at(i * lspace));
		}
	} else {
		this->xlabels_next = this->sync_plot->GetXAxisLables();
	}

	auto tdata = this->data;
	this->data_changed = false;

	std::swap(this->xlabels_next, this->xlabels_current);
	this->data = this->backdata;
	this->backdata = data;
}

std::optional<std::pair<UINT64, UINT64>> Plot::GetXAxisRange() const {
	return x_axis;
}

void Plot::SynchronizeXTo(const Plot& other) {
	auto other_x_axis = other.GetXAxisRange();
	if (!other_x_axis)
		return;
	this->x_axis = other_x_axis;
	this->sync_plot = &other;
	this->data_changed = true;
}

#!/usr/bin/python

import argparse
import h5py
import numpy as np
import y5

class Datatypes:
    plaintext_ciphertext_pair = np.dtype([
        ('plaintext', np.uint8, (16,)),
        ('ciphertext', np.uint8, (16,)),
    ])

    program_trace_entry = np.dtype([
        ('time', np.uint64),
        ('address', np.uint64),
    ])

    # Old hdf5 datatypes
    memory_trace_entry = np.dtype([
        ('time', np.uint64),
        ('address', np.uint64),
        ('pc', np.uint64),
        ('width', np.uint),
        ('value', np.uint8, (34,)),
        ('endianness', np.uint8),
    ])

    cas_trace_entry = np.dtype([
        ('time', np.uint64),
        ('address', np.uint64),
        ('pc', np.uint64),
        ('width', np.uint),
        ('read', np.uint8, (16,)),
        ('write', np.uint8, (16,)),
        ('endianness', np.ubyte),
    ])

    # y5 datatypes
    y5_memory_trace_entry = np.dtype([
        ('time', np.uint64),
        ('pc', np.uint64),
        ('address', np.uint64),
        ('width', np.uint32),
        ('endianness', np.uint8),
        ('mode', np.uint8),
        ('value', np.uint8, (34,)),
    ])

def cvt_mem(array_of_y5_mte):
    shape = (array_of_y5_mte.shape[-1],)
    array_of_y5_mte = array_of_y5_mte.reshape(shape).view(dtype=Datatypes.y5_memory_trace_entry)
    new_array = np.empty(shape, dtype=Datatypes.memory_trace_entry)
    for i in range(shape[-1]):
        new_array[i]['time'] = array_of_y5_mte[i]['time']
        new_array[i]['pc'] = array_of_y5_mte[i]['pc']
        new_array[i]['address'] = array_of_y5_mte[i]['address']
        new_array[i]['width'] = array_of_y5_mte[i]['width']
        new_array[i]['value'] = array_of_y5_mte[i]['value']
        new_array[i]['endianness'] = array_of_y5_mte[i]['endianness']
    return new_array

def cvt_cas(array_of_y5_mte):
    shape = (array_of_y5_mte.shape[-1],)
    array_of_y5_mte = array_of_y5_mte.reshape(shape).view(dtype=Datatypes.y5_memory_trace_entry)
    new_array = np.empty(shape, dtype=Datatypes.cas_trace_entry)
    for i in range(shape[-1]):
        new_array[i]['time'] = array_of_y5_mte[i]['time']
        new_array[i]['pc'] = array_of_y5_mte[i]['pc']
        new_array[i]['address'] = array_of_y5_mte[i]['address']
        new_array[i]['width'] = array_of_y5_mte[i]['width']
        new_array[i]['read'] = array_of_y5_mte[i]['value'][:16]
        new_array[i]['write'] = array_of_y5_mte[i]['value'][16:32]
        new_array[i]['endianness'] = array_of_y5_mte[i]['endianness']
    return new_array



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('values', help='*-values.y5')
    parser.add_argument('pc', help='*-pc.y5')
    parser.add_argument('read', help='*-read.y5')
    parser.add_argument('write', help='*-write.y5')
    parser.add_argument('cas', help='*-cas.y5')
    parser.add_argument('output', help='The HDF5 file to write to')
    parser.add_argument('-f', '--overwrite', help='Overwrite output file', action='store_true')
    parser.add_argument('--group', help='Group path', default='y5')
    parser.add_argument('--buffer-size', help='Buffer size', type=int, default=8192)
    args = parser.parse_args()

    assert args.buffer_size > 0, 'Invalid buffer size'

    out_file = h5py.File(args.output, 'w' if args.overwrite else 'a')
    assert out_file, 'Failed to open file'
    container = out_file.create_group(args.group)
    if 'most-recent' in out_file:
        del out_file['most_recent']
    out_file['most-recent'] = h5py.SoftLink(f'/{args.group}')

    # Create datasets
    def slurp(path, dtype, converter):
        print(f'Loading {path}')
        if not converter:
            converter = lambda x: x
        reader = y5.reader(path)
        shape = (reader.rows, reader.max_columns)
        storage = np.empty(shape, dtype=dtype)
        if reader.max_columns == 0:
            return shape, storage
        for row in range(reader.rows):
            col = 0
            while True:
                entries, done = reader.read(args.buffer_size)
                count = entries.shape[-1]
                storage[row:row+1, col:col+count] = converter(entries).view(dtype=dtype)
                col += count
                if done:
                    break
        return shape, storage

    def add(container, name, source, dtype, converter=None, compress=True):
        shape, data = slurp(source, dtype, converter)
        print(f'Adding {source} as {name}')
        if compress:
            return container.create_dataset(name, shape, dtype=dtype, data=data, compression="gzip")
        else:
            return container.create_dataset(name, shape, dtype=dtype, data=data)

    add(container, 'values', args.values, Datatypes.plaintext_ciphertext_pair, compress=False)
    add(container, 'pc-trace', args.pc, Datatypes.program_trace_entry)
    mem = container.create_group('mem-trace')
    add(mem, 'read', args.read, Datatypes.memory_trace_entry, converter=cvt_mem)
    add(mem, 'write', args.write, Datatypes.memory_trace_entry, converter=cvt_mem)
    add(mem, 'cas', args.cas, Datatypes.cas_trace_entry, converter=cvt_cas)

    out_file.close()

#pragma once
#include <windows.h>
#include <d2d1.h>
#include <d2d1helper.h>
#include <H5Cpp.h>
#include <vector>
#include <tuple>
#include <iostream>
#include <functional>
#include <optional>

using namespace H5;

#define LABEL_BUFSIZE 20
#define LABEL_FONTSIZE 12.0f

// TODO: Make this configurable
#define XLABEL_COUNT 10
#define XLABEL_SIZE 50.0f

#define PCPLOT_MINADDR 0x100000
#define PCPLOT_MAXADDR 0x120000

enum YLABEL_POSITION { YLABEL_BOTTOM, YLABEL_MID, YLABEL_TOP };

class Plot
{
public:
	using ExtractorFunction = std::function<std::pair<uint64_t, uint64_t>(void *)>;
	Plot(WCHAR* label, H5File* fp, const std::string &dset, size_t entry_size, ExtractorFunction extract_value, UINT64 min_addr, UINT64 max_addr/*, whatever foo we need */);
	~Plot();

	WCHAR* GetName();
	FLOAT GetXZoom();
	FLOAT GetYZoom();
	void SetZoom(FLOAT x, FLOAT y);

	void getMinMaxFromData();
	void ZoomYIn();
	void ZoomYOut();

	FLOAT GetXPan();
	FLOAT GetYPan();
	void SetPan(FLOAT x, FLOAT y);

	FLOAT GetXPos();
	FLOAT GetYPos();
	void SetPos(FLOAT x, FLOAT y);

	FLOAT GetWidth();
	FLOAT GetHeight();
	void SetDimensions(FLOAT width, FLOAT height);

	void GetPlotArea(FLOAT* x, FLOAT* y, FLOAT* w, FLOAT* h);
	void GetPlotTitleBoundingBox(FLOAT* x, FLOAT* y, FLOAT* w, FLOAT* h);
	void GetYAxisLables(WCHAR* bottom, WCHAR* mid, WCHAR* top);
	D2D1_RECT_F GetYAxisLabelBoundingBox(YLABEL_POSITION pos);
	const std::vector<UINT64>& GetXAxisLables() const;
	D2D1_RECT_F GetXAxisLabelBoundingBox(UINT pos);

	bool Collides(FLOAT x, FLOAT y);

	void PrintPlotArea();
	std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>* GetDataForPlot();

	ID2D1SolidColorBrush* brush;
	bool Selected;

	std::optional<std::pair<UINT64, UINT64>> GetXAxisRange() const;
	void SynchronizeXTo(const Plot& other);

private:
	WCHAR* name;
	FLOAT zoom_x;
	FLOAT zoom_y;
	FLOAT pan_x;
	FLOAT pan_y;

	FLOAT pos_x;
	FLOAT pos_y;

	FLOAT width;
	FLOAT height;

	std::vector<UINT64> xlabels_current;
	std::vector<UINT64> xlabels_next;

	H5File *file;
	DataSet *dset;

	size_t entry_size;
	ExtractorFunction extract_value;

	DOUBLE adj_minaddr, adj_midaddr, adj_maxaddr;
	UINT64 data_minaddr, data_maxaddr;

	UINT zoom_step_y;

	bool data_changed;

	// vector with len width, elements <time, address, y-in-plot>
	// idea: load off data access to another thread to not stall
	// the rendering thread, and flip the pointers when data loading
	// is complete.
	// needs logic to simplyfy scrolling.
	std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>* data;
	std::vector<std::tuple<unsigned long long, unsigned long long, FLOAT, FLOAT>>* backdata;

	std::optional<std::pair<UINT64, UINT64>> x_axis;
	const Plot *sync_plot;

	void fetchData();
	void calcYLabels();
	FLOAT calcZoomValue(UINT step);
	UINT getStepForZoomValue(FLOAT zoom);
};


// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>
#include <iostream>
#include <H5Cpp.h>
#include <comdef.h>
#include <tuple>
#include <iostream>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include "Main.h"
#include "Plot.h"

using namespace H5;


SteroidsRenderer::SteroidsRenderer() :
	m_hwnd(NULL),
	m_pDirect2dFactory(NULL),
	m_pRenderTarget(NULL),
	m_pLightSlateGrayBrush(NULL),
	m_pBlackBrush(NULL),
	m_pCornflowerBlueBrush(NULL),
	m_pTitleTextFormat(NULL),
	PcPlot(NULL),
	HeapPlot(NULL),
	DataPlot(NULL)
{
}


SteroidsRenderer::~SteroidsRenderer()
{
	SafeRelease(&m_pDirect2dFactory);
	SafeRelease(&m_pRenderTarget);
	SafeRelease(&m_pLightSlateGrayBrush);
	SafeRelease(&m_pCornflowerBlueBrush);

}

void SteroidsRenderer::RunMessageLoop()
{
	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

HRESULT SteroidsRenderer::Initialize(WCHAR* path)
{
	HRESULT hr;
	this->filepath = path;

	// Setup h5 stuff
	auto lp = StringFromWide(path);
	this->hfile = new H5File(lp.c_str(), H5F_ACC_RDONLY);
	PcPlot = new Plot((WCHAR*)L"Program Counter", this->hfile, "pc-trace", 16, [](void* raw) {
		auto tup = reinterpret_cast<uint64_t*>(raw);
		return std::pair<uint64_t, uint64_t> { tup[0], tup[1] };
		}, PCPLOT_MINADDR, PCPLOT_MAXADDR);
	HeapPlot = new Plot((WCHAR*)L"Heap Access", this->hfile, "mem-trace/write", 128, [](void* raw) {
		auto tup = reinterpret_cast<uint64_t*>(raw);
		return std::pair<uint64_t, uint64_t> { tup[0], tup[1] };
		}, 0x1ffeull << 24ull, 0x20ull << 32ull);
	DataPlot = new Plot((WCHAR*)L"bss Access", this->hfile, "mem-trace/write", 128, [](void* raw) {
		auto tup = reinterpret_cast<uint64_t*>(raw);
		return std::pair<uint64_t, uint64_t> { tup[0], tup[1] };
		}, PCPLOT_MINADDR, PCPLOT_MAXADDR);

	// Initialize device-indpendent resources, such
	// as the Direct2D factory.
	hr = CreateDeviceIndependentResources();

	if (SUCCEEDED(hr))
	{
		// Register the window class.
		WNDCLASSEX wcex = { sizeof(WNDCLASSEX) };
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = SteroidsRenderer::WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = sizeof(LONG_PTR);
		wcex.hInstance = HINST_THISCOMPONENT;
		wcex.hbrBackground = NULL;
		wcex.lpszMenuName = NULL;
		wcex.hCursor = LoadCursor(NULL, IDI_APPLICATION);
		wcex.lpszClassName = L"SteroidsRenderer";

		RegisterClassEx(&wcex);


		// Because the CreateWindow function takes its size in pixels,
		// obtain the system DPI and use it to scale the window size.
		FLOAT dpiX, dpiY;

		// The factory returns the current system DPI. This is also the value it will use
		// to create its own windows.
		//m_pDirect2dFactory->GetDpiForWindow(&dpiX, &dpiY);
		dpiX = dpiY = static_cast<FLOAT>(GetDpiForSystem());


		// Create the window.
		m_hwnd = CreateWindow(
			L"SteroidsRenderer",
			L"SteroidsRenderer Demo App",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			static_cast<UINT>(ceil(640.f * dpiX / 96.f)),
			static_cast<UINT>(ceil(480.f * dpiY / 96.f)),
			NULL,
			NULL,
			HINST_THISCOMPONENT,
			this
		);
		hr = m_hwnd ? S_OK : E_FAIL;
		if (SUCCEEDED(hr))
		{
			ShowWindow(m_hwnd, SW_SHOWNORMAL);
			UpdateWindow(m_hwnd);
		}
	}

	return hr;
}


HRESULT SteroidsRenderer::CreateDeviceIndependentResources()
{
	HRESULT hr = S_OK;

	// Create a Direct2D factory.
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);

	return hr;
}

HRESULT SteroidsRenderer::CreateDeviceResources()
{
	HRESULT hr = S_OK;

	if (!m_pRenderTarget)
	{
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(
			rc.right - rc.left,
			rc.bottom - rc.top
		);

		this->SetPlotsSizes(static_cast<FLOAT>(size.width), static_cast<FLOAT>(size.height));


		// Create a Direct2D render target.
		hr = m_pDirect2dFactory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(m_hwnd, size),
			&m_pRenderTarget
		);


		if (SUCCEEDED(hr))
		{
			// Create a gray brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::LightSlateGray),
				&m_pLightSlateGrayBrush
			);
		}
		if (SUCCEEDED(hr))
		{
			// Create a gray brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::Black),
				&m_pBlackBrush
			);
		}
		if (SUCCEEDED(hr))
		{
			// Create a blue brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::RoyalBlue),
				&this->PcPlot->brush
			);
		}
		if (SUCCEEDED(hr))
		{
			// Create a blue brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::DarkRed),
				&this->DataPlot->brush
			);
		}
		if (SUCCEEDED(hr))
		{
			// Create a blue brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::DarkRed),
				&this->HeapPlot->brush
			);
		}
		if (SUCCEEDED(hr))
		{
			hr = D2D1CreateFactory(
				D2D1_FACTORY_TYPE_SINGLE_THREADED,
				&pD2DFactory_
			);
		}
		if (SUCCEEDED(hr))
		{
			hr = DWriteCreateFactory(
				DWRITE_FACTORY_TYPE_SHARED,
				__uuidof(IDWriteFactory),
				reinterpret_cast<IUnknown**>(&pDWriteFactory_)
			);
		}
		if (SUCCEEDED(hr))
		{
			hr = pDWriteFactory_->CreateTextFormat(
				L"Segoe UI",	// Font family name.
				NULL,		// Font collection (NULL sets it to use the system font collection).
				DWRITE_FONT_WEIGHT_REGULAR,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				14.0f,
				L"en-us",
				&m_pTitleTextFormat
			);
		}
		// Center align (horizontally) the text.
		if (SUCCEEDED(hr))
		{
			hr = m_pTitleTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		}

		if (SUCCEEDED(hr))
		{
			hr = m_pTitleTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
		}

		if (SUCCEEDED(hr))
		{
			hr = pDWriteFactory_->CreateTextFormat(
				L"Segoe UI",	// Font family name.
				NULL,		// Font collection (NULL sets it to use the system font collection).
				DWRITE_FONT_WEIGHT_REGULAR,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				12.0f,
				L"en-us",
				&m_pYAxisTextFormat
			);
		}
		// Center align (horizontally) the text.
		if (SUCCEEDED(hr))
		{
			hr = m_pYAxisTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_TRAILING);
		}

		if (SUCCEEDED(hr))
		{
			hr = m_pYAxisTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
		}

		if (SUCCEEDED(hr))
		{
			hr = pDWriteFactory_->CreateTextFormat(
				L"Segoe UI",	// Font family name.
				NULL,		// Font collection (NULL sets it to use the system font collection).
				DWRITE_FONT_WEIGHT_REGULAR,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				12.0f,
				L"en-us",
				&m_pXAxisLeftTextFormat
			);
		}
		// Center align (horizontally) the text.
		if (SUCCEEDED(hr))
		{
			hr = m_pXAxisLeftTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		}

		if (SUCCEEDED(hr))
		{
			hr = m_pXAxisLeftTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
		}

		if (SUCCEEDED(hr))
		{
			hr = pDWriteFactory_->CreateTextFormat(
				L"Segoe UI",	// Font family name.
				NULL,		// Font collection (NULL sets it to use the system font collection).
				DWRITE_FONT_WEIGHT_REGULAR,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				12.0f,
				L"en-us",
				&m_pXAxisTextFormat
			);
		}
		// Center align (horizontally) the text.
		if (SUCCEEDED(hr))
		{
			hr = m_pXAxisTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		}

		if (SUCCEEDED(hr))
		{
			hr = m_pXAxisTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
		}
	}

	return hr;
}

void SteroidsRenderer::DiscardDeviceResources()
{
	SafeRelease(&m_pRenderTarget);
	SafeRelease(&m_pLightSlateGrayBrush);
	SafeRelease(&m_pCornflowerBlueBrush);
}

LRESULT CALLBACK SteroidsRenderer::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT result = 0;

	if (message == WM_CREATE)
	{
		LPCREATESTRUCT pcs = (LPCREATESTRUCT)lParam;
		SteroidsRenderer* pDemoApp = (SteroidsRenderer*)pcs->lpCreateParams;

		::SetWindowLongPtrW(
			hwnd,
			GWLP_USERDATA,
			reinterpret_cast<LONG_PTR>(pDemoApp)
		);

		result = 1;
	}
	else
	{
		SteroidsRenderer* pDemoApp = reinterpret_cast<SteroidsRenderer*>(static_cast<LONG_PTR>(
			::GetWindowLongPtrW(
				hwnd,
				GWLP_USERDATA
			)));

		bool wasHandled = false;

		if (pDemoApp)
		{
			switch (message)
			{
			case WM_SIZE:
			{
				UINT width = LOWORD(lParam);
				UINT height = HIWORD(lParam);
				pDemoApp->OnResize(width, height);
			}
			result = 0;
			wasHandled = true;
			break;

			case WM_DISPLAYCHANGE:
			{
				InvalidateRect(hwnd, NULL, FALSE);
			}
			result = 0;
			wasHandled = true;
			break;

			case WM_PAINT:
			{
				pDemoApp->OnRender();
				ValidateRect(hwnd, NULL);
			}
			result = 0;
			wasHandled = true;
			break;

			case WM_DESTROY:
			{
				PostQuitMessage(0);
			}
			result = 1;
			wasHandled = true;
			break;

			case WM_KEYUP:
				pDemoApp->HandleKeyUp(wParam);
				pDemoApp->OnRender();
				ValidateRect(hwnd, NULL);
				result = 0;
				wasHandled = true;
				break;

			case WM_KEYDOWN:
				pDemoApp->HandleKeyDown(wParam);
				pDemoApp->OnRender();
				ValidateRect(hwnd, NULL);
				result = 0;
				wasHandled = true;
				break;

			case WM_LBUTTONDOWN:
				FLOAT xpos = static_cast<FLOAT>(LOWORD(lParam));
				FLOAT ypos = static_cast<FLOAT>(HIWORD(lParam));
				pDemoApp->HandleLMouseButton(xpos, ypos);
				pDemoApp->OnRender();
				ValidateRect(hwnd, NULL);
				result = 0;
				wasHandled = true;
				break;
			}
		}

		if (!wasHandled)
		{
			result = DefWindowProc(hwnd, message, wParam, lParam);
		}
	}

	return result;
}

void SteroidsRenderer::HandleLMouseButton(FLOAT x, FLOAT y) {

	if (this->PcPlot->Collides(x, y)) {
		if (!(GetKeyState(VK_SHIFT) & 0x8000)) {
			this->HeapPlot->Selected = false;
			this->DataPlot->Selected = false;
		}
		this->PcPlot->Selected = true;
	}
	if (this->HeapPlot->Collides(x, y)) {
		if (!(GetKeyState(VK_SHIFT) & 0x8000)) {
			this->PcPlot->Selected = false;
			this->DataPlot->Selected = false;
		}
		this->HeapPlot->Selected = true;
	}
	if (this->DataPlot->Collides(x, y)) {
		if (!(GetKeyState(VK_SHIFT) & 0x8000)) {
			this->PcPlot->Selected = false;
			this->DataPlot->Selected = false;
		}
		this->DataPlot->Selected = true;
	}
}

FLOAT getZoomStep(FLOAT value, bool in) {
	if (in) {
		if (value > 0.1) {
			return 0.1;
		}
		else if (value > 0.01) {
			return 0.01;
		}
		else if (value > 0.001) {
			return 0.001;
		}
		else
			return 0;
	}
	else {
		if (value < 0.01) {
			return 0.001;
		}
		else if (value < 0.1) {
			return 0.01;
		}
		else if (value < 1) {
			return 0.1;
		}
		else
			return 0;
	}
}

void SteroidsRenderer::HandleKeyUp(WPARAM vKeyCode) {
	// Keycode list:
	// https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
	// Luckily, codes match uppercase-ASCII.

	FLOAT zx, zy, step;
	FLOAT multiplier = 0.1;

	if (GetKeyState(VK_SHIFT) & 0x8000) {
		multiplier = 1;
	}

	switch (vKeyCode) {
	case 'N': // Zoom in X-Axis
		zx = this->PcPlot->GetXZoom();
		zy = this->PcPlot->GetYZoom();
		step = getZoomStep(zx, true);
		zx = zx - step;
		this->PcPlot->SetZoom(zx, zy);

		zx = this->HeapPlot->GetXZoom();
		zy = this->HeapPlot->GetYZoom();
		step = getZoomStep(zx, true);
		zx = zx - step;
		this->HeapPlot->SetZoom(zx, zy);

		zx = this->DataPlot->GetXZoom();
		zy = this->DataPlot->GetYZoom();
		step = getZoomStep(zx, true);
		zx = zx - step;

		this->HeapPlot->SetZoom(zx, zy);

		this->DataPlot->SynchronizeXTo(*this->PcPlot);
		this->HeapPlot->SynchronizeXTo(*this->PcPlot);
		break;
	case 'M': // Zoom out X-Axis
		zx = this->PcPlot->GetXZoom();
		zy = this->PcPlot->GetYZoom();
		step = getZoomStep(zx, false);
		zx = zx - step;
		this->PcPlot->SetZoom(zx, zy);

		zx = this->HeapPlot->GetXZoom();
		zy = this->HeapPlot->GetYZoom();
		step = getZoomStep(zx, false);
		zx = zx - step;
		this->HeapPlot->SetZoom(zx, zy);

		zx = this->DataPlot->GetXZoom();
		zy = this->DataPlot->GetYZoom();
		step = getZoomStep(zx, false);
		zx = zx - step;
		this->DataPlot->SetZoom(zx, zy);

		this->DataPlot->SynchronizeXTo(*this->PcPlot);
		this->HeapPlot->SynchronizeXTo(*this->PcPlot);
		break;
	case 'H': // Zoom in Y-Axis
		if (this->PcPlot->Selected) {
			this->PcPlot->ZoomYIn();
		}

		if (this->HeapPlot->Selected) {
			this->HeapPlot->ZoomYIn();
		}

		if (this->DataPlot->Selected) {
			this->DataPlot->ZoomYIn();
		}
		break;
	case 'J': // Zoom out Y-Axis
		if (this->PcPlot->Selected) {
			this->PcPlot->ZoomYOut();
		}

		if (this->HeapPlot->Selected) {
			this->HeapPlot->ZoomYOut();
		}

		if (this->DataPlot->Selected) {
			this->DataPlot->ZoomYOut();
		}
		break;
	case VK_ESCAPE: // Clear all selections
		this->PcPlot->Selected = false;
		this->HeapPlot->Selected = false;
		this->DataPlot->Selected = false;
		break;
	case 'U':
		if (this->PcPlot->Selected) {
			this->PcPlot->getMinMaxFromData();
		}
		if (this->HeapPlot->Selected) {
			this->HeapPlot->getMinMaxFromData();
		}
		if (this->DataPlot->Selected) {
			this->DataPlot->getMinMaxFromData();
		}
		break;
	case 'Q':
		exit(0);
	}
}

void SteroidsRenderer::HandleKeyDown(WPARAM wParam) {
	FLOAT px, py;
	FLOAT multiplier = 10;

	if (GetKeyState(VK_SHIFT) & 0x8000) {
		multiplier = 100;
	}


	switch (wParam) {
	case VK_RIGHT:
		px = this->PcPlot->GetXPan() + multiplier;
		py = this->PcPlot->GetYPan();

		this->PcPlot->SetPan(px, py);

		px = this->DataPlot->GetXPan() + multiplier;
		py = this->DataPlot->GetYPan();

		this->DataPlot->SetPan(px, py);


		px = this->HeapPlot->GetXPan() + multiplier;
		py = this->HeapPlot->GetYPan();

		this->HeapPlot->SetPan(px, py);

		this->DataPlot->SynchronizeXTo(*this->PcPlot);
		this->HeapPlot->SynchronizeXTo(*this->PcPlot);
		break;
	case VK_LEFT:
		px = this->PcPlot->GetXPan() - multiplier;
		py = this->PcPlot->GetYPan();

		this->PcPlot->SetPan(px, py);

		px = this->DataPlot->GetXPan() - multiplier;
		py = this->DataPlot->GetYPan();

		this->DataPlot->SetPan(px, py);


		px = this->HeapPlot->GetXPan() - multiplier;
		py = this->HeapPlot->GetYPan();

		this->HeapPlot->SetPan(px, py);

		this->DataPlot->SynchronizeXTo(*this->PcPlot);
		this->HeapPlot->SynchronizeXTo(*this->PcPlot);
		break;
	case VK_UP:
		if (this->PcPlot->Selected) {
			px = this->PcPlot->GetXPan();
			py = this->PcPlot->GetYPan() + multiplier;

			this->PcPlot->SetPan(px, py);
		}

		if (this->DataPlot->Selected) {
			px = this->DataPlot->GetXPan();
			py = this->DataPlot->GetYPan() + multiplier;

			this->DataPlot->SetPan(px, py);
		}

		if (this->HeapPlot->Selected) {
			px = this->HeapPlot->GetXPan();
			py = this->HeapPlot->GetYPan() + multiplier;

			this->HeapPlot->SetPan(px, py);
		}
		break;
	case VK_DOWN:
		if (this->PcPlot->Selected) {
			px = this->PcPlot->GetXPan();
			py = this->PcPlot->GetYPan() - multiplier;

			this->PcPlot->SetPan(px, py);
		}

		if (this->DataPlot->Selected) {
			px = this->DataPlot->GetXPan();
			py = this->DataPlot->GetYPan() - multiplier;

			this->DataPlot->SetPan(px, py);
		}

		if (this->HeapPlot->Selected) {
			px = this->HeapPlot->GetXPan();
			py = this->HeapPlot->GetYPan() - multiplier;

			this->HeapPlot->SetPan(px, py);
		}
		break;
	}
}

HRESULT SteroidsRenderer::OnRender()
{
	HRESULT hr = S_OK;

	hr = CreateDeviceResources();
	if (SUCCEEDED(hr))
	{
		m_pRenderTarget->BeginDraw();

		m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());

		m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));
		D2D1_SIZE_F rtSize = m_pRenderTarget->GetSize();

		RenderPlot(PcPlot, false);
		RenderPlot(DataPlot, false);
		RenderPlot(HeapPlot, false);

		hr = m_pRenderTarget->EndDraw();
	}

	if (hr == D2DERR_RECREATE_TARGET)
	{
		hr = S_OK;
		DiscardDeviceResources();
	}
	return hr;
}

void SteroidsRenderer::RenderPlot(Plot* plot, bool dataOnly)
{
	// Assumption: width is target width
	//             height is target height
	//             ylabel_sapce * width = space on the left side to render labels
	FLOAT x, y, width, height;

	if (!dataOnly) {
		// Step 1: Draw plot name
		plot->GetPlotTitleBoundingBox(&x, &y, &width, &height);
		m_pRenderTarget->DrawTextW(plot->GetName(), wcslen(plot->GetName()), this->m_pTitleTextFormat, D2D1::RectF(x, y, x + width, y + height), m_pBlackBrush);

		// Step 2: Draw plot box
		plot->GetPlotArea(&x, &y, &width, &height);

		m_pRenderTarget->DrawLine(D2D1::Point2F(x, y), D2D1::Point2F(x, y + height), m_pBlackBrush);
		m_pRenderTarget->DrawLine(D2D1::Point2F(x, y + height), D2D1::Point2F(x + width, y + height), m_pBlackBrush);
	}

	// Step 3: render data (which also fills the axis labels)
	auto data = plot->GetDataForPlot();
	for (auto it = data->begin(); it != data->end(); ++it) {
		auto elem = *it;

		auto yoff = std::get<2>(elem);
		auto xoff = std::get<3>(elem);

		if (yoff != 0 && xoff >= 0)
			m_pRenderTarget->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(x + xoff, y + height - yoff), 1, 1), plot->brush);

	}

	if (!dataOnly) {
		// Step 4: Draw Y Axis Labels
		WCHAR bot[LABEL_BUFSIZE], mid[LABEL_BUFSIZE], top[LABEL_BUFSIZE] = { 0 };

		plot->GetYAxisLables(bot, mid, top);
		m_pRenderTarget->DrawTextW(top, wcslen(top), this->m_pYAxisTextFormat, plot->GetYAxisLabelBoundingBox(YLABEL_TOP), m_pBlackBrush);
		m_pRenderTarget->DrawTextW(mid, wcslen(mid), this->m_pYAxisTextFormat, plot->GetYAxisLabelBoundingBox(YLABEL_MID), m_pBlackBrush);
		m_pRenderTarget->DrawTextW(bot, wcslen(bot), this->m_pYAxisTextFormat, plot->GetYAxisLabelBoundingBox(YLABEL_BOTTOM), m_pBlackBrush);


		WCHAR xlabel[LABEL_BUFSIZE];
		const auto& labels = plot->GetXAxisLables();
		// Step 5: Draw X Axis Labels
		wsprintf(xlabel, L"%u", labels[0]);
		m_pRenderTarget->DrawTextW(xlabel, wcslen(xlabel), this->m_pXAxisLeftTextFormat, plot->GetXAxisLabelBoundingBox(0), m_pBlackBrush);

		for (UINT i = 1; i < XLABEL_COUNT - 1; i++) {
			wsprintf(xlabel, L"%u", labels[i]);
			m_pRenderTarget->DrawTextW(xlabel, wcslen(xlabel), this->m_pXAxisTextFormat, plot->GetXAxisLabelBoundingBox(i), m_pBlackBrush);
		}

		wsprintf(xlabel, L"%u", labels[XLABEL_COUNT - 1]);
		m_pRenderTarget->DrawTextW(xlabel, wcslen(xlabel), this->m_pYAxisTextFormat, plot->GetXAxisLabelBoundingBox(XLABEL_COUNT - 1), m_pBlackBrush);


		// plot selected?
		if (plot->Selected) {
			x = plot->GetXPos();
			y = plot->GetYPos();
			width = plot->GetWidth();
			height = plot->GetHeight();

			m_pRenderTarget->DrawRectangle(D2D1::RectF(x, y, x + width, y + height), m_pLightSlateGrayBrush);
		}
	}
}

void SteroidsRenderer::OnResize(UINT width, UINT height)
{
	if (m_pRenderTarget)
	{
		// Note: This method can fail, but it's okay to ignore the
		// error here, because the error will be returned again
		// the next time EndDraw is called.
		this->SetPlotsSizes(static_cast<FLOAT>(width), static_cast<FLOAT>(height));
		m_pRenderTarget->Resize(D2D1::SizeU(width, height));

	}
}

void SteroidsRenderer::SetPlotsSizes(FLOAT width, FLOAT height) {
	const FLOAT padding = 10.0f;
	FLOAT plotwidth = width - 2 * padding;
	FLOAT plotheight = (height - padding) / 3 - padding;

	PcPlot->SetPos(padding, padding);
	PcPlot->SetDimensions(plotwidth, plotheight);

	DataPlot->SetPos(padding, 2 * padding + plotheight);
	DataPlot->SetDimensions(plotwidth, plotheight);

	HeapPlot->SetPos(padding, 3 * padding + 2 * plotheight);
	HeapPlot->SetDimensions(plotwidth, plotheight);

	// Synchronize immediately
	PcPlot->GetDataForPlot();
	DataPlot->SynchronizeXTo(*PcPlot);
	HeapPlot->SynchronizeXTo(*PcPlot);
}

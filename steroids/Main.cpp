// Windows Header Files:
#include <windows.h>
#define STRICT_TYPED_ITEMIDS
#include <shlobj.h>
#include <objbase.h>      // For COM headers
#include <shobjidl.h>     // for IFileDialogEvents and IFileDialogControlEvents
#include <shlwapi.h>
#include <knownfolders.h> // for KnownFolder APIs/datatypes/function headers
#include <propvarutil.h>  // for PROPVAR-related functions
#include <propkey.h>      // for the Property key APIs/datatypes
#include <propidl.h>      // for the Property System APIs
#include <strsafe.h>      // for StringCchPrintfW
#include <shtypes.h>      // for COMDLG_FILTERSPEC
#include <new>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include "Main.h"

HRESULT GetHdf5File(WCHAR* path);

const COMDLG_FILTERSPEC c_rgSaveTypes[] =
{
	{L"HDF5 Container (*.h5)",       L"*.h5"},
	{L"All Files (*.*)",         L"*.*"}
};

#if defined(__MINGW32__) || defined(__MINGW64__)
int WINAPI wWinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPWSTR lpCmdLine,
    int nCmdShow
) {
    auto cmdline = StringFromWide(lpCmdLine);
    auto editable = std::vector<char>(cmdline.cbegin(), cmdline.cend());
    return WinMain(hInstance, hPrevInstance, editable.data(), nCmdShow);
}
#endif

int WINAPI WinMain(
	HINSTANCE /* hInstance */,
	HINSTANCE /* hPrevInstance */,
	LPSTR /* lpCmdLine */,
	int /* nCmdShow */
)
{
	// Use HeapSetInformation to specify that the process should
	// terminate if the heap manager detects an error in any heap used
	// by the process.
	// The return value is ignored, because we want to continue running in the
	// unlikely event that HeapSetInformation fails.
	HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);

	if (SUCCEEDED(CoInitialize(NULL)))
	{
		{
			HRESULT hr;
			WCHAR* path = (WCHAR*)calloc(MAX_PATH, sizeof(WCHAR));

			hr = GetHdf5File(path);

			if (SUCCEEDED(hr)) {
				SteroidsRenderer app;

				if (SUCCEEDED(app.Initialize(path)))
				{
					app.RunMessageLoop();
				}
			}
			free(path);
		}
		CoUninitialize();
	}

	return 0;
}

// "simple" file open :confused-rofl:
// also the buffer at path must be able to handle all windows path lengths
// dangerous coding at home
HRESULT GetHdf5File(WCHAR* path) {

	if (path == NULL)
		return E_FAIL;

	IFileDialog* pfd = NULL;

	HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pfd));
	if (SUCCEEDED(hr))
	{
		// Set the options on the dialog.
		DWORD dwFlags;

		// Before setting, always get the options first in order 
		// not to override existing options.
		hr = pfd->GetOptions(&dwFlags);
		if (SUCCEEDED(hr))
		{
			// In this case, get shell items only for file system items.
			hr = pfd->SetOptions(dwFlags | FOS_FORCEFILESYSTEM);
			if (SUCCEEDED(hr))
			{
				// Set the file types to display only. 
				// Notice that this is a 1-based array.
				hr = pfd->SetFileTypes(ARRAYSIZE(c_rgSaveTypes), c_rgSaveTypes);
				if (SUCCEEDED(hr))
				{
					// Set the selected file type index to Word Docs for this example.
					hr = pfd->SetFileTypeIndex(1);
					if (SUCCEEDED(hr))
					{
						// Set the default extension to be ".doc" file.
						hr = pfd->SetDefaultExtension(L"h5");
						if (SUCCEEDED(hr))
						{
							// Show the dialog
							hr = pfd->Show(NULL);
							if (SUCCEEDED(hr))
							{
								// Obtain the result once the user clicks 
								// the 'Open' button.
								// The result is an IShellItem object.
								IShellItem* psiResult;
								hr = pfd->GetResult(&psiResult);
								if (SUCCEEDED(hr))
								{
									PWSTR pszFilePath = NULL;
									hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH,
										&pszFilePath);

									

									if (SUCCEEDED(hr))
									{
										wcscpy_s(path, MAX_PATH, pszFilePath);
										CoTaskMemFree(pszFilePath);
									}
									psiResult->Release();
								}
							}
						}
					}
				}
			}
		}
		pfd->Release();
	}
	return hr;
}
